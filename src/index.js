import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { BrowserRouter } from "react-router-dom";
import { StylesProvider } from "@material-ui/core/styles";
// import ReactTooltip from "react-tooltip";
//import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  // <React.StrictMode>
    
  // </React.StrictMode>,
  <BrowserRouter>
    {/* <ReactTooltip />  */}
    <StylesProvider injectFirst>
      
      <App />
    </StylesProvider>
  </BrowserRouter>,
  document.getElementById('root')
);