import http from "../http-common";

class BackofficeDataService {
  // getAll() {
  //   return http.get("/users");
  // }

  addSymbols(data) {
    return http.post('/backoffice-add', data);
  }

  removeSymbols(data) {
    return http.post('/backoffice-remove', data);
  }

  // create(data) {
  //   return http.post("/login", data);
  // }

  // update(id, data) {
  //   return http.put(`/users/${id}`, data);
  // }

  // delete(id) {
  //   return http.delete(`/users/${id}`);
  // }

  // deleteAll() {
  //   return http.delete(`/users`);
  // }

  // findByEmail(email) {
  //   return http.get(`/users?email=${email}`);
  // }
}

export default new BackofficeDataService();