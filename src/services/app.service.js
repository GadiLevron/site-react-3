import http from "../http-common";

class AppService {
  // getAll() {
  //   return http.get("/users");
  // }

  sendMessage(data) {
    return http.post('/main-contact', data);
  }

  // create(data) {
  //   return http.post("/login", data);
  // }

  // update(id, data) {
  //   return http.put(`/users/${id}`, data);
  // }

  // delete(id) {
  //   return http.delete(`/users/${id}`);
  // }

  // deleteAll() {
  //   return http.delete(`/users`);
  // }

  // findByEmail(email) {
  //   return http.get(`/users?email=${email}`);
  // }
}

export default new AppService();