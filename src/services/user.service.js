import http from "../http-common";

class UserDataService {
  // getAll() {
  //   return http.get("/users");
  // }

  checkLogin(data) {
    return http.post('/login', data);
  }

  // create(data) {
  //   return http.post("/login", data);
  // }

  // update(id, data) {
  //   return http.put(`/users/${id}`, data);
  // }

  // delete(id) {
  //   return http.delete(`/users/${id}`);
  // }

  // deleteAll() {
  //   return http.delete(`/users`);
  // }

  // findByEmail(email) {
  //   return http.get(`/users?email=${email}`);
  // }
}

export default new UserDataService();