  
import DOTNEV from 'dotenv';

export const login = () => {
    DOTNEV.config();
    sessionStorage.setItem(process.env.STORAGE_TOKEN_KEY, 'TestLogin');
}

export const logout = () => {
  sessionStorage.removeItem(process.env.STORAGE_TOKEN_KEY);
}

export const isLogin = () => {
    if (sessionStorage.getItem(process.env.STORAGE_TOKEN_KEY)) {
        return true;
    }

    return false;
}