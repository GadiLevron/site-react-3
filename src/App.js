import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Switch } from "react-router-dom";
import MainComponent from './components/pages/MainComponent';
import AboutUsComponent from './components/pages/AboutUsComponent';
import ContactUsComponent from './components/pages/ContactUsComponent';
import DisclaimerComponent from './components/pages/DisclaimerComponent';
// import LoginComponent from './components/pages/LoginComponent';
import BackofficeComponent from './components/pages/BackofficeComponent';
// import PrivateRoute from './components/routes/PrivateRoute';
import PublicRoute from './components/routes/PublicRoute';
// import StaticVars from './StaticVars';

// //define a new console
// var console=(function(oldCons){
//   return {
//       log: function(text){
//           if (StaticVars.isDeveloper) oldCons.log(text);
//       },
//       info: function (text) {
//         if (StaticVars.isDeveloper) oldCons.info(text);
//       },
//       warn: function (text) {
//         if (StaticVars.isDeveloper) oldCons.warn(text);
//       },
//       error: function (text) {
//         if (StaticVars.isDeveloper) oldCons.error(text);
//       }
//   };
// }(window.console));

// //redefine the old console
// window.console = console;

export default function App() {
  return (
    <Router>      
        <div className="AppDiv">
          <Switch>
            {/* <PublicRoute restricted={true} exact path={["/", "/login"]} component={LoginComponent} /> */}
            {/* <PublicRoute restricted={false} exact path={["/backoffice"]} component={BackofficeComponent} /> */}
            <PublicRoute restricted={false} exact path={["/", "/main"]} component={MainComponent} />
            <PublicRoute restricted={false} exact path={["/about"]} component={AboutUsComponent} />
            <PublicRoute restricted={false} exact path={["/contact"]} component={ContactUsComponent} />
            <PublicRoute restricted={false} exact path={["/disclaimer"]} component={DisclaimerComponent} />
            {/* <Route path="/tutorials/:id" component={Tutorial} /> */}
          </Switch>
        </div>
    </Router>
  );
}

//ReactDOM.render(<DrawArea />, document.getElementById("root"));