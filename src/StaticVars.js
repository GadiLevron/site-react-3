
import Immutable from 'immutable';

class StaticVars{
    isDeveloper = true;
    consoleLog = (message) => {if (this.isDeveloper) console.log(message)};

    TIMEFRAME_MENU_DEFAULT_INDEX = 5;

    DEFAULT_ALLOWED_RESULTS = 10;
    DEFAULT_BAR_WIDTH = 30;
    CANVAS_SIZE = {width: 900, height: 450};
    MINI_CANVAS_SIZE = {width: 180, height: 120};
    CANDLESTICK_SVG_HEIGHT_BUFFER = 50;
    BAR_EDIT_BACKGROUND_Y = 75;
    BAR_BACKGROUND_HEIGHT = 333;
    BAR_EDIT_HEIGHT = 258;
    BAR_EDIT_MIN_HIGH = 89;
    BAR_EDIT_MAX_LOW = 348;
    BAR_EDIT_OPEN_CLOSE_BUFFER =1.25;
    MINI_CANDLESTICK_SVG_HEIGHT_BUFFER = 17;
    CANVAS_DRAW_MIN_HEIGHT = 10;
    DRAW_BAR_HEIGHT_BUFFER = 85;
    MAX_ALLOWED_CANDLES = 10;
    DRAW_ANYTHING_MESSAGE = 'Draw any pattern, we will find it!';
    SEARCH_BUTTON_TEXT = 'Plot it!';
    MIN_BARS_SLIDER_MESSAGE = 'Minimum Pattern Candles';
    GRAPH_SELECTED_BACKGROUND_SIZE = {width: 62, height: 102};
    CUSTOM_PATTERN_TEXT = 'Custom Pattern';

    CANDLES_PATTERNS = {
      /* Bullish */
      Hammer:{name:'Hammer', pattern: new Immutable.List().push({isGrey: true, open: this.BAR_EDIT_MIN_HIGH + 50, high:this.BAR_EDIT_MIN_HIGH, low: this.BAR_EDIT_MIN_HIGH + 150, close: this.BAR_EDIT_MIN_HIGH, stroke: '#83868D', fill:'#83868D'})},  
      InvertedHammer:{name:'Inverted Hammer', pattern: new Immutable.List().push({isGrey: true, open: 250, high:100, low: 250, close: 200, stroke: '#83868D', fill:'#83868D'})},  
      DragonFlyDoji:{name:'DragonFly Doji', pattern: new Immutable.List().push({isGrey: true, open: 110, high:100, low: 250, close: 100, stroke: '#83868D', fill:'#83868D'})},  
      BullishEngulfing:{name:'Bullish Engulfing', pattern: new Immutable.List().push({open: 150, high:140, low: 210, close: 200}).push({open: 220, high:120, low: 230, close: 130})},  
      BullishHarami:{name:'Bullish Harami', pattern: new Immutable.List().push({open: 130, high:120, low: 230, close: 220}).push({open: 200, high:140, low: 210, close: 150})},  
      BullishKicker:{name:'Bullish Kicker', pattern: new Immutable.List().push({open: 190, high:180, low: 250, close: 240}).push({open: 160, high:100, low: 170, close: 110})},  
      MorningStar:{name:'Morning Star', pattern: new Immutable.List().push({open: 120, high:110, low: 220, close: 210}).push({isGrey: true, open: 250, high:240, low: 280, close: 270, stroke: '#83868D', fill:'#83868D'}).push({open: 210, high:110, low: 220, close: 120})},
      BullishAbandonedBaby:{name:'Bullish Abandoned Baby', pattern: new Immutable.List().push({open: 120, high:110, low: 220, close: 210}).push({open: 250, high:240, low: 280, close: 270}).push({open: 210, high:110, low: 220, close: 120})},
      ThreeSoldiers:{name:'Three Soldiers', pattern: new Immutable.List().push({open: this.BAR_EDIT_MIN_HIGH+160, high:this.BAR_EDIT_MIN_HIGH+60, low: this.BAR_EDIT_MIN_HIGH+170, close: this.BAR_EDIT_MIN_HIGH+70}).push({open: this.BAR_EDIT_MIN_HIGH+130, high:this.BAR_EDIT_MIN_HIGH+30, low: this.BAR_EDIT_MIN_HIGH+140, close: this.BAR_EDIT_MIN_HIGH+40}).push({open: this.BAR_EDIT_MIN_HIGH + 100, high:this.BAR_EDIT_MIN_HIGH, low: this.BAR_EDIT_MIN_HIGH + 110, close: this.BAR_EDIT_MIN_HIGH + 10})},
      BullishThreeLineStrike:{name:'Bullish Three Line Strike', pattern: new Immutable.List().push({open: this.BAR_EDIT_MIN_HIGH+190, high:this.BAR_EDIT_MIN_HIGH+90, low: this.BAR_EDIT_MIN_HIGH+200, close: this.BAR_EDIT_MIN_HIGH+100}).push({open: this.BAR_EDIT_MIN_HIGH+160, high:this.BAR_EDIT_MIN_HIGH+60, low: this.BAR_EDIT_MIN_HIGH+170, close: this.BAR_EDIT_MIN_HIGH+70}).push({open: this.BAR_EDIT_MIN_HIGH + 130, high:this.BAR_EDIT_MIN_HIGH + 30, low: this.BAR_EDIT_MIN_HIGH + 140, close: this.BAR_EDIT_MIN_HIGH + 40}).push({open: this.BAR_EDIT_MIN_HIGH+10, high:this.BAR_EDIT_MIN_HIGH, low: this.BAR_EDIT_MIN_HIGH+210, close: this.BAR_EDIT_MIN_HIGH+200})},
  
      /* Bearish */
      HangingMan:{name:'Hanging Man', pattern: new Immutable.List().push({isGrey: true, open: 110, high:100, low: 250, close: 100, stroke: '#83868D', fill:'#83868D'})},
      ShootingStar:{name:'Shooting Star', pattern: new Immutable.List().push({isGrey: true, open: 240, high:100, low: 250, close: 250, stroke: '#83868D', fill:'#83868D'})},  
      GravestoneDoji:{name:'Gravestone Doji', pattern: new Immutable.List().push({isGrey: true, open: 245, high:100, low: 250, close: 250, stroke: '#83868D', fill:'#83868D'})},
      BearishEngulfing:{name:'Bearish Engulfing', pattern: new Immutable.List().push({open: 200, high:140, low: 210, close: 150}).push({open: 130, high:120, low: 230, close: 220})},
      BearishHarami:{name:'Bearish Harami', pattern: new Immutable.List().push({open: 220, high:120, low: 230, close: 130}).push({open: 150, high:140, low: 210, close: 200})},
      BearishKicker:{name:'Bearish Kicker', pattern: new Immutable.List().push({open: 160, high:100, low: 170, close: 110}).push({open: 190, high:180, low: 250, close: 240})},
      EveningStar:{name:'Evening Star', pattern: new Immutable.List().push({open: this.BAR_EDIT_MIN_HIGH+160, high:this.BAR_EDIT_MIN_HIGH+50, low: this.BAR_EDIT_MIN_HIGH+170, close: this.BAR_EDIT_MIN_HIGH+60}).push({isGrey: true, open: this.BAR_EDIT_MIN_HIGH+10, high:this.BAR_EDIT_MIN_HIGH, low: this.BAR_EDIT_MIN_HIGH+40, close: this.BAR_EDIT_MIN_HIGH+30, stroke: '#83868D', fill:'#83868D'}).push({open: this.BAR_EDIT_MIN_HIGH+60, high:this.BAR_EDIT_MIN_HIGH+50, low: this.BAR_EDIT_MIN_HIGH+170, close: this.BAR_EDIT_MIN_HIGH+160})},
      BearishAbandonedBaby:{name:'Bearish Abandoned Baby', pattern: new Immutable.List().push({open: this.BAR_EDIT_MIN_HIGH+160, high:this.BAR_EDIT_MIN_HIGH+50, low: this.BAR_EDIT_MIN_HIGH+170, close: this.BAR_EDIT_MIN_HIGH+60}).push({open: this.BAR_EDIT_MIN_HIGH+30, high:this.BAR_EDIT_MIN_HIGH, low: this.BAR_EDIT_MIN_HIGH+40, close: this.BAR_EDIT_MIN_HIGH+10}).push({open: this.BAR_EDIT_MIN_HIGH+60, high:this.BAR_EDIT_MIN_HIGH+50, low: this.BAR_EDIT_MIN_HIGH+170, close: this.BAR_EDIT_MIN_HIGH+160})},
      ThreeCrows:{name:'Three Crows', pattern: new Immutable.List().push({open: this.BAR_EDIT_MIN_HIGH + 10, high:this.BAR_EDIT_MIN_HIGH, low: this.BAR_EDIT_MIN_HIGH + 110, close: this.BAR_EDIT_MIN_HIGH + 100}).push({open: this.BAR_EDIT_MIN_HIGH+40, high:this.BAR_EDIT_MIN_HIGH+30, low: this.BAR_EDIT_MIN_HIGH+140, close: this.BAR_EDIT_MIN_HIGH+130}).push({open: this.BAR_EDIT_MIN_HIGH+70, high:this.BAR_EDIT_MIN_HIGH+60, low: this.BAR_EDIT_MIN_HIGH+170, close: this.BAR_EDIT_MIN_HIGH+160})},
      BearishThreeLineStrike:{name:'Bearish Three Line Strike', pattern: new Immutable.List().push({open: this.BAR_EDIT_MIN_HIGH + 40, high:this.BAR_EDIT_MIN_HIGH+30, low: this.BAR_EDIT_MIN_HIGH + 140, close: this.BAR_EDIT_MIN_HIGH + 130}).push({open: this.BAR_EDIT_MIN_HIGH+70, high:this.BAR_EDIT_MIN_HIGH+60, low: this.BAR_EDIT_MIN_HIGH+170, close: this.BAR_EDIT_MIN_HIGH+160}).push({open: this.BAR_EDIT_MIN_HIGH+100, high:this.BAR_EDIT_MIN_HIGH+90, low: this.BAR_EDIT_MIN_HIGH+200, close: this.BAR_EDIT_MIN_HIGH+190}).push({open: this.BAR_EDIT_MIN_HIGH+200, high:this.BAR_EDIT_MIN_HIGH, low: this.BAR_EDIT_MIN_HIGH+210, close: this.BAR_EDIT_MIN_HIGH+10})},
  }

    GRAPHS_PATTERNS = {
      HeadShouldersTop:{
        name:'Head and Shoulders Top',
        pattern:
        new Immutable.List()  //head shoulders top
        .push(new Immutable.Map({
            x: (this.CANVAS_SIZE.width / 2) - 200,
            y: (this.CANVAS_SIZE.height / 2) + 100,
          }))
        .push(new Immutable.Map({
            x: (this.CANVAS_SIZE.width / 2) - 137.5,
            y: (this.CANVAS_SIZE.height / 2) - 5,
          }))
        .push(new Immutable.Map({
            x: (this.CANVAS_SIZE.width / 2) - 75,
            y: (this.CANVAS_SIZE.height / 2) + 100,
          }))
        .push(new Immutable.Map({ //center
            x: (this.CANVAS_SIZE.width / 2),
            y: (this.CANVAS_SIZE.height / 2) - 100,
          }))
        .push(new Immutable.Map({
            x: (this.CANVAS_SIZE.width / 2) + 75,
            y: (this.CANVAS_SIZE.height / 2) + 100,
          }))
        .push(new Immutable.Map({
            x: (this.CANVAS_SIZE.width / 2) + 137.5,
            y: (this.CANVAS_SIZE.height / 2) - 5,
          }))
        .push(new Immutable.Map({
            x: (this.CANVAS_SIZE.width / 2) + 200,
            y: (this.CANVAS_SIZE.height / 2) + 100,
          }))
        }
      ,
      HeadShouldersBottom:{
        name:'Head and Shoulders Bottom',
        pattern:
        new Immutable.List() //head shoulders bottom
        .push(new Immutable.Map({
          x: (this.CANVAS_SIZE.width / 2) - 200,
          y: (this.CANVAS_SIZE.height / 2) - 100,
        }))
        .push(new Immutable.Map({
            x: (this.CANVAS_SIZE.width / 2) - 137.5,
            y: (this.CANVAS_SIZE.height / 2) + 5,
          }))
        .push(new Immutable.Map({
            x: (this.CANVAS_SIZE.width / 2) - 75,
            y: (this.CANVAS_SIZE.height / 2) - 100,
          }))
        .push(new Immutable.Map({ //center
            x: (this.CANVAS_SIZE.width / 2),
            y: (this.CANVAS_SIZE.height / 2) + 100,
          }))
        .push(new Immutable.Map({
            x: (this.CANVAS_SIZE.width / 2) + 75,
            y: (this.CANVAS_SIZE.height / 2) - 100,
          }))
        .push(new Immutable.Map({
            x: (this.CANVAS_SIZE.width / 2) + 137.5,
            y: (this.CANVAS_SIZE.height / 2) + 5,
          }))
        .push(new Immutable.Map({
            x: (this.CANVAS_SIZE.width / 2) + 200,
            y: (this.CANVAS_SIZE.height / 2) - 100,
          }))
        }
      ,
      DoubleTop:{
        name:'Double Top',
        pattern:
        new Immutable.List() //double top
        .push(new Immutable.Map({
            x: (this.CANVAS_SIZE.width / 2) - 150,
            y: (this.CANVAS_SIZE.height / 2) + 90,
          }))
        .push(new Immutable.Map({
            x: (this.CANVAS_SIZE.width / 2) - 75,
            y: (this.CANVAS_SIZE.height / 2) - 90,
          }))
        .push(new Immutable.Map({   //center
            x: (this.CANVAS_SIZE.width / 2),
            y: (this.CANVAS_SIZE.height / 2) + 90,
          }))
        .push(new Immutable.Map({
            x: (this.CANVAS_SIZE.width / 2) + 75,
            y: (this.CANVAS_SIZE.height / 2) - 90,
          }))
        .push(new Immutable.Map({
            x: (this.CANVAS_SIZE.width / 2) + 150,
            y: (this.CANVAS_SIZE.height / 2) + 90,
          }))
        }
      ,
      DoubleBottom:{
        name:'Double Bottom',
        pattern:
        new Immutable.List() //double bottom
        .push(new Immutable.Map({
          x: (this.CANVAS_SIZE.width / 2) - 150,
          y: (this.CANVAS_SIZE.height / 2) - 90,
        }))
        .push(new Immutable.Map({
            x: (this.CANVAS_SIZE.width / 2) - 75,
            y: (this.CANVAS_SIZE.height / 2) + 90,
          }))
        .push(new Immutable.Map({   //center
            x: (this.CANVAS_SIZE.width / 2),
            y: (this.CANVAS_SIZE.height / 2) - 90,
          }))
        .push(new Immutable.Map({
            x: (this.CANVAS_SIZE.width / 2) + 75,
            y: (this.CANVAS_SIZE.height / 2) + 90,
          }))
        .push(new Immutable.Map({
            x: (this.CANVAS_SIZE.width / 2) + 150,
            y: (this.CANVAS_SIZE.height / 2) - 90,
          }))
        }
      ,
      WedgeUp:{
        name:'Wedge Up',
        pattern:
        new Immutable.List() //wedge up
        .push(new Immutable.Map({
            x: (this.CANVAS_SIZE.width / 2) - 175,
            y: (this.CANVAS_SIZE.height / 2) - 25,
          }))
        .push(new Immutable.Map({
            x: (this.CANVAS_SIZE.width / 2) - 125,
            y: (this.CANVAS_SIZE.height / 2) + 100,
          }))
        .push(new Immutable.Map({
            x: (this.CANVAS_SIZE.width / 2) - 75,
            y: (this.CANVAS_SIZE.height / 2) - 75,
          }))
        .push(new Immutable.Map({   //center 1
            x: (this.CANVAS_SIZE.width / 2) - 25,
            y: (this.CANVAS_SIZE.height / 2) + 20,
          }))
        .push(new Immutable.Map({   //center 2
            x: (this.CANVAS_SIZE.width / 2) + 25,
            y: (this.CANVAS_SIZE.height / 2) - 125,
          }))
        .push(new Immutable.Map({
            x: (this.CANVAS_SIZE.width / 2) + 75,
            y: (this.CANVAS_SIZE.height / 2) - 65,
          }))
        .push(new Immutable.Map({
            x: (this.CANVAS_SIZE.width / 2) + 125,
            y: (this.CANVAS_SIZE.height / 2) - 175,
          }))
        .push(new Immutable.Map({
            x: (this.CANVAS_SIZE.width / 2) + 150,
            y: (this.CANVAS_SIZE.height / 2) - 125,
          }))
        }
        ,
        WedgeDown:{
          name:'Wedge Down',
          pattern:
          new Immutable.List() //wedge down
          .push(new Immutable.Map({
            x: (this.CANVAS_SIZE.width / 2) - 175,
            y: (this.CANVAS_SIZE.height / 2) + 25,
          }))
          .push(new Immutable.Map({
              x: (this.CANVAS_SIZE.width / 2) - 125,
              y: (this.CANVAS_SIZE.height / 2) - 100,
            }))
          .push(new Immutable.Map({
              x: (this.CANVAS_SIZE.width / 2) - 75,
              y: (this.CANVAS_SIZE.height / 2) + 75,
            }))
          .push(new Immutable.Map({   //center 1
              x: (this.CANVAS_SIZE.width / 2) - 25,
              y: (this.CANVAS_SIZE.height / 2) - 20,
            }))
          .push(new Immutable.Map({   //center 2
              x: (this.CANVAS_SIZE.width / 2) + 25,
              y: (this.CANVAS_SIZE.height / 2) + 125,
            }))
          .push(new Immutable.Map({
              x: (this.CANVAS_SIZE.width / 2) + 75,
              y: (this.CANVAS_SIZE.height / 2) + 65,
            }))
          .push(new Immutable.Map({
              x: (this.CANVAS_SIZE.width / 2) + 125,
              y: (this.CANVAS_SIZE.height / 2) + 175,
            }))
          .push(new Immutable.Map({
              x: (this.CANVAS_SIZE.width / 2) + 150,
              y: (this.CANVAS_SIZE.height / 2) + 125,
            }))
          }
          ,
          CupHandleTop:{
            name:'Cup and Handle Top',
            pattern:
            new Immutable.List()  //cup and handle top
          .push(new Immutable.Map({
              x: (this.CANVAS_SIZE.width / 2) - 150,
              y: (this.CANVAS_SIZE.height / 2) - 50,
            }))
          .push(new Immutable.Map({
              x: (this.CANVAS_SIZE.width / 2) - 100,
              y: (this.CANVAS_SIZE.height / 2) + 50,
            }))
          .push(new Immutable.Map({ //center
              x: (this.CANVAS_SIZE.width / 2),
              y: (this.CANVAS_SIZE.height / 2) + 100,
            }))
          .push(new Immutable.Map({
              x: (this.CANVAS_SIZE.width / 2) + 100,
              y: (this.CANVAS_SIZE.height / 2) + 50,
            }))
          .push(new Immutable.Map({
              x: (this.CANVAS_SIZE.width / 2) + 100,
              y: (this.CANVAS_SIZE.height / 2) + 50,
            }))
          .push(new Immutable.Map({
              x: (this.CANVAS_SIZE.width / 2) + 150,
              y: (this.CANVAS_SIZE.height / 2) - 50,
            }))
          .push(new Immutable.Map({
              x: (this.CANVAS_SIZE.width / 2) + 200,
              y: (this.CANVAS_SIZE.height / 2),
            }))
          .push(new Immutable.Map({
              x: (this.CANVAS_SIZE.width / 2) + 250,
              y: (this.CANVAS_SIZE.height / 2) - 50,
            }))
          }
        ,
        CupHandleBottom:{
          name:'Cup and Handle Bottom',
          pattern:
          new Immutable.List()  //cup and handle bottom
          .push(new Immutable.Map({
              x: (this.CANVAS_SIZE.width / 2) - 150,
              y: (this.CANVAS_SIZE.height / 2) + 100,
            }))
          .push(new Immutable.Map({
              x: (this.CANVAS_SIZE.width / 2) - 100,
              y: (this.CANVAS_SIZE.height / 2),
            }))
          .push(new Immutable.Map({ //center
              x: (this.CANVAS_SIZE.width / 2),
              y: (this.CANVAS_SIZE.height / 2) - 50,
            }))
          .push(new Immutable.Map({
              x: (this.CANVAS_SIZE.width / 2) + 100,
              y: (this.CANVAS_SIZE.height / 2),
            }))
          .push(new Immutable.Map({
              x: (this.CANVAS_SIZE.width / 2) + 150,
              y: (this.CANVAS_SIZE.height / 2) + 100,
            }))
          .push(new Immutable.Map({
              x: (this.CANVAS_SIZE.width / 2) + 200,
              y: (this.CANVAS_SIZE.height / 2) + 50,
            }))
          .push(new Immutable.Map({
              x: (this.CANVAS_SIZE.width / 2) + 250,
              y: (this.CANVAS_SIZE.height / 2)  + 100,
            }))
          }
        };

}

export default new StaticVars(); 