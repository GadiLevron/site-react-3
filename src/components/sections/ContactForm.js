import React from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
// import MODAL from 'react-modal';
import './ContactForm.css';
import AppService from "../../services/app.service";
// import CustomizedInputs from './CustomizedInput';
// import { render } from '@testing-library/react';

const useStyles = makeStyles((theme) => ({
    root: {
        '& label.Mui-focused': {
            color: '#83868D',
        },
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '500px',
            // color:'white',
        },
    },
    texts: {
        color:'#83868D',
    },
    input: {
        color:'#83868D',//labels
    },
    MessageTextField: {
        '& label.Mui-focused': {
            color: '#83868D',
        },
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
        },
    }

}));

//className={classes.root}
export default function ContactForm() {
    const classes = useStyles();
    const [name, setName] = React.useState("");
    const [email, setEmail] = React.useState("");
    const [message, setMessage] = React.useState("");
    const [response, setResponse] = React.useState(null);

    const onChangeName = (event) => {
        setName(event.target.value);
    };

    const onChangeEmail = (event) => {
        setEmail(event.target.value);
    };

    const onChangeMessage = (event) => {
        setMessage(event.target.value);
    };
    // const handleChange = (event, newValue) => {
    //     console.log(newValue);
    // };

    const sendMessage = () =>{
        if (message !== ""){
            setResponse("Sending message...");
            const data = {
                name: name,
                email: email,
                message: message
            };

            AppService.sendMessage(data)
              .then(response => {
                if (response.data.success){
                    setResponse("Message was sent. Thank you.");
                    // localThis.props.history.push('/main');  //redirects to another route
                }
                else{
                    setResponse("Sorry, message was not sent. Please try again soon.");
                }
                //console.log(response.data);
              })
              .catch(e => {
                console.log(e);
              });
        }
    }

    return (
        
            <form  autoComplete="off">
            <div className="ContactUsDiv">
                <div className="FormDiv">
                    <div>
                        <div className="FormTitle">
                            Get in Touch
                        </div>
                        <div className="FormMessage">
                            We appreciate suggestions, comments and any other<br/>
                            information we may use to improve our product.
                        </div>
                        <div className="FormInputDiv">
                            <div className={classes.root}>
                                <TextField 
                                    InputProps={{
                                        className: classes.input
                                    }}
                                    InputLabelProps={{
                                        className: classes.input
                                    }}
                                    onChange={onChangeName}
                                    label="Name"
                                    defaultValue=""
                                />
                                <TextField
                                    InputProps={{
                                        className: classes.input
                                    }}
                                    InputLabelProps={{
                                        className: classes.input
                                    }}
                                    onChange={onChangeEmail}
                                    label="Email"
                                    defaultValue="" 
                                />  
                            
                            <TextField 
                                InputProps={{
                                    className: classes.input
                                }}
                                InputLabelProps={{
                                    className: classes.input
                                }}
                                onChange={onChangeMessage}
                                className={classes.MessageTextField} 
                                rows={10} 
                                fullWidth={true} 
                                required={true} 
                                multiline={true} 
                                variant="outlined" 
                                label="Message" 
                                defaultValue="" 
                            />
                            </div>
                        </div>
                    </div>
                    <img className='ContactUsImage'/>
                </div>
                {response === null ? 
                        <Button onClick={sendMessage} className="FormButton">Send</Button>
                        :
                        <div className="FormResponse">{response}</div>
                    }
            </div>
            </form>
        
    );
}
