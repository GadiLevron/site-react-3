import React from 'react';
import './AboutUsSection.css'

export default function AboutUsSection() {

    return (
        <div className="AboutUsDiv">
            <div>
                <div className="AboutUsTitle">
                    About Us
                </div>
                <div className="AboutUsText">
                Technical trading patterns are price patterns that<br/>
                are displayed on a stock, forex or commodity charts.<br/>
                Understanding these patterns is essential for<br/>
                advanced market trading.<br/><br/>
                We, at <span className="AboutUsTextTitleName">PLOTTING ALPHA</span>, have created a powerful<br/>
                search tool to find any trading pattern.<br/> 
                You can draw any pattern you may think of, or you can<br/>
                search for known patterns.<br/>
                This tool can be a powerfull addition to your<br/>
                trading arsenal.
                </div>
            </div>
            <img className='AboutUsImage'/>
        </div>
    );

}