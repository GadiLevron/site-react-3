import React from 'react';
import './DisclaimerSection.css'

export default function DisclaimerComponent() {

    return (
        <div className='Disclaimer_PageDiv'>
            <div className="DisclaimerDiv">
                <div className="DisclaimerTitle">
                    Disclaimer
                </div>
                <div className="DisclaimerText">
                    No Investment Advice Provided. Our content is intended to be used and must be used for informational, educational and entertainment purposes only. It is very important to do your own analysis before making any investment based on your own personal circumstances. Data displayed on the site is 15 minute delayed price.
                    We do not recommend the use of technical analysis as a sole means of trading decisions. We do not recommend making hurried trading decisions. You should always understand that PAST PERFORMANCE IS NOT NECESSARILY INDICATIVE OF FUTURE RESULTS.
                    <br/><br/><div className='Disclaimer_SmallTitle'>Regarding content</div>
                    PlottingAlpha.com cannot and does not represent or guarantee that any of the information on is accurate, reliable, current, complete or appropriate for your needs. Nevertheless, due to various factors — including the inherent possibility of human and mechanical error — the accuracy, completeness, timeliness, results obtained from use, and correct sequencing of information available through our services and website are not and cannot be guaranteed by Plottingalpha.com. 
                    <br/><br/><div className='Disclaimer_SmallTitle'>Regarding investment decisions and trading</div>
                    Decisions to buy, sell, hold or trade in securities, commodities and other investments involve risk and are best made based on the advice of qualified financial professionals. Any trading in securities or other investments involves a risk of substantial losses. The practice of "Day Trading" involves particularly high risks and can cause you to lose substantial sums of money. Before undertaking any trading program, you should consult a qualified financial professional. Please consider carefully whether such trading is suitable for you in light of your financial condition and ability to bear financial risks. Under no circumstances shall we be liable for any loss or damage you or anyone else incurs as a result of any trading or investment activity that you or anyone else engages in based on any information or material you receive through Plottingalpha.com or our services.
                </div>
            </div>
            {/* <img className='DisclaimerImage'/> */}
        </div>
    );

}

/*
We, at <span className="DisclaimerTextTitleName">PLOTTING ALPHA</span>, have created a powerful<br/>
                search tool to find any trading pattern.<br/> 
                You can draw any pattern you may think of, or you can<br/>
                search for known patterns.<br/>
                This tool can be a powerfull addition to your<br/>
                trading arsenal.

*/