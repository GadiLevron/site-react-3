import React from 'react';
import './TitleComponent.css';

export default function TitleCompoenent({redirectPage}) {

    const redirect = () =>{
        redirectPage('/main');
    }

    return (
        <div className="logoDiv" onMouseUp={redirect} style={{cursor:'pointer'}}>
            <img className="LogoImage" alt='title'></img>
            <img className="TitleImage" alt='title'></img>
        </div>
    );
  }