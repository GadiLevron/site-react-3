import React from 'react';
import './MainComponent.css';
import AboutUsSection from '../sections/AboutUsSection';
// import NavbarComponent from '../ui/NavbarComponent';

import AppTopBar from '../ui/AppTopBar';
// import TitleCompoenent from '../TitleComponent';

export default class AboutUsComponent extends React.Component {
    clientID = -1;
    constructor() {
      super();

      this.state = {
        isShowResults : false,
        isShowContactForm: false,
        resultsCounter : 0,
      };
     
      this.redirectPage = this.redirectPage.bind(this);
    }

    redirectPage(page){
      this.props.history.push(page);
    }

    render() {
        return (
          <div className="MainParentDiv">
            <AppTopBar defaultValue={1} redirectPage={this.redirectPage}/>
            <AboutUsSection/>
            {/* <TitleCompoenent/> */}
            {/* <NavbarComponent/> */}

          </div>
        )
    }
}//end class
