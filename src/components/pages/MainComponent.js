import React from 'react';
import './MainComponent.css';
import NavbarComponent from '../ui/NavbarComponent';

import AppTopBar from '../ui/AppTopBar';
// import TitleCompoenent from '../TitleComponent';

export default class MainComponent extends React.Component {
    clientID = -1;
    constructor() {
      super();
      this.contentSpringRef = React.createRef();
      this.searchResultsComponentRef = React.createRef();
      

      this.state = {
        isShowResults : false,
        isShowContactForm: false,
        resultsCounter : 0,
      };
      this.onShowResult = this.onShowResult.bind(this);
      this.clearResults = this.clearResults.bind(this);
      this.onScrollBottom = this.onScrollBottom.bind(this);
      this.setCanUpdateScroll = this.setCanUpdateScroll.bind(this);
      this.showResultsMessage = this.showResultsMessage.bind(this);
      this.setActiveSearch = this.setActiveSearch.bind(this);
      this.redirectPage = this.redirectPage.bind(this);
      
    }

    onShowResult(resultData){
      const curCounter = this.state.resultsCounter;
      
      this.searchResultsComponentRef.current.showResult(resultData);
      this.setState({
        isShowResults : true,
        resultsCounter : curCounter + resultData.length,
      });
      
    }

    clearResults(callbackFunc, sliderValues){
      this.setState({
        isShowResults : false,
        resultsCounter : 0,
      });
      this.searchResultsComponentRef.current.clearResults(callbackFunc, sliderValues);
      
    }

    showResultsMessage(message){
      this.searchResultsComponentRef.current.showMessage(message);
    }

    onScrollBottom(){
      
    }

    setCanUpdateScroll(val){
      this.searchResultsComponentRef.current.setCanUpdateScroll(val);
    }

    setActiveSearch(isSearchActive){
      this.searchResultsComponentRef.current.setActiveSearch(isSearchActive);
    }

    redirectPage(page){
      this.props.history.push(page);
    }

    render() {
        return (
          <div className="MainParentDiv">
            <AppTopBar defaultValue={0} redirectPage={this.redirectPage}/>
            {/* <TitleCompoenent/> */}
            <NavbarComponent/>

          </div>
        )
    }
}//end class
