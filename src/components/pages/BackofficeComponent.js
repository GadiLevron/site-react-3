import React, { Component } from "react";
import BackofficeDataService from "../../services/backoffice.service";
import './BackofficeComponent.css';
//import { login } from '../utils';

export default class BackofficeComponent extends Component {
  constructor(props) {
    super(props);
    this.onChangeSymbols = this.onChangeSymbols.bind(this);
    this.onChangeMarketIndex = this.onChangeMarketIndex.bind(this);
    this.addSymbols = this.addSymbols.bind(this);
    this.removeSymbols = this.removeSymbols.bind(this);
    // this.saveUser = this.saveUser.bind(this);
    
    this.state = {
        symbols: "",
        marketindex: "", 
        message: ""
    };
  }

  componentDidMount() {
    //this.getUser(this.props.match.params.id);
  }

  onChangeSymbols(e) {
    this.setState({
      symbols: e.target.value
    });
  }

  onChangeMarketIndex(e) {
    this.setState({
      marketindex: e.target.value
    });
  }

//   saveUser() {
//     var data = {
//         email: this.state.email,
//         password: this.state.password
//     };

//     UserDataService.create(data)
//       .then(response => {
//         this.setState({
//           id: response.data.id,
//           email: response.data.email,
//           password: response.data.password,
//         });
//         console.log(response.data);
//       })
//       .catch(e => {
//         console.log(e);
//       });
//   }

addSymbols() {
  var data = {
      symbols: this.state.symbols,
      marketindex: this.state.marketindex
  };

  //const localThis = this;

  BackofficeDataService.addSymbols(data)
    .then(response => {
      this.setState({
          message: response.data.message ? response.data.message : "",
      });
    })
    .catch(e => {
      this.setState({
          message: "failed to save to database",
      });
      console.log(e);
    });
}


removeSymbols() {
  var data = {
      marketindex: this.state.marketindex
  };

  BackofficeDataService.removeSymbols(data)
    .then(response => {
      this.setState({
          message: response.data.message ? response.data.message : "",
      });
    })
    .catch(e => {
      this.setState({
          message: e.response.data.message,
      });
      console.log(e);
    });
}

//   updateUser() {
//     UserDataService.update(
//       this.state.currentUser.id,
//       this.state.currentUser
//     )
//       .then(response => {
//         console.log(response.data);
//         this.setState({
//           message: "The user was updated successfully!"
//         });
//       })
//       .catch(e => {
//         console.log(e);
//       });
//   }

//   deleteUser() {    
//     UserDataService.delete(this.state.currentUser.id)
//       .then(response => {
//         console.log(response.data);
//         this.props.history.push('/users')
//       })
//       .catch(e => {
//         console.log(e);
//       });
//   }

  render() {

    return (
        <div className="submit-form">
          <div>
            <div className="form-group">
              <label htmlFor="symbols">Symbols List (seperate by space)</label>
              <input
                type="text"
                className="form-control"
                id="symbols"
                required
                value={this.state.symbols}
                onChange={this.onChangeSymbols}
                name="symbols"
              />
            </div>

            <div className="form-group">
              <label htmlFor="marketindex">Market Index (S&P, NASDAQ etc.)</label>
              <input
                type="text"
                className="form-control"
                id="marketindex"
                required
                value={this.state.marketindex}
                onChange={this.onChangeMarketIndex}
                name="marketindex"
              />
            </div>
            {this.state.message !== "" ? <p>{this.state.message}</p> : <p></p>}
            <button onClick={this.addSymbols} className="btn btn-success">
              Add Symbols
            </button>

            <button onClick={this.removeSymbols} className="btn btn-danger" style={{marginLeft:'20px'}}>
              Remove Symbols
            </button>
          </div>
        
      </div>
    );
  }
}