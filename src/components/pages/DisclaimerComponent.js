import React from 'react';
import './MainComponent.css';
import DisclaimerSection from '../sections/DisclaimerSection';
// import NavbarComponent from '../ui/NavbarComponent';

import AppTopBar from '../ui/AppTopBar';
// import TitleCompoenent from '../TitleComponent';

export default class DisclaimerComponent extends React.Component {
    clientID = -1;
    constructor() {
      super();

      this.state = {
        isShowResults : false,
        isShowContactForm: false,
        resultsCounter : 0,
      };
     
      this.redirectPage = this.redirectPage.bind(this);
    }

    redirectPage(page){
      this.props.history.push(page);
    }

    render() {
        return (
          <div className="MainParentDiv">
            <AppTopBar defaultValue={2} redirectPage={this.redirectPage}/>
            <DisclaimerSection/>
          </div>
        )
    }
}//end class
