import React, { Component } from "react";
import UserDataService from "../../services/user.service";
import './LoginComponent.css';
import { login } from '../../utils';

export default class LoginComponent extends Component {
  constructor(props) {
    super(props);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
    this.checkLogin = this.checkLogin.bind(this);
    // this.saveUser = this.saveUser.bind(this);
    
    this.state = {
        id: null,
        email: "",
        password: "", 
        message: ""
    };
  }

  componentDidMount() {
    //this.getUser(this.props.match.params.id);
  }

  onChangeEmail(e) {
    this.setState({
      email: e.target.value
    });
  }

  onChangePassword(e) {
    this.setState({
        password: e.target.value
    });
  }

//   saveUser() {
//     var data = {
//         email: this.state.email,
//         password: this.state.password
//     };

//     UserDataService.create(data)
//       .then(response => {
//         this.setState({
//           id: response.data.id,
//           email: response.data.email,
//           password: response.data.password,
//         });
//         console.log(response.data);
//       })
//       .catch(e => {
//         console.log(e);
//       });
//   }

  checkLogin() {
    var data = {
        email: this.state.email,
        password: this.state.password
    };

    const localThis = this;

    UserDataService.checkLogin(data)
      .then(response => {
        this.setState({
            message: response.data.message ? response.data.message : "",
        });
        if (response.data.success){
            login();
            localThis.props.history.push('/main');  //redirects to another route
        }
        //console.log(response.data);
      })
      .catch(e => {
        this.setState({
            message: "failed to login, server issue?",
        });
        console.log(e);
      });
  }

//   updateUser() {
//     UserDataService.update(
//       this.state.currentUser.id,
//       this.state.currentUser
//     )
//       .then(response => {
//         console.log(response.data);
//         this.setState({
//           message: "The user was updated successfully!"
//         });
//       })
//       .catch(e => {
//         console.log(e);
//       });
//   }

//   deleteUser() {    
//     UserDataService.delete(this.state.currentUser.id)
//       .then(response => {
//         console.log(response.data);
//         this.props.history.push('/users')
//       })
//       .catch(e => {
//         console.log(e);
//       });
//   }

  render() {

    return (
        <div className="submit-form">
          <div>
            <div className="form-group">
              <label htmlFor="email">Email</label>
              <input
                type="text"
                className="form-control"
                id="email"
                required
                value={this.state.email}
                onChange={this.onChangeEmail}
                name="email"
              />
            </div>

            <div className="form-group">
              <label htmlFor="password">Password</label>
              <input
                type="password"
                className="form-control"
                id="password"
                required
                value={this.state.password}
                onChange={this.onChangePassword}
                name="password"
              />
            </div>
            {this.state.message !== "" ? <p>{this.state.message}</p> : <p></p>}
            <button onClick={this.checkLogin} className="btn btn-success">
              Submit
            </button>
          </div>
        
      </div>
    );
  }
}