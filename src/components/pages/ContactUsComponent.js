import React from 'react';
import './MainComponent.css';
import ContactForm from '../sections/ContactForm';

import AppTopBar from '../ui/AppTopBar';
// import TitleCompoenent from '../TitleComponent';

export default class ContactUsComponent extends React.Component {
    clientID = -1;
    constructor() {
      super();
      this.contentSpringRef = React.createRef();
      this.searchResultsComponentRef = React.createRef();
      

      this.state = {
        isShowResults : false,
        isShowContactForm: false,
        resultsCounter : 0,
      };

      this.redirectPage = this.redirectPage.bind(this);
    }


    redirectPage(page){
      this.props.history.push(page);
    }

    render() {
        return (
          <div className="MainParentDiv">
            <AppTopBar defaultValue={3} redirectPage={this.redirectPage}/>
            <ContactForm/>
            {/* <TitleCompoenent/> */}
            {/* <NavbarComponent/> */}

          </div>
        )
    }
}//end class
