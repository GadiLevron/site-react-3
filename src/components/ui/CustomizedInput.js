import React from 'react';
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";

const styles = {
  root: {
    '& .MuiTextField-root': {
        
        width: '55ch',
    },
    //   marginLeft:"10em",
    background: "black"
  },
  input: {
    color: "white",
    // marginLeft:"10em",
  },

};

function CustomizedInputs(props) {
    const myStyle = styles;
  return (
    <TextField
      defaultValue={props.defaultValue}
      label={props.label}
      variant={props.variant !== undefined ? props.variant : "standard"}
      multiline={props.multiline !== undefined ? props.multiline : false}
      fullWidth={props.fullWidth !== undefined ? props.fullWidth : false}
      required={props.required !== undefined ? props.required : false}
      rows={props.rows !== undefined ? props.rows : 1}
    />
  );
}

CustomizedInputs.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(CustomizedInputs);