import React from 'react';
//import ReactDOM from 'react-dom';
import Immutable from 'immutable';
import './MyCanvas.css';
// import AnimateHeight from 'react-animate-height';
// import { FilledInput } from '@material-ui/core';

const CANVAS_SIZE = {width: 800, height: 400};
const GRIDLINES = {horizontal:20, vertical: 40};
const MAX_ALLOWED_POINTS = 12;

const PATTERNS = [new Immutable.List()  //head shoulders top
                  .push(new Immutable.Map({
                      x: (CANVAS_SIZE.width / 2) - 150,
                      y: (CANVAS_SIZE.height / 2) + 100,
                    }))
                  .push(new Immutable.Map({
                      x: (CANVAS_SIZE.width / 2) - 100,
                      y: (CANVAS_SIZE.height / 2) + 20,
                    }))
                  .push(new Immutable.Map({
                      x: (CANVAS_SIZE.width / 2) - 50,
                      y: (CANVAS_SIZE.height / 2) + 100,
                    }))
                  .push(new Immutable.Map({ //center
                      x: (CANVAS_SIZE.width / 2),
                      y: (CANVAS_SIZE.height / 2) - 100,
                    }))
                  .push(new Immutable.Map({
                      x: (CANVAS_SIZE.width / 2) + 50,
                      y: (CANVAS_SIZE.height / 2) + 100,
                    }))
                  .push(new Immutable.Map({
                      x: (CANVAS_SIZE.width / 2) + 100,
                      y: (CANVAS_SIZE.height / 2) + 20,
                    }))
                  .push(new Immutable.Map({
                      x: (CANVAS_SIZE.width / 2) + 150,
                      y: (CANVAS_SIZE.height / 2) + 100,
                    }))
                ,
                new Immutable.List() //head shoulders bottom
                  .push(new Immutable.Map({
                    x: (CANVAS_SIZE.width / 2) - 150,
                    y: (CANVAS_SIZE.height / 2) - 100,
                  }))
                  .push(new Immutable.Map({
                      x: (CANVAS_SIZE.width / 2) - 100,
                      y: (CANVAS_SIZE.height / 2) - 20,
                    }))
                  .push(new Immutable.Map({
                      x: (CANVAS_SIZE.width / 2) - 50,
                      y: (CANVAS_SIZE.height / 2) - 100,
                    }))
                  .push(new Immutable.Map({ //center
                      x: (CANVAS_SIZE.width / 2),
                      y: (CANVAS_SIZE.height / 2) + 100,
                    }))
                  .push(new Immutable.Map({
                      x: (CANVAS_SIZE.width / 2) + 50,
                      y: (CANVAS_SIZE.height / 2) - 100,
                    }))
                  .push(new Immutable.Map({
                      x: (CANVAS_SIZE.width / 2) + 100,
                      y: (CANVAS_SIZE.height / 2) - 20,
                    }))
                  .push(new Immutable.Map({
                      x: (CANVAS_SIZE.width / 2) + 150,
                      y: (CANVAS_SIZE.height / 2) - 100,
                    }))
                ,
                new Immutable.List() //double top
                  .push(new Immutable.Map({
                      x: (CANVAS_SIZE.width / 2) - 150,
                      y: (CANVAS_SIZE.height / 2) + 90,
                    }))
                  .push(new Immutable.Map({
                      x: (CANVAS_SIZE.width / 2) - 75,
                      y: (CANVAS_SIZE.height / 2) - 90,
                    }))
                  .push(new Immutable.Map({   //center
                      x: (CANVAS_SIZE.width / 2),
                      y: (CANVAS_SIZE.height / 2) + 90,
                    }))
                  .push(new Immutable.Map({
                      x: (CANVAS_SIZE.width / 2) + 75,
                      y: (CANVAS_SIZE.height / 2) - 90,
                    }))
                  .push(new Immutable.Map({
                      x: (CANVAS_SIZE.width / 2) + 150,
                      y: (CANVAS_SIZE.height / 2) + 90,
                    }))
                ,
                new Immutable.List() //double bottom
                  .push(new Immutable.Map({
                    x: (CANVAS_SIZE.width / 2) - 150,
                    y: (CANVAS_SIZE.height / 2) - 90,
                  }))
                  .push(new Immutable.Map({
                      x: (CANVAS_SIZE.width / 2) - 75,
                      y: (CANVAS_SIZE.height / 2) + 90,
                    }))
                  .push(new Immutable.Map({   //center
                      x: (CANVAS_SIZE.width / 2),
                      y: (CANVAS_SIZE.height / 2) - 90,
                    }))
                  .push(new Immutable.Map({
                      x: (CANVAS_SIZE.width / 2) + 75,
                      y: (CANVAS_SIZE.height / 2) + 90,
                    }))
                  .push(new Immutable.Map({
                      x: (CANVAS_SIZE.width / 2) + 150,
                      y: (CANVAS_SIZE.height / 2) - 90,
                    }))
                ,
                new Immutable.List() //wedge up
                  .push(new Immutable.Map({
                      x: (CANVAS_SIZE.width / 2) - 175,
                      y: (CANVAS_SIZE.height / 2) - 25,
                    }))
                  .push(new Immutable.Map({
                      x: (CANVAS_SIZE.width / 2) - 125,
                      y: (CANVAS_SIZE.height / 2) + 100,
                    }))
                  .push(new Immutable.Map({
                      x: (CANVAS_SIZE.width / 2) - 75,
                      y: (CANVAS_SIZE.height / 2) - 75,
                    }))
                  .push(new Immutable.Map({   //center 1
                      x: (CANVAS_SIZE.width / 2) - 25,
                      y: (CANVAS_SIZE.height / 2) + 20,
                    }))
                  .push(new Immutable.Map({   //center 2
                      x: (CANVAS_SIZE.width / 2) + 25,
                      y: (CANVAS_SIZE.height / 2) - 125,
                    }))
                  .push(new Immutable.Map({
                      x: (CANVAS_SIZE.width / 2) + 75,
                      y: (CANVAS_SIZE.height / 2) - 65,
                    }))
                  .push(new Immutable.Map({
                      x: (CANVAS_SIZE.width / 2) + 125,
                      y: (CANVAS_SIZE.height / 2) - 175,
                    }))
                  .push(new Immutable.Map({
                      x: (CANVAS_SIZE.width / 2) + 150,
                      y: (CANVAS_SIZE.height / 2) - 125,
                    }))
                  ,
                  new Immutable.List() //wedge down
                    .push(new Immutable.Map({
                      x: (CANVAS_SIZE.width / 2) - 175,
                      y: (CANVAS_SIZE.height / 2) + 25,
                    }))
                    .push(new Immutable.Map({
                        x: (CANVAS_SIZE.width / 2) - 125,
                        y: (CANVAS_SIZE.height / 2) - 100,
                      }))
                    .push(new Immutable.Map({
                        x: (CANVAS_SIZE.width / 2) - 75,
                        y: (CANVAS_SIZE.height / 2) + 75,
                      }))
                    .push(new Immutable.Map({   //center 1
                        x: (CANVAS_SIZE.width / 2) - 25,
                        y: (CANVAS_SIZE.height / 2) - 20,
                      }))
                    .push(new Immutable.Map({   //center 2
                        x: (CANVAS_SIZE.width / 2) + 25,
                        y: (CANVAS_SIZE.height / 2) + 125,
                      }))
                    .push(new Immutable.Map({
                        x: (CANVAS_SIZE.width / 2) + 75,
                        y: (CANVAS_SIZE.height / 2) + 65,
                      }))
                    .push(new Immutable.Map({
                        x: (CANVAS_SIZE.width / 2) + 125,
                        y: (CANVAS_SIZE.height / 2) + 175,
                      }))
                    .push(new Immutable.Map({
                        x: (CANVAS_SIZE.width / 2) + 150,
                        y: (CANVAS_SIZE.height / 2) + 125,
                      }))
                ];

export default class MyCanvas extends React.Component {
  constructor() {
    super();

    this.state = {
      points: new Immutable.List(),
      isDrawing: false,
      isRightDirection : null,
      isSearchActive:false,
      currentPointIndex: -1,
      savedOriginalPoint: null,
      pointJustCreated: false,
      isHoverOnPoint : false,
    };

    this.handleMouseDown = this.handleMouseDown.bind(this);
    this.handleMouseMove = this.handleMouseMove.bind(this);
    this.handleMouseLeave = this.handleMouseLeave.bind(this);
    this.handleMouseUp = this.handleMouseUp.bind(this);
    this.isPointGoodDirection = this.isPointGoodDirection.bind(this);
  }

  componentDidMount() {
    document.addEventListener("mouseup", this.handleMouseUp);
  }

  componentWillUnmount() {
    document.removeEventListener("mouseup", this.handleMouseUp);
  }

  drawPattern(patternIndex){
    this.setState({
      points: PATTERNS[patternIndex],
      currentPointIndex: -1,
      pointJustCreated: false,
      savedOriginalPoint: null,
      isRightDirection: true,
      isDrawing: false,
    }, this.props.updateNumOfPoints(PATTERNS[patternIndex].size));
  }

  isPointVeryClose(newPoint, lastPoint){
    // for convenience, the user can be within 10 px from the point
    return (newPoint !== null && lastPoint !== null && Math.abs((lastPoint.get('x') - newPoint.get('x'))) <= 15 && Math.abs((lastPoint.get('y') - newPoint.get('y'))) <= 15);
  }

  isPointInBound(newPoint){
    const boundingRect = this.refs.drawArea.getBoundingClientRect();
    return (newPoint.get('x') >= 5 && newPoint.get('x') <= boundingRect.width - 10 && newPoint.get('y') >= 5 && newPoint.get('y') <= boundingRect.height - 10)
  }

  isPointGoodDirection(point, prevPoint){
    if (point !== null && prevPoint !== null && this.state.isRightDirection !== null //check if line is illegal
      && (
      (this.state.isRightDirection && point.get('x') < prevPoint.get('x')) 
      || (!this.state.isRightDirection && point.get('x') > prevPoint.get('x'))
      ))
    {
      return false;
    }

    return true;
  }

  handleMouseLeave(mouseEvent){
    if (!this.state.isDrawing) {
      return;
    }

    if (this.state.pointJustCreated === true){
      this.setState(prevState => ({
        points: prevState.points.pop(),
        isDrawing: false,
        currentPointIndex: -1,
        pointJustCreated: false,
      }), this.props.updateNumOfPoints(this.state.points.size - 1));
    }
    else{
      this.setState(prevState => ({
        points: this.isPointGoodDirection(prevState.currentPointIndex > 0 ? prevState.savedOriginalPoint : prevState.points.get(prevState.currentPointIndex + 1), prevState.currentPointIndex > 0 ? prevState.points.get(prevState.currentPointIndex - 1) : prevState.savedOriginalPoint) ?
                prevState.points.updateIn([prevState.currentPointIndex], 1, point => point = prevState.savedOriginalPoint)
                :
                prevState.points.splice(prevState.currentPointIndex, 1)
                ,
        isDrawing: false,
        currentPointIndex: -1,
        pointJustCreated: false,
      }), this.props.updateNumOfPoints(this.state.points.size));
    }
  }

  clickedOnPoint(newPoint){
    for (let i = 0; i < this.state.points.size; i++){
      const curPoint = this.state.points.get(i);
      if ( Math.abs(curPoint.get('x') - newPoint.get('x')) < 7.5 && Math.abs(curPoint.get('y') - newPoint.get('y')) < 7.5 ){
        return i;        
      }
    }
    return -1;
  }

  handleMouseDown(mouseEvent) {
    console.log("MyCanvas handleMouseDown");
    if (mouseEvent.button !== 0) {
      return;
    }

    // if (this.state.isSearchActive === true){
    //   this.props.onCanvasClicked();
    //   return;
    // }

    const newPoint = this.relativeCoordinatesForEvent(mouseEvent);

    let index = this.clickedOnPoint(newPoint);
    if (this.state.isDrawing === true){
      this.handleMouseLeave(null);
    }
    else{
      if (index > -1){
        this.setState({
          isDrawing: true,
          currentPointIndex: index,
          savedOriginalPoint: newPoint,
        });
      }
      else{
        this.setState(prevState => ({
          points: prevState.points.push(newPoint),
          savedOriginalPoint: newPoint,
          isDrawing: true,
          pointJustCreated: true,
          currentPointIndex: prevState.points.size,
        }), this.props.updateNumOfPoints(this.state.points.size + 1));
      }
    }
  }

  handleMouseMove(mouseEvent) {
    const newPoint = this.relativeCoordinatesForEvent(mouseEvent);
    
    if (!this.state.isDrawing) {
      let index = this.clickedOnPoint(newPoint);
      if (index > -1){
        if (this.state.isHoverOnPoint === false && this.state.currentPointIndex !== index){
          this.setState({
            isHoverOnPoint: true,
            currentPointIndex: index,
          });
        }
      }
      else{
        if (this.state.isHoverOnPoint){
          this.setState({
            isHoverOnPoint: false,
            currentPointIndex: -1,
          });
        }
      }
      return;
    }

    if (!this.isPointInBound(newPoint)){
      this.handleMouseLeave(null);
      return;
    }

    if (this.state.points.size === 1){
      this.setState(prevState =>  ({
        points: prevState.points.push(newPoint),
        currentPointIndex: prevState.points.size,
      }));
    }
    else{
      if (this.state.points.size > 1){
        this.setState(prevState =>  ({
          points: prevState.points.updateIn([prevState.currentPointIndex], 1, point => point = newPoint),
          // points: prevState.points.updateIn([prevState.points.size - 1], 1, point => point = newPoint),
          // currentPointIndex: prevState.points.size - 1,
        }));
      }
    }

    this.props.updateNumOfPoints(this.state.points.size);
  }

  handleMouseUp(mouseEvent) {
    if (!this.state.isDrawing) {
      return;
    }

    // const lastPoint = this.state.points.get(this.state.points.size - 2);
    const prevPoint = this.state.currentPointIndex > 0 ? this.state.points.get(this.state.currentPointIndex - 1) : null;
    const nextPoint = this.state.currentPointIndex > -1 && this.state.currentPointIndex < this.state.points.size - 1 ? this.state.points.get(this.state.currentPointIndex + 1) : null;
    const newPoint = this.relativeCoordinatesForEvent(mouseEvent);
    

    if (this.state.points.size > 1){ 
      if (this.isPointGoodDirection(newPoint, prevPoint) === false || this.isPointGoodDirection(nextPoint, newPoint) === false){
        this.handleMouseLeave(null);
        return;
      }

      if (this.isPointVeryClose(newPoint, prevPoint) || this.isPointVeryClose(nextPoint, newPoint)){
        this.handleMouseLeave(null);
        return;
      }
    }

    if (this.state.points.size > MAX_ALLOWED_POINTS){
      this.handleMouseLeave(null);
      return;
    }

    if (this.isPointInBound(newPoint)){
      // if (this.isPointGoodDirection(newPoint, lastPoint) === true){
        this.setState(prevState =>  ({
          // points: prevState.points.push(newPoint),
          currentPointIndex: -1,
          isDrawing: false,
          pointJustCreated: false,
          isRightDirection : (prevState.isRightDirection === null && prevState.points.size > 1 ? (prevPoint.get('x') < newPoint.get('x') ? true : false) : prevState.isRightDirection),
        }));
      // }
      
    }

    this.props.updateNumOfPoints(this.state.points.size);
  }

  relativeCoordinatesForEvent(mouseEvent) {
    const boundingRect = this.refs.drawArea.getBoundingClientRect();
    return new Immutable.Map({
      x: mouseEvent.clientX - boundingRect.left,
      y: mouseEvent.clientY - boundingRect.top,
    });
  }

  getPattern() {
    let retArr = [];
    for (let i = 0; i < this.state.points.size; i++){
      retArr.push({x: this.state.points.get(i).get('x'), y: CANVAS_SIZE.height - this.state.points.get(i).get('y')});
    }
    return retArr;
    // return this.state.points;  old version - server calcs canvasheight - y for each point
  }

  getDrawRightDirection() {
    return this.state.isRightDirection;
  }

  clearCanvas() {
    this.props.updateNumOfPoints(0);
    this.setState({
        points: new Immutable.List(),
        isDrawing : false,
        isRightDirection : null,
        isHoverOnPoint: false,
      });
  }

  undoLastPoint(){
    this.props.updateNumOfPoints(this.state.points.size > 0 ?  this.state.points.size - 1 : 0);
    this.setState(prevState =>  ({
        points: prevState.points.pop(),
        isDrawing: false,
        isRightDirection : prevState.isRightDirection,
    }));
  }

  drawGrid(){

  }

  setSearchActive(isActive){
    this.setState({
      isSearchActive:isActive,
    });
  }

  render() {
    return (
      <div  className="myCanvas" >
          <button className="canvasClearButton" onClick={() => this.clearCanvas()}>
            <img alt='' data-tip="Clear Canvas"></img>
          </button>
          <button className="canvasUndoButton" onClick={() => this.undoLastPoint()}>
            <img alt='' data-tip="Undo Last Line"></img>
          </button>
       {/* 
       style={this.state.isSearchActive ? {pointerEvents: "none"} : {}}
       <AnimateHeight
              duration={ 500 }
              height={ this.state.height }
            > */}
          {/* <svg width="100" height="100">
          <circle r="25" cx="50" cy="50"/>
          </svg> */}
        <div
            className={"drawArea" + (this.state.isHoverOnPoint ? (this.state.isDrawing ? " grabItem" : " selectItem") : "")}
            ref="drawArea"
            onMouseDown={this.handleMouseDown}
            onMouseMove={this.handleMouseMove}
            onMouseLeave={this.handleMouseLeave}
        >
        <Drawing points={this.state.points} currentPointIndex={this.state.currentPointIndex}/>
        
         {/* <circle cx={10} cy={10} r={100} fill="red" />
            <Drawing lines={this.state.lines} />
            <drawCircle lines={this.state.lines}/> */}
        </div>
        
        {/* <div>
        
            <button className="canvasClearButton" onClick={() => this.clearCanvas()}>Clear</button>
        </div>
        <div>
            <button className="undoButton" onClick={() => this.undoLine()}>Undo</button>
        </div> */}
        {/* </AnimateHeight> */}
      </div>
      
      
    );
  }//end render
}

// function drawCircle({ lines }) {
//   return (
//     <svg className="circlePoint">
//       {lines.map((line, index) => (
//         <circle cx={line.get('x')} cy={line.get('y')} r={100} fill="red" />
//         ))}
     
//     </svg>
//   );
// }

function RedCircle({point}){
  return(
    <circle className="redCircle" r="5" cx={point.get('x')} cy={point.get('y')}/>
  );
}

function EditCircle({point}){
  return(
    <circle className="editCircle" r="5" cx={point.get('x')} cy={point.get('y')} />
  );
}

function GridLineHorizontal({lineNumber}){
  return(
    <line className="gridLine" x1={0} y1={(lineNumber + 1) * CANVAS_SIZE.height / GRIDLINES.horizontal} x2={CANVAS_SIZE.width} y2={(lineNumber + 1) * CANVAS_SIZE.height / GRIDLINES.horizontal}/>
  );
}

function GridLineVertical({lineNumber}){
  return(
    <line className="gridLine" x1={(lineNumber + 1) * CANVAS_SIZE.width / GRIDLINES.vertical} y1={0} x2={(lineNumber + 1) * CANVAS_SIZE.width / GRIDLINES.vertical} y2={CANVAS_SIZE.height}/>
  );
}

function Drawing({ points, currentPointIndex }) {
  return (
    <svg className="drawing">
      {/* <linearGradient id="linear-gradient" gradientUnits="userSpaceOnUse" x1="1041.6901" y1="169.485" x2="1383.9301" y2="169.485" gradientTransform="matrix(1 0 0 -1 -761.14 398.97)">
      <stop offset="0%" stopColor="var(--color-stop-1)" />
      <stop offset="50%" stopColor="var(--color-stop-2)" />
      <stop offset="100%" stopColor="var(--color-stop-3)" />
      </linearGradient> */}
      <linearGradient id="grid-gradient" gradientUnits="userSpaceOnUse" x1="0%" y1="0%" x2="100%" y2="100%" gradientTransform="matrix(1 0 0 -1 -761.14 398.97)">
        <stop offset="0%" stopColor="var(--color-stop-1)" />
        <stop offset="100%" stopColor="var(--color-stop-2)" />
        {/* <rect x="10" y="10" width="600" height="200" fill="url(#grid-gradient)" /> */}
      </linearGradient>
      {/* <filter id="shadow">
        <feDropShadow dx="var(--dx)" dy="var(--dy)" stdDeviation="var(--stdDeviation)"/>
      </filter> */}
      <filter id="dropshadow" x="-40%" y="-40%" width="180%" height="180%" filterUnits="userSpaceOnUse">
        <feGaussianBlur in="SourceAlpha" stdDeviation="1"/>
        <feOffset dx="2" dy="2" result="offsetblur"/> 
        <feOffset dx="-2" dy="-2" result="offsetblur"/>
        <feMerge> 
          <feMergeNode/>
          <feMergeNode in="SourceGraphic"/>
          <feMergeNode in="SourceGraphic"/>
        </feMerge>
    </filter>
      {/* <symbol id="redCircle">
        <circle r="25" cx="200" cy="100"/>
      </symbol> */}
      {[...Array(GRIDLINES.vertical)].map((x, i) =>
        <GridLineVertical key={i} lineNumber={i}/>
      )}
      {[...Array(GRIDLINES.horizontal)].map((x, i) =>
        <GridLineHorizontal key={i} lineNumber={i}/>
      )}
      {points.map((point, index) => (
        index > 0 ? 
          <DrawingLine key={index} point={point} prevPoint={points.get(index - 1)} />
          :
          null
      ))}
      {points.map((point, index) => (
          currentPointIndex === index ?
            <EditCircle key={index} point={point} />
            :
            <RedCircle  style={{filter:"url(#dropshadow)"}} key={index} point={point} />
      ))}
    </svg>
  );
}

function DrawingLine({ point, prevPoint }) {
  let line = [point, prevPoint];
  const pathData = "M " + //prevPoint.get('x') + " " + prevPoint.get('y') + point.get('x') + " " + point.get('y') + " L ";
    line
      .map(p => {
        return `${p.get('x')} ${p.get('y')}`;
      })
      .join(" L ");

  return <path className="path" d={pathData} />;
}




/*

{ <use href="#redCircle"/> }
*/