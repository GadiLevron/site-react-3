import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Slider from '@material-ui/core/Slider';
// import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
// import { StylesProvider } from "@material-ui/core/styles";
import './MinBarSlider.css';
const useStyles = makeStyles((theme) => ({
  root: {
    width: 155 + theme.spacing(3) * 2,
    color:'#ffffff',
    textAlign:'Left',
    display:'block',
  },
  margin: {
    height: theme.spacing(3),
  },
}));

function ValueLabelComponent(props) {
  const { children, open, value } = props;

  return (
    <Tooltip open={open} enterTouchDelay={0} placement="top" title={value}>
      {children}
    </Tooltip>
  );
}

ValueLabelComponent.propTypes = {
  children: PropTypes.element.isRequired,
  open: PropTypes.bool.isRequired,
  value: PropTypes.number.isRequired,
};

const iOSBoxShadow =
  '0 3px 1px rgba(0,0,0,0.1),0 4px 8px rgba(0,0,0,0.13),0 0 0 1px rgba(0,0,0,0.02)';

const IOSSlider = withStyles({
  root: {
    color: '#3880ff',
    height: 2,
    padding: '15px 0',
    marginLeft:'35px',
    marginTop:'25px',
    // display:'flex',
    // flexDirection:'column',
  },
  thumb: {
    height: 35,
    width: 35,
    backgroundColor: '#fff',
    boxShadow: iOSBoxShadow,
    marginTop: -17.5,
    marginLeft: -17.5,
    '&:focus, &:hover, &$active': {
      boxShadow: '0 3px 1px rgba(0,0,0,0.1),0 4px 8px rgba(0,0,0,0.3),0 0 0 1px rgba(0,0,0,0.02)',
      // Reset on touch devices, it doesn't add specificity
      '@media (hover: none)': {
        boxShadow: iOSBoxShadow,
      },
    },
  },
  active: {},
  valueLabel: {
    fontSize:20,
    fontFamily:'Arial',
    fontWeight:'bold',
    left: 'calc(0% + 2px)',
    top: 'calc(50% - 5px)',
    '& *': {
      background: 'transparent',
      color: '#3C3E45',
    },
  },
  track: {
    height: 0,
  },
  rail: {
    height: 4,
    opacity: 1,
    backgroundColor: '#CDCFD6',
    boxShadow:'inset 0px -1px 0px #CECFD7, inset 0px 4px 4px rgba(0, 0, 0, 0.25)',
    borderRadius:'30px',
  },
  mark: {
    backgroundColor: '#bfbfbf',
    height: 8,
    width: 1,
    marginTop: -3,
  },
  markActive: {
    opacity: 1,
    backgroundColor: 'currentColor',
  },
})(Slider);

export default function MinBarSlider({onChangeFunc, message, min, max, marks, textSuffix, defaultValue}) {
  const classes = useStyles();
  const [value, setValue] = React.useState([parseInt(defaultValue !== undefined ? defaultValue : min)]);
  const handleChange = (event, newValue) => {
    setValue(newValue);
    onChangeFunc(newValue);
  };

  function valuetext(value) {
    return `${value}` + textSuffix;
  }

  onChangeFunc(value[0] < min ? [min] : value);
  return (
    <div className={classes.root}>
      {/* <StylesProvider injectFirst> */}
        <IOSSlider
          // aria-label="ios slider"
          // defaultValue={0}
          value={value[0] < min ? [min] : value}
          valueLabelDisplay="on"
          onChange={handleChange}
          aria-labelledby="day-label"
          getAriaValueText={valuetext}
          valueLabelFormat={valuetext}
          step={1}
          marks={marks}
          min={parseInt(min)}
          max={parseInt(max)}
          // valueLabelDisplay="auto"
        />
        <div className='MinBarSliderText'>
          {message}
        </div>
      {/* </StylesProvider> */}
    </div>
  );
}
