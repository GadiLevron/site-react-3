import React from "react";
import PropTypes from "prop-types";
import StaticVars from '../../StaticVars';

export const GraphTitleIcon = (props) => {
    return(
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid meet" viewBox="0 0 640 640" width="640" height="640"
            style={{
                        position:"absolute",
                        marginLeft: props.position !== undefined ? props.position.x : 0,
                        marginTop: props.position !== undefined ? props.position.y : 0,
                        }}
            transform= {props.scale !== undefined ? "scale(" + props.scale + " " + props.scale + ")" : ""}
        >
            <defs>
                {/* line */}<path d="M350 150L250 350L200 250L99.56 450" id="aKxmEcBmY"></path>

                {/* circle 1 */}<path d="M115 445C115 453.28 108.28 460 100 460C91.72 460 85 453.28 85 445C85 436.72 91.72 430 100 430C108.28 430 115 436.72 115 445Z" id="d81hxC3Jzx"></path>
                {/* circle 1 */}<path d="M106.55 444.83C106.55 448.24 103.62 451 100 451C96.38 451 93.45 448.24 93.45 444.83C93.45 441.42 96.38 438.66 100 438.66C103.62 438.66 106.55 441.42 106.55 444.83Z" id="cJ3PBEhhH"></path>

                {/* circle 2 */}<path d="M212.08 248.39C212.08 256.67 205.36 263.39 197.08 263.39C188.8 263.39 182.08 256.67 182.08 248.39C182.08 240.12 188.8 233.39 197.08 233.39C205.36 233.39 212.08 240.12 212.08 248.39Z" id="b4v7gG48O"></path>
                {/* circle 2 */}<path d="M203.1 247.28C203.1 250.69 200.17 253.45 196.55 253.45C192.94 253.45 190 250.69 190 247.28C190 243.88 192.94 241.11 196.55 241.11C200.17 241.11 203.1 243.88 203.1 247.28Z" id="f3hgC8vt7l"></path>
                
                {/* circle 3 */}<path d="M265 353.43C265 361.71 258.28 368.43 250 368.43C241.72 368.43 235 361.71 235 353.43C235 345.15 241.72 338.43 250 338.43C258.28 338.43 265 345.15 265 353.43Z" id="ehpVvR6dO"></path>                
                {/* circle 3 */}<path d="M256.55 353.22C256.55 356.63 253.62 359.39 250 359.39C246.38 359.39 243.45 356.63 243.45 353.22C243.45 349.82 246.38 347.05 250 347.05C253.62 347.05 256.55 349.82 256.55 353.22Z" id="b248MQ0w0w"></path>

                {/* circle 4 */}<path d="M363.3 150C363.3 158.28 356.58 165 348.3 165C340.02 165 333.3 158.28 333.3 150C333.3 141.72 340.02 135 348.3 135C356.58 135 363.3 141.72 363.3 150Z" id="c2HkZ9m7TO"></path>
                {/* circle 4 */}<path d="M355 150C355 153.41 352.06 156.17 348.45 156.17C344.83 156.17 341.9 153.41 341.9 150C341.9 146.59 344.83 143.83 348.45 143.83C352.06 143.83 355 146.59 355 150Z" id="f13SJEra5T"></path>

            </defs>
            {/* line */}
            <use href="#aKxmEcBmY" opacity="1" fillOpacity="0" stroke="#01103f" strokeWidth="30" strokeOpacity="1"></use>

            {/* circle 1 */}
            <use href="#d81hxC3Jzx" opacity="1" fillOpacity="0" stroke="#01103f" strokeWidth="50" strokeOpacity="1"></use>
            <use href="#cJ3PBEhhH" opacity="1" fillOpacity="0" stroke="#ffffff" strokeWidth="17" strokeOpacity="1"></use>
            
            {/* circle 2 */}
            <use href="#b4v7gG48O" opacity="1" fillOpacity="0" stroke="#01103f" strokeWidth="50" strokeOpacity="1"></use>
            <use href="#f3hgC8vt7l" opacity="1" fillOpacity="0" stroke="#ffffff" strokeWidth="17" strokeOpacity="1"></use>
            {/* circle 3 */}
            <use href="#ehpVvR6dO" opacity="1" fillOpacity="0" stroke="#01103f" strokeWidth="50" strokeOpacity="1"></use>
            <use href="#b248MQ0w0w" opacity="1" fillOpacity="0" stroke="#ffffff" strokeWidth="17" strokeOpacity="1"></use>
            {/* circle 4 */}
            <use href="#c2HkZ9m7TO" opacity="1" fillOpacity="0" stroke="#01103f" strokeWidth="50" strokeOpacity="1"></use>
            <use href="#f13SJEra5T" opacity="1" fillOpacity="0" stroke="#ffffff" strokeWidth="17" strokeOpacity="1"></use>
            {/*  */}
            
        </svg>
    );
}

export const CandleTitleIcon = (props) => {
    return(
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid meet" viewBox="0 0 640 640" width="640" height="640"
            style={{
                    position:"absolute",
                    marginLeft: props.position !== undefined ? props.position.x : 0,
                    marginTop: props.position !== undefined ? props.position.y : 0,
                    }}
            transform= {props.scale !== undefined ? "scale(" + props.scale + " " + props.scale + ")" : ""}
        >
            <defs>
                <path d="M122.32 298.88L242.25 298.88L242.25 533.9L122.32 533.9L122.32 298.88Z" id="c5vrP2MK2w"></path>
                <path d="M170.29 533.9L194.28 533.9L194.28 585.9L170.29 585.9L170.29 533.9Z" id="b3XtDQVRBl"></path>
                <path d="M170.29 246.89L194.28 246.89L194.28 298.88L170.29 298.88L170.29 246.89Z" id="br2SdxQFS"></path>
                <path d="M357.47 135.44L477.4 135.44L477.4 370.46L357.47 370.46L357.47 135.44Z" id="d5H3ynsd78"></path>
                <path d="M405.44 370.46L429.43 370.46L429.43 422.45L405.44 422.45L405.44 370.46Z" id="b1oDxneU"></path>
                <path d="M405.44 83.45L429.43 83.45L429.43 135.44L405.44 135.44L405.44 83.45Z" id="dxQdPOQok"></path>
                </defs>
                <use href="#c5vrP2MK2w" opacity="1" fill="#01103f" fillOpacity="1"></use>
                <use href="#c5vrP2MK2w" opacity="1" fillOpacity="0" stroke="#01103f" strokeWidth="1" strokeOpacity="1"></use>
                <use href="#b3XtDQVRBl" opacity="1" fill="#01103f" fillOpacity="1"></use>
                <use href="#b3XtDQVRBl" opacity="1" fillOpacity="0" stroke="#01103f" strokeWidth="12" strokeOpacity="1"></use>
                <use href="#br2SdxQFS" opacity="1" fill="#01103f" fillOpacity="1"></use>
                <use href="#br2SdxQFS" opacity="1" fillOpacity="0" stroke="#01103f" strokeWidth="12" strokeOpacity="1"></use>
                <use href="#d5H3ynsd78" opacity="1" fill="#ffffff" fillOpacity="1"></use>
                <use href="#d5H3ynsd78" opacity="1" fillOpacity="0" stroke="#01103f" strokeWidth="12" strokeOpacity="1"></use>
                <use href="#b1oDxneU" opacity="1" fill="#ffffff" fillOpacity="1"></use>
                <use href="#b1oDxneU" opacity="1" fillOpacity="0" stroke="#01103f" strokeWidth="12" strokeOpacity="1"></use>
                <use href="#dxQdPOQok" opacity="1" fill="#ffffff" fillOpacity="1"></use>
                <use href="#dxQdPOQok" opacity="1" fillOpacity="0" stroke="#01103f" strokeWidth="12" strokeOpacity="1"></use> 
            </svg>
    );
}

export const AddGreenBarIcon = (props) => {
    return(
        <svg width="17" height="30" viewBox="0 0 17 30" fill="none" xmlns="http://www.w3.org/2000/svg"
            transform= {props.scale !== undefined ? "scale(" + props.scale + " " + props.scale + ")" : ""}
            style={{
                    // position:"absolute",
                    marginLeft:(props.marginLeft !== undefined ? props.marginLeft : "unset"),
                    marginRight:(props.marginRight !== undefined ? props.marginRight : "unset"),
                    marginTop:(props.marginTop !== undefined ? props.marginTop : "unset"),
                    
                }}
        >
        <path d="M4 1C4 0.447716 4.44772 0 5 0C5.55228 0 6 0.447715 6 1V29C6 29.5523 5.55228 30 5 30C4.44772 30 4 29.5523 4 29V1Z" fill="url(#paint0_radial)"/>
        <path d="M0 4L10 4V26H0V4Z" fill="url(#paint1_radial)"/>
        <path d="M6.25 17.25H7.75L7.75 18.75C7.75 19.9926 8.75736 21 10 21C11.2426 21 12.25 19.9926 12.25 18.75V17.25H13.75C14.9926 17.25 16 16.2426 16 15C16 13.7574 14.9926 12.75 13.75 12.75H12.25V11.25C12.25 10.0074 11.2426 9 10 9C8.75736 9 7.75 10.0074 7.75 11.25L7.75 12.75H6.25C5.00736 12.75 4 13.7574 4 15C4 16.2426 5.00736 17.25 6.25 17.25Z" fill="url(#paint2_radial)" stroke="white" strokeWidth="2"/>
        <defs>
        <radialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(5.6125 -4.7093) rotate(91.7164) scale(50.0806 2.57207)">
        <stop stopColor="#1CEFCC"/>
        <stop offset="1" stopColor="#15E0A7"/>
        </radialGradient>
        <radialGradient id="paint1_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(8.0625 0.546511) rotate(101.547) scale(37.4676 12.6057)">
        <stop stopColor="#1CEFCC"/>
        <stop offset="1" stopColor="#15E0A7"/>
        </radialGradient>
        <radialGradient id="paint2_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(13.0625 8.43023) rotate(114.203) scale(18.2941 11.7352)">
        <stop stopColor="#1CEFCC"/>
        <stop offset="1" stopColor="#15E0A7"/>
        </radialGradient>
        </defs>
        </svg>
    );
}

export const AddGreyBarIcon = (props) => {
    return(
        <svg width="17" height="30" viewBox="0 0 17 30" fill="none" xmlns="http://www.w3.org/2000/svg"
            transform= {props.scale !== undefined ? "scale(" + props.scale + " " + props.scale + ")" : ""}
            style={{
                    // position:"absolute",
                    marginLeft:(props.marginLeft !== undefined ? props.marginLeft : "unset"),
                    marginRight:(props.marginRight !== undefined ? props.marginRight : "unset"),
                    marginTop:(props.marginTop !== undefined ? props.marginTop : "unset"),
                    
                }}
        >
        <path d="M4 1C4 0.447716 4.44772 0 5 0C5.55228 0 6 0.447715 6 1V29C6 29.5523 5.55228 30 5 30C4.44772 30 4 29.5523 4 29V1Z" fill="url(#paint0_radial_grey)"/>
        <path d="M0 4L10 4V26H0V4Z" fill="url(#paint1_radial_grey)"/>
        <path d="M6.25 17.25H7.75L7.75 18.75C7.75 19.9926 8.75736 21 10 21C11.2426 21 12.25 19.9926 12.25 18.75V17.25H13.75C14.9926 17.25 16 16.2426 16 15C16 13.7574 14.9926 12.75 13.75 12.75H12.25V11.25C12.25 10.0074 11.2426 9 10 9C8.75736 9 7.75 10.0074 7.75 11.25L7.75 12.75H6.25C5.00736 12.75 4 13.7574 4 15C4 16.2426 5.00736 17.25 6.25 17.25Z" fill="url(#paint2_radial_grey)" stroke="white" strokeWidth="2"/>
        <defs>
        <radialGradient id="paint0_radial_grey" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(5.6125 -4.7093) rotate(91.7164) scale(50.0806 2.57207)">
        <stop stopColor="#83868D"/>
        <stop offset="1" stopColor="#83868D"/>
        </radialGradient>
        <radialGradient id="paint1_radial_grey" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(8.0625 0.546511) rotate(101.547) scale(37.4676 12.6057)">
        <stop stopColor="#83868D"/>
        <stop offset="1" stopColor="#83868D"/>
        </radialGradient>
        <radialGradient id="paint2_radial_grey" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(13.0625 8.43023) rotate(114.203) scale(18.2941 11.7352)">
        <stop stopColor="#83868D"/>
        <stop offset="1" stopColor="#83868D"/>
        </radialGradient>
        </defs>
        </svg>
    );
}

export const AddRedBarIcon = (props) => {
    return(
        <svg width="17" height="30" viewBox="0 0 17 30" fill="none" xmlns="http://www.w3.org/2000/svg"
            transform= {props.scale !== undefined ? "scale(" + props.scale + " " + props.scale + ")" : ""}
            style={{
                    // position:"absolute",
                    marginLeft:(props.marginLeft !== undefined ? props.marginLeft : "unset"),
                    marginRight:(props.marginRight !== undefined ? props.marginRight : "unset"),
                    marginTop:(props.marginTop !== undefined ? props.marginTop : "unset"),
                    
                }}
        >
        <path d="M4 1C4 0.447716 4.44772 0 5 0C5.55228 0 6 0.447715 6 1V29C6 29.5523 5.55228 30 5 30C4.44772 30 4 29.5523 4 29V1Z" fill="url(#paint0_radial_red)"/>
        <path d="M0 4L10 4V26H0V4Z" fill="url(#paint1_radial_red)"/>
        <path d="M6.25 17.25H7.75L7.75 18.75C7.75 19.9926 8.75736 21 10 21C11.2426 21 12.25 19.9926 12.25 18.75V17.25H13.75C14.9926 17.25 16 16.2426 16 15C16 13.7574 14.9926 12.75 13.75 12.75H12.25V11.25C12.25 10.0074 11.2426 9 10 9C8.75736 9 7.75 10.0074 7.75 11.25L7.75 12.75H6.25C5.00736 12.75 4 13.7574 4 15C4 16.2426 5.00736 17.25 6.25 17.25Z" fill="url(#paint2_radial_red)" stroke="white" strokeWidth="2"/>
        <defs>
        <radialGradient id="paint0_radial_red" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(5.6125 -4.7093) rotate(91.7164) scale(50.0806 2.57207)">
        <stop stopColor="#EF4444"/>
        <stop offset="1" stopColor="#F33939"/>
        </radialGradient>
        <radialGradient id="paint1_radial_red" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(8.0625 0.546511) rotate(101.547) scale(37.4676 12.6057)">
        <stop stopColor="#EF4444"/>
        <stop offset="1" stopColor="#F33939"/>
        </radialGradient>
        <radialGradient id="paint2_radial_red" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(13.0625 8.43023) rotate(114.203) scale(18.2941 11.7352)">
        <stop stopColor="#EF4444"/>
        <stop offset="1" stopColor="#F33939"/>
        </radialGradient>
        </defs>
        </svg>
    );
}

export const GraphSimplePoint = ({point}) => {
    return(
        <g>
            <g filter="url(#filter0_d)">
                <circle cx={point.get('x')} cy={point.get('y')} r="12" fill="url(#paint0_radial)"/>
            </g>
            <circle cx={point.get('x')} cy={point.get('y')} r="8" fill="white"/>
            <defs>
                <filter id="filter0_d" x="0" y="0" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
                    <feFlood floodOpacity="0" result="BackgroundImageFix"/>
                    <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
                    <feOffset dy="4"/>
                    <feGaussianBlur stdDeviation="2"/>
                    <feColorMatrix type="matrix" values="0 0 0 0 0.435294 0 0 0 0 0.443137 0 0 0 0 0.505882 0 0 0 0.25 0"/>
                    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
                    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
                </filter>
                <radialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(29.65 -2.7907) rotate(130.124) scale(45.6205 45.8234)">
                    <stop stopColor="#1E568E"/>
                    <stop offset="1" stopColor="#142C4E"/>
                </radialGradient>
            </defs>
        </g>
    );
}

export const GraphSelectedPoint = (props) => {
    const backgroundSize = StaticVars.GRAPH_SELECTED_BACKGROUND_SIZE;
    return(
        <g >
            {props.isHideMenu ? 
                null
                :
                <rect width={backgroundSize.width} height={backgroundSize.height} x={props.point.get('x') - backgroundSize.width/2} y={props.point.get('y') - 2*backgroundSize.height/3} rx="31" fill="#ECF0F4b4"/>
            }
            <g filter="url(#filter0_d)">
                <circle cx={props.point.get('x')} cy={props.point.get('y')} r="12" fill="url(#paint0_radial)"/>
            </g>
            <g filter="url(#filter1_d)">
                <circle cx={props.point.get('x')} cy={props.point.get('y')} r="8" fill="white"/>
            </g>
            <circle cx={props.point.get('x')} cy={props.point.get('y')} r="6" fill="url(#paint1_radial)"/>
            <defs>
                <filter id="filter0_d" x="0" y="0" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
                    <feFlood floodOpacity="0" result="BackgroundImageFix"/>
                    <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
                    <feOffset dy="4"/>
                    <feGaussianBlur stdDeviation="2"/>
                    <feColorMatrix type="matrix" values="0 0 0 0 0.435294 0 0 0 0 0.443137 0 0 0 0 0.505882 0 0 0 0.25 0"/>
                    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
                    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
                </filter>
                <filter id="filter1_d" x="4" y="4" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
                    <feFlood floodOpacity="0" result="BackgroundImageFix"/>
                    <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
                    <feOffset dy="4"/>
                    <feGaussianBlur stdDeviation="2"/>
                    <feColorMatrix type="matrix" values="0 0 0 0 0.434844 0 0 0 0 0.441776 0 0 0 0 0.504167 0 0 0 0.25 0"/>
                    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
                    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
                </filter>
                <radialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(29.65 -2.7907) rotate(130.124) scale(45.6205 45.8234)">
                    <stop stopColor="#1E568E"/>
                    <stop offset="1" stopColor="#142C4E"/>
                </radialGradient>
                <radialGradient id="paint1_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(22.825 4.60465) rotate(130.124) scale(22.8103 22.9117)">
                    <stop stopColor="#1E568E"/>
                    <stop offset="1" stopColor="#142C4E"/>
                </radialGradient>
            </defs>
        </g>
    );
}

export const RedCircleIcon = (color) => {
    return(
        <svg width="20" height="20" style={{position:"absolute"}}>
            <circle r="10" cx="10" cy="10" fill="red" />
        </svg>
    );
}

export const DefaultBarIcon = (props) => {
    return(
        <div  style={{position:"absolute"}}>
            <svg width={StaticVars.DEFAULT_BAR_WIDTH} height="100" xmlns="http://www.w3.org/2000/svg"
                transform= {props.scale !== undefined ? "scale(" + props.scale + " " + props.scale + ")" : ""}
                style={{
                    position:"absolute",
                    marginLeft:(props.marginLeft !== undefined ? props.marginLeft : "unset"),
                    marginRight:(props.marginRight !== undefined ? props.marginRight : "unset"),
                    marginTop:(props.marginTop !== undefined ? props.marginTop : "unset"),
                    
                }}>
                <defs>
                    <radialGradient id="red-radial" cx="50%" cy="50%" r="50%" fx="50%" fy="50%">
                        <stop offset="0%" stopColor="rgb(255,28,3)" stopOpacity="1" />
                        <stop offset="100%" stopColor="rgb(184,41,31)" stopOpacity="1" />
                    </radialGradient>
                    <radialGradient id="green-radial" cx="50%" cy="50%" r="50%" fx="50%" fy="50%">
                        <stop offset="0%" stopColor="rgb(3,255,28)" stopOpacity="1" />
                        <stop offset="100%" stopColor="rgb(50,179,63)" stopOpacity="1" />
                    </radialGradient>
                </defs>
                <path d="M 15 10 V 25 H 1 V 75 H 30 V 25 H 15 M 15 75 V 90" 
                    stroke={props.stroke !== undefined ? props.stroke : "#3e9ae0"}
                    fill={props.fill !== undefined ? props.fill : "none"}
                    strokeWidth={props.strokeWidth !== undefined ? props.strokeWidth : "1"}    
                />
            </svg>
        </div>
    );
    
}

export const EditBarTrash = (props) => {
    const trashTransform = (props.scale !== undefined ? "scale(" + props.scale + " " + props.scale + ")" : "");
    // const trans = "scale(0.7 0.7)";
    return (
      <svg xmlns="http://www.w3.org/2000/svg" style={{position:'absolute', marginLeft: props.position.x, marginTop: props.position.y, width: '30', height:'30', position:"absolute",'zIndex':1 }}>
        <g  
          id="trash"
          transform={trashTransform}
          cursor="pointer"
          onMouseDown={props.onMouseDown}
        >
          <path d="M26.2887 4.12399H22.2507C22.0858 1.2938 21.0145 0 17.4709 0H11.9494C8.65303 0 7.5817 1.05121 7.49929 4.12399H3.79085C1.73061 4.12399 0 5.74124 0 7.84367V8.08625C0 9.94609 1.40097 11.4825 3.21398 11.8059V25.6334C3.21398 29.434 4.36772 30 7.66411 30H21.509C25.135 30 26.2887 29.1105 26.2887 25.6334V11.8059C28.349 11.7251 29.9972 10.1078 29.9972 8.08625V7.84367C30.0796 5.74124 28.349 4.12399 26.2887 4.12399ZM11.867 1.77898H17.3885C19.6135 1.77898 20.1904 2.02156 20.2728 4.04313H9.2299C9.31231 2.10243 9.55954 1.77898 11.867 1.77898ZM24.5581 25.6334C24.5581 28.1402 24.2285 28.221 21.6738 28.221H7.74652C5.027 28.221 5.10941 28.0593 5.10941 25.6334V11.8059H24.5581V25.6334ZM28.2666 8.08625C28.2666 9.13747 27.3601 10.027 26.2887 10.027H3.79085C2.71952 10.027 1.81302 9.13747 1.81302 8.08625V7.84367C1.81302 6.79245 2.71952 5.90297 3.79085 5.90297H26.2063C27.2776 5.90297 28.1842 6.79245 28.1842 7.84367V8.08625H28.2666ZM20.3552 25.3908C20.6025 25.3908 20.8497 25.1482 20.8497 24.9057V14.0701C20.8497 13.8275 20.6025 13.5849 20.3552 13.5849C20.108 13.5849 19.8608 13.8275 19.8608 14.0701V24.9865C19.8608 25.2291 20.108 25.3908 20.3552 25.3908ZM14.8338 25.3908C15.081 25.3908 15.3282 25.1482 15.3282 24.9057V14.0701C15.3282 13.8275 15.081 13.5849 14.8338 13.5849C14.5865 13.5849 14.3393 13.8275 14.3393 14.0701V24.9865C14.3393 25.2291 14.5865 25.3908 14.8338 25.3908ZM9.2299 25.3908C9.47713 25.3908 9.72436 25.1482 9.72436 24.9057V14.0701C9.72436 13.8275 9.47713 13.5849 9.2299 13.5849C8.98267 13.5849 8.73544 13.8275 8.73544 14.0701V24.9865C8.81785 25.2291 8.98267 25.3908 9.2299 25.3908Z" fill="#000000"/>
          <rect width="30" height="30" style={{fill:"rgba(0,0,0,0)"}}/>
        </g>
      </svg>
    );
  }

export const EditBarBackground = (props) => {
    const trashYoffset = + StaticVars.BAR_EDIT_BACKGROUND_Y + StaticVars.BAR_BACKGROUND_HEIGHT - 40;
    const trashXoffset = StaticVars.DEFAULT_BAR_WIDTH- 9;
    return(
        <svg width={StaticVars.DEFAULT_BAR_WIDTH*2} height={StaticVars.CANVAS_SIZE.height} xmlns="http://www.w3.org/2000/svg"
            style={{
                position:"absolute",
                marginLeft:(props.position !== undefined ? props.position.x : "unset"),
                marginTop:(props.position !== undefined ? props.position.y : "unset"),
            }}
            transform= {props.scale !== undefined ? "scale(" + props.scale + " " + props.scale + ")" : ""}
        >
            {props.isEdit? <rect width="62" height={StaticVars.BAR_BACKGROUND_HEIGHT} rx="31" y={StaticVars.BAR_EDIT_BACKGROUND_Y} fill="#ECF0F4aa"/> : null}
            <rect x={StaticVars.DEFAULT_BAR_WIDTH}  y={StaticVars.BAR_EDIT_BACKGROUND_Y + 15} width="2" height={StaticVars.BAR_EDIT_HEIGHT} rx="1" fill={props.isEdit?"#DFE3EC" :"#eef1fa"}/>
             {/* "#eef1fa" */}
        </svg>
            
    );
}

export const CustomBarIcon = (props) => {
    if (props.candleInfo !== undefined){
        const isGreen = (props.candleInfo.close <= props.candleInfo.open ? true : false);
        let barFill = props.fill !== undefined ? props.fill : (isGreen ? 'url(#custom_bar_green)' : 'url(#custom_bar_red)');
        let barStroke = props.stroke !== undefined ? props.stroke : (isGreen ? 'url(#custom_bar_green)' : 'url(#custom_bar_red)');
        if (props.candleInfo.isGrey){
            barFill = '#83868D';
            barStroke = '#83868D';
        }
        const editBorderColor = props.candleInfo.isGrey ? '#83868D' : (isGreen? "#1CEFCC" : "#EF4444");
        const topBody = (isGreen ? props.candleInfo.close : props.candleInfo.open);
        const bottomBody = (isGreen ? props.candleInfo.open : props.candleInfo.close);
        const barEditOpenCloseHeight = 4;
        const barEditOpenCloseHeightBuffer = 4;//multiplier
        return(
                <svg width={StaticVars.DEFAULT_BAR_WIDTH*2} height={StaticVars.CANVAS_SIZE.height} xmlns="http://www.w3.org/2000/svg"
                    style={{
                        position:"absolute",
                        marginLeft:(props.marginLeft !== undefined ? props.marginLeft : "unset"),
                        marginTop:(props.marginTop !== undefined ? props.marginTop : "unset"),
                    }}
                    transform= {props.scale !== undefined ? "scale(" + props.scale + " " + props.scale + ")" : ""}
                >
                <defs>
                    <radialGradient id="red-radial" cx="50%" cy="50%" r="50%" fx="50%" fy="50%">
                        <stop offset="0%" stopColor="rgb(184,41,31)" stopOpacity="1" />
                        <stop offset="100%" stopColor="rgb(255,28,3)" stopOpacity="1" />
                    </radialGradient>
                    <radialGradient id="green-radial" cx="50%" cy="50%" r="50%" fx="50%" fy="50%">
                        <stop offset="0%" stopColor="rgb(3,255,28)" stopOpacity="1" />
                        <stop offset="100%" stopColor="rgb(50,179,63)" stopOpacity="1" />
                    </radialGradient>
                    <radialGradient id="custom_bar_green" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(4.8375 -25.7442) rotate(90.9421) scale(273.688 7.71864)">
                        <stop stopColor="#1CEFCC"/>
                        <stop offset="1" stopColor="#15E0A7"/>
                    </radialGradient>
                    <radialGradient id="custom_bar_red" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(4.8375 -33.75) rotate(90.7187) scale(358.778 7.71907)">
                        <stop stopColor="#EF4444"/>
                        <stop offset="1" stopColor="#F33939"/>
                    </radialGradient>
                </defs>
                    <g id="barbody" cursor="move" onMouseDown={props.isEdit? props.barEditEvents.onMouseDown : null}>
                        <rect x={StaticVars.DEFAULT_BAR_WIDTH - 2} width="6" height={props.candleInfo.low - props.candleInfo.high} y={props.candleInfo.high}  rx="3" fill={barFill} stroke={barStroke}/>
                        <rect x={StaticVars.DEFAULT_BAR_WIDTH - 11} width="24" height={Math.abs(props.candleInfo.open - props.candleInfo.close)} y={topBody} rx="2" fill={barFill} stroke={barStroke}/>
                    </g>  
                    {props.isEdit?
                        <g cursor="n-resize">
                            {/* top body */}
                            <g id={isGreen ? "close" : "open"}
                                onMouseDown={props.barEditEvents.onMouseDown}
                            >
                            
                                <rect x="4" y={topBody} width="20" height={barEditOpenCloseHeight} rx="2" fill={editBorderColor}/>
                                <rect x="5" y={topBody + barEditOpenCloseHeight/4} width="18" height={barEditOpenCloseHeight/2} rx="2" fill="#ffffff"/>
                                <rect x="4" y={topBody - barEditOpenCloseHeight*((barEditOpenCloseHeightBuffer - 1) / 2)} width="20" height={barEditOpenCloseHeight*barEditOpenCloseHeightBuffer} rx="2" fill="#000044" fillOpacity={0}/>    
                            </g>
                            {/* bottom body */}
                            <g id={isGreen ? "open" : "close"}
                                onMouseDown={props.barEditEvents.onMouseDown}
                            >
                            
                                <rect x={StaticVars.DEFAULT_BAR_WIDTH*2 - 24} y={bottomBody - barEditOpenCloseHeight} width="20" height={barEditOpenCloseHeight} rx="2" fill={editBorderColor}/>
                                <rect x={StaticVars.DEFAULT_BAR_WIDTH*2 - 23 } y={bottomBody + barEditOpenCloseHeight/4 - barEditOpenCloseHeight} width="18" height={barEditOpenCloseHeight/2} rx="2" fill="#ffffff"/>
                                <rect x={StaticVars.DEFAULT_BAR_WIDTH*2 - 24} y={bottomBody - barEditOpenCloseHeight*barEditOpenCloseHeightBuffer/2 - barEditOpenCloseHeight/2} width="20" height={barEditOpenCloseHeight*barEditOpenCloseHeightBuffer} rx="2" fill="#000044" fillOpacity={0}/>    
                            </g>
                            <g id="high"
                                onMouseDown={props.barEditEvents.onMouseDown}
                            >
                                <circle cx={StaticVars.DEFAULT_BAR_WIDTH + 1} cy={props.candleInfo.high} r="6" fill="#ffffff"/>
                                <circle cx={StaticVars.DEFAULT_BAR_WIDTH + 1} cy={props.candleInfo.high} r="7" fill="#00000000" stroke={editBorderColor} strokeWidth="2"/>
                            </g>
                            <g id="low"
                                onMouseDown={props.barEditEvents.onMouseDown}
                            >
                                <circle cx={StaticVars.DEFAULT_BAR_WIDTH + 1} cy={props.candleInfo.low} r="6" fill="#ffffff"/>
                                <circle cx={StaticVars.DEFAULT_BAR_WIDTH + 1} cy={props.candleInfo.low} r="7" fill="#00000000" stroke={editBorderColor} strokeWidth="2"/>
                            </g>
                        </g>
                        :
                        null
                    }
                </svg>
                
        );
    }
}

CustomBarIcon.propTypes = {
    stroke: PropTypes.string,
    fill: PropTypes.string,
    gradient1color: PropTypes.string,
    gradient2color: PropTypes.string,
    strokeWidth: PropTypes.number,
    marginLeft: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    marginRight: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    scale: PropTypes.number,
    candleInfo: PropTypes.object,
    events: PropTypes.object,
    barEditEvents: PropTypes.object,
    isEdit: PropTypes.bool,
  };

  export const MiniBarIcon = (props) => {//props.marginLeft props.marginTop
    const CANVAS_SCALE = {width: StaticVars.MINI_CANVAS_SIZE.width / StaticVars.CANVAS_SIZE.width, height: StaticVars.MINI_CANVAS_SIZE.height / StaticVars.CANVAS_SIZE.height};
    const barWidth = StaticVars.DEFAULT_BAR_WIDTH * CANVAS_SCALE.width;
    const isGreen = (props.candleInfo.close <= props.candleInfo.open ? true : false);
    let barFill = props.fill !== undefined ? props.fill : (isGreen ? 'url(#custom_bar_green)' : 'url(#custom_bar_red)');
    let barStroke = props.stroke !== undefined ? props.stroke : (isGreen ? 'url(#custom_bar_green)' : 'url(#custom_bar_red)');
    if (props.candleInfo.isGrey){
        barFill = '#83868D';
        barStroke = '#83868D';
    }
    if (props.candleInfo !== undefined){
        const isGreen = (props.candleInfo.close <= props.candleInfo.open ? true : false);
        // const barHeight = Math.abs(props.candleInfo.high - props.candleInfo.low);
        const topBody = (isGreen ? props.candleInfo.close : props.candleInfo.open);
        const bottomBody = (isGreen ? props.candleInfo.open : props.candleInfo.close);
        const path = "M" + (props.marginLeft + barWidth/2) + " " + (props.marginTop + props.candleInfo.high) + " L" + (props.marginLeft + barWidth/2) + " " + (props.marginTop + topBody) + " L" + (props.marginLeft) + " " + (props.marginTop + topBody) + " L" + (props.marginLeft) + " " + (props.marginTop + bottomBody) + " L" + (props.marginLeft + barWidth) + " " + (props.marginTop + bottomBody) + " L" +(props.marginLeft + barWidth) + " " + (props.marginTop + topBody) + " L" + (props.marginLeft + barWidth/2) + " " + (props.marginTop + topBody) + " M" + (props.marginLeft + barWidth/2) + " " + (props.marginTop + bottomBody) + " L" + (props.marginLeft + barWidth/2) + " " + (props.marginTop + props.candleInfo.low);
        return(
                <path d={path}
                    
                    stroke={barStroke}
                    fill={barFill}
                    strokeWidth={props.strokeWidth !== undefined ? props.strokeWidth : "1"}    
                    transform= {props.scale !== undefined ? "scale(" + props.scale + " " + props.scale + ")" : ""}
                />
        );
    }
}

  export const SearchingIcon = () =>{
    return(
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"
             transform= "scale(1 1)"
             style={{position:"absolute", top:"0px", left:"0px"}}
        >
            <path d="M389.6,289.4c0,73.8-59.8,133.6-133.6,133.6c-73.7,0-133.6-59.8-133.6-133.6c0-73.8,59.8-133.6,133.6-133.6v66.8
            l113.9-111.3L256,0v66.8c-122.9,0-222.6,99.7-222.6,222.6C33.4,412.3,133.1,512,256,512c122.9,0,222.6-99.7,222.6-222.6H389.6z"/>
        </svg>
    );
}

export const Searching4Icon = () =>{
    return(
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40"
             transform= "scale(-1 1)"
             style={{position:"absolute", top:"0px", left:"0px"}}
        >
            <g>
                <polygon fill="#616E7D" points="8.4,20.9 7.9,20.9 7.9,20.9 	"/>
                <path fill="#616E7D" d="M10.8,30c-2.4-2.4-3.8-5.3-4.1-8.6l-3.4,0.4l0,0l0,0c-0.3,0-0.6,0-1,0.1c-0.1,0-0.1,0-0.2,0
                    c0.4,4.3,2.3,8.4,5.4,11.4c3.6,3.6,8.3,5.5,13.3,5.5c4.7,0,9.2-1.7,12.7-4.9l-3.3-3.3c-2.6,2.3-5.9,3.6-9.4,3.6
                    C17.1,34.1,13.5,32.6,10.8,30z"/>
                <path fill="#616E7D" d="M20.7,1.3c-5,0-9.8,2-13.3,5.5C7,7.2,6.6,7.6,6.2,8.1l3.3,3.3c0.4-0.5,0.7-0.9,1.2-1.3
                    c2.7-2.7,6.2-4.1,10-4.1c3.8,0,7.4,1.5,10,4.1c2.6,2.6,4,6,4.1,9.6c1.5-0.3,3.1-0.5,4.7-0.8C39.2,14.4,37.3,10,34,6.8
                    C30.5,3.2,25.7,1.3,20.7,1.3L20.7,1.3z"/>
                <polygon fill="#AAD3C7" points="2.8,4.6 0.8,17.8 14,15.8 9.5,11.4 6.2,8.1"/>
                <polygon fill="#AAD3C7" points="30.2,30.6 33.6,33.9 36.9,37.3 38.9,24.1 25.7,26.1"/>
            </g>
        </svg>
    );
}

export const Searching5Icon = () => {
    return(
<svg viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg" style={{position:"absolute", top:"0px", left:"0px"}}>
    <path d="M25.5053 50C28.1206 50 30.2408 47.8953 30.2408 45.299C30.2408 42.7027 28.1206 40.598 25.5053 40.598C22.8899 40.598 20.7698 42.7027 20.7698 45.299C20.7698 47.8953 22.8899 50 25.5053 50Z" fill="#3C3E45"/>
    <g opacity="0.9">
    <path opacity="0.9" d="M34.3635 40.3711C34.3635 37.7732 36.482 35.6701 39.099 35.6701C41.716 35.6701 43.8345 37.7732 43.8345 40.3711C43.8345 42.9691 41.716 45.0722 39.099 45.0722C36.482 45.0722 34.3635 42.9691 34.3635 40.3711Z" fill="#3C3E45"/>
    </g>
    <g opacity="0.8">
    <path opacity="0.8" d="M41.5291 28.1134C41.5291 25.5154 43.6476 23.4124 46.2645 23.4124C48.8815 23.4124 50 25.5154 50 28.1134C51 30.7113 48.8815 32.8144 46.2645 32.8144C43.6579 32.8144 41.5291 30.7113 41.5291 28.1134Z" fill="#3C3E45"/>
    </g>
    <g opacity="0.7">
    <path opacity="0.7" d="M38.9951 13.7732C38.9951 11.1752 41.1136 9.07214 43.7306 9.07214C46.3476 9.07214 48.4661 11.1752 48.4661 13.7732C48.4661 16.3711 46.3476 18.4742 43.7306 18.4742C41.1136 18.4742 38.9951 16.3711 38.9951 13.7732Z" fill="#3C3E45"/>
    </g>
    <g opacity="0.6">
    <path opacity="0.6" d="M28.1326 4.70103C28.1326 2.10309 30.2511 0 32.8681 0C35.485 0 37.6036 2.10309 37.6036 4.70103C37.6036 7.29897 35.485 9.40206 32.8681 9.40206C30.2511 9.41237 28.1326 7.29897 28.1326 4.70103Z" fill="#3C3E45"/>
    </g>
    <g opacity="0.5">
    <path opacity="0.5" d="M13.3965 4.70103C13.3965 2.10309 15.5254 0 18.1424 0C20.7593 0 22.8779 2.10309 22.8779 4.70103C22.8779 7.29897 20.7593 9.40206 18.1424 9.40206C15.5254 9.41237 13.3965 7.29897 13.3965 4.70103Z" fill="#3C3E45"/>
    </g>
    <g opacity="0.4">
    <path opacity="0.4" d="M2.54419 13.7732C2.54419 11.1752 4.6627 9.07214 7.27968 9.07214C9.89666 9.07214 12.0152 11.1752 12.0152 13.7732C12.0152 16.3711 9.89666 18.4742 7.27968 18.4742C4.6627 18.4742 2.54419 16.3711 2.54419 13.7732Z" fill="#3C3E45"/>
    </g>
    <g opacity="0.3">
    <path opacity="0.3" d="M4.73549 32.8144C7.35083 32.8144 9.47098 30.7097 9.47098 28.1134C9.47098 25.5171 7.35083 23.4124 4.73549 23.4124C2.12015 23.4124 0 25.5171 0 28.1134C0 30.7097 2.12015 32.8144 4.73549 32.8144Z" fill="#3C3E45"/>
    </g>
    <g opacity="0.2">
    <path opacity="0.2" d="M7.16553 40.3711C7.16553 37.7732 9.28404 35.6701 11.901 35.6701C14.518 35.6701 16.6365 37.7732 16.6365 40.3711C16.6365 42.9691 14.518 45.0722 11.901 45.0722C9.29442 45.0722 7.16553 42.9691 7.16553 40.3711Z" fill="#3C3E45"/>
    </g>
    <g opacity="0.1">
    <path opacity="0.1" d="M25.5053 50C28.1206 50 30.2408 47.8953 30.2408 45.299C30.2408 42.7027 28.1206 40.598 25.5053 40.598C22.8899 40.598 20.7698 42.7027 20.7698 45.299C20.7698 47.8953 22.8899 50 25.5053 50Z" fill="#3C3E45"/>
    </g>
</svg>

    );
}

export const Searching3Icon = () =>{
    return(
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"
             transform= "scale(1 1)"
             style={{position:"absolute", top:"0px", left:"0px"}}
        >
            <path d="M45.633,17.348C46.54,19.792,47,22.367,47,25c0,12.131-9.869,22-22,22c-6.595,0-12.795-2.963-16.958-8H15v-2H5v10h2v-6.126
                C11.53,46.007,18.068,49,25,49c13.234,0,24-10.767,24-24c0-2.872-0.502-5.68-1.492-8.348L45.633,17.348z" fill="#82bad612"/>
            <path d="M35,13h10V3h-2v6.125C38.47,3.992,31.934,1,25,1C11.767,1,1,11.767,1,25c0,2.872,0.502,5.68,1.492,8.348l1.875-0.696
                C3.46,30.208,3,27.633,3,25C3,12.869,12.869,3,25,3c6.598,0,12.795,2.962,16.958,8H35V13z" fill="#82bad612"/>
        </svg>
    );
}

export const Searching2Icon = () => {
    return(
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 487.622 487.622"
                transform= "scale(1 1)"
                style={{position:"absolute", top:"0px", left:"0px"}}
            >
            <g>
                <circle style={{fill:"#3C556B", cx:"243.811", cy:"243.811", r:"243.811"}}/>
                <path style={{opacity:"0.37", fill:"#3C556B"}} d="M476.399,317.117L262.036,110.221l9.471,43.123
                    l-75.696,9.467l-62.708,81l26.34,98.648l132.744,140.354C379.549,465.225,450.003,400.942,476.399,317.117z"/>
                <path style={{fill:"#FFFFFF"}} d="M328.161,342.458c17.312-17.313,28.744-39.161,33.057-63.181c4.213-23.453,1.4-47.348-8.129-69.096
                    l-26.674,11.688c15.02,34.276,7.623,73.529-18.846,99.996c-35.16,35.161-92.372,35.161-127.533,0
                    c-35.162-35.16-35.162-92.372,0-127.533c17.797-17.798,41.485-27.023,65.701-26.449l-9.879,34.947l46.645-11.816l46.646-11.816
                    l-33.557-34.488l-33.557-34.488l-8.195,28.994c-12.077-1.016-24.3-0.197-36.249,2.484c-22.018,4.938-42.124,16.018-58.147,32.041
                    c-22.533,22.533-34.942,52.492-34.942,84.358c0,0.002,0-0.001,0,0.001c0,31.863,12.411,61.826,34.942,84.359
                    c22.533,22.532,52.493,34.941,84.359,34.941C275.668,377.401,305.627,364.991,328.161,342.458z"/>
            </g>
        </svg>
    );
}