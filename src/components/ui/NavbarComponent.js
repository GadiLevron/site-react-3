import React from 'react';
import { withStyles, useTheme  } from '@material-ui/core/styles';
import { useEffect } from 'react';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import PropTypes from 'prop-types';
import './NavbarComponent.css';
import SwipeableViews from 'react-swipeable-views';
import Box from '@material-ui/core/Box';
import GraphEngineComponent from '../graphsearch/GraphEngineComponent';
import CandleEngineComponent from '../candlesticksearch/CandleEngineComponent';
import SearchResultsComponent from '../SearchResultsComponent';
import '../../lib/semantic.css';
import {CandleTitleIcon, GraphTitleIcon} from './SvgIcons';
// import SadFaceLottie from './lotties/SadFaceLottie';
// import {Searching4Icon} from './SvgIcons';

const StyledTabs = withStyles({
  root:{
    // width:'1000px',
    // backgroundColor:'#00000000',
  },
  indicator: {
    display: 'flex',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    // backgroundColor:'#194474',
    // backgroundColor: '#fff',
    // marginTop:'-1.5em',
    
    '& > span': {
      backgroundColor:'#194474',
      // maxWidth: 80,
      maxHeight: 2,
      width: '100%',
      marginTop:'-5px',
      backgroundColor: '#194474',
    },
  },
})((props) => <Tabs {...props} TabIndicatorProps={{ children: <span /> }} />);

const StyledTab = withStyles((theme) => ({
  root: {
    textTransform: 'none',
    color: '#194474',
    fontWeight: theme.typography.fontWeightRegular,
    fontSize: theme.typography.pxToRem(25),
    marginRight: theme.spacing(0),
    opacity: 0.4,
    borderRadius:'20px',
    // minHeight:15,
    width:'50%',
    '&:hover': {
      opacity: 0.75,
      color: '#194474',
  },
    '&:focus, &:active': {
        opacity: 1,
        outline:'none',
        outlineOffset:'none',
        border:'none',
        color: '#194474',
    },
  },
}))((props) => <Tab disableRipple {...props} />);

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`full-width-tabpanel-${index}`}
        aria-labelledby={`full-width-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box style={{marginLeft:"0em"}}>
            <div style={{overflow:"hidden"}}>{children}</div>
          </Box>
        )}
      </div>
    );
  }
  
  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };

export default function NavbarComponent({onShowContactForm}) {
    //   const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const [isSearchEnd, setSearchEnd] = React.useState(false);
    const theme = useTheme();
    const searchResultsComponentRef = React.createRef();
    const graphComponentRef = React.createRef();
    const candleComponentRef = React.createRef();
    
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const handleChangeIndex = (index) => {
        setValue(index);
    };

    useEffect(() => {
        // theme.direction = "ltr";
        // console.log("This only happens ONCE.  But it happens AFTER the initial render.");
    }, []);

    const onShowResult = (resultData) => {
      // window.scrollTo({ behavior: 'smooth', top: 470 });
      if (searchResultsComponentRef.current !== null){
          searchResultsComponentRef.current.showResult(resultData);
      }
    }

    const onClearResults = (callbackFunc, sliderValues) => {
        searchResultsComponentRef.current.clearResults(callbackFunc, sliderValues);
    }
    
    const setActiveSearch = (isSearchActive) => {
      if (searchResultsComponentRef.current){
          searchResultsComponentRef.current.setActiveSearch(isSearchActive);
      }
    }

    const endSearch = () => {
      if (searchResultsComponentRef.current){
        searchResultsComponentRef.current.endSearch();
      }
    }

    const updatePattern = (pattern, patternName) => {
      if (searchResultsComponentRef.current){
        searchResultsComponentRef.current.updatePattern(pattern, patternName);
      }
    }
  
    return (
      
        <div className="NavbarDiv">
          {/* <TitleCompoenent/> */}
          <div className="NavbarVisual">
            <StyledTabs centered value={value} onChange={handleChange} >
                <StyledTab label="LINES PLOT" style={{backgroundColor:'#294c7a00'}} icon={<GraphTitleIcon scale={0.07} position={{x:-75, y:2}}/>} />
                <StyledTab label="CANDLES PLOT" style={{backgroundColor:'#294c7a00'}} icon={<CandleTitleIcon scale={0.05} position={{x:-100, y:0}}/>} />
            </StyledTabs>
            <SwipeableViews
                style={{ overflow: "hidden" }}
                axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                index={value}
                onChangeIndex={handleChangeIndex}
            >
                <TabPanel value={value} index={0} dir={theme.direction}>
                    <GraphEngineComponent
                        ref={graphComponentRef}
                        onShowResult={onShowResult}
                        clearResults={onClearResults}
                        setActiveSearch={setActiveSearch}
                        endSearch={endSearch}
                        updatePattern={updatePattern}
                    />
                    {/* <SearchResultsComponent ref={searchResultsComponentRef} /> */}
                </TabPanel>
                <TabPanel value={value} index={1} dir={theme.direction}>
                    <CandleEngineComponent 
                        ref={candleComponentRef}
                        onShowResult={onShowResult}
                        clearResults={onClearResults}
                        setActiveSearch={setActiveSearch}
                        endSearch={endSearch}
                        updatePattern={updatePattern}
                    />
                </TabPanel>
            </SwipeableViews>
          </div>
          {/* {isSearchEnd === false?
            <div className='SearchingAnimationDiv'>
                <Searching4Icon/>
            </div>
            :
            <div className='NoMatchMessage'>
              <SadFaceLottie width={70} height={70} position={{x:100, y:100}}/>
              Sorry, no matches found
            </div>
          }
          */}
          <SearchResultsComponent ref={searchResultsComponentRef} /> 
        </div>
    );
}
