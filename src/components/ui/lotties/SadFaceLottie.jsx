// UncontrolledLottie.jsx
import React, { Component } from 'react'
import Lottie from 'react-lottie'
import animationData from './sad-animation.json'

class UncontrolledLottie extends Component {

  constructor(props){
    super(props);
  }

  render(){

    const defaultOptions = {
      loop: true,
      autoplay: true,
      animationData: animationData,
      rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice'
      }
    };

    return(
      <Lottie options={defaultOptions}
              height={this.props.height}
              width={this.props.width}
              style={{position:"relative", left:this.props.position.x, top:this.props.position.y}}
      />
    )
  }
}

export default UncontrolledLottie