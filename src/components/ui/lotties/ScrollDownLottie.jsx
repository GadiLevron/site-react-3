// UncontrolledLottie.jsx
import React, { Component } from 'react'
import Lottie from 'react-lottie'
import animationData from './scroll-down1.json'

class UncontrolledLottie extends Component {

  constructor(props){
    super(props);
  }

  render(){

    const defaultOptions = {
      loop: true,
      autoplay: true,
      animationData: animationData,
      rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice'
      }
    };

    return(
      <Lottie options={defaultOptions}
              speed={1.7}
              isClickToPauseDisabled={true}
              height={this.props.height}
              width={this.props.width}
              style={{position:"relative", left:this.props.position.x, top:this.props.position.y, opacity:0.45}}
      />
    )
  }
}

export default UncontrolledLottie