import React from 'react';
import { Slider} from 'rsuite';
import { Backdrop } from '@material-ui/core';
import 'rsuite/lib/styles/index.less';
import 'rsuite/dist/styles/rsuite-default.css';

// import 'rsuite/lib/styles/themes/default/index.less';
export default class CustomSlider extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        value: 0
      };
    }
    render() {
      const labels = ['A', 'B', 'C', 'D'];
      const { value } = this.state;
      const handleStyle = {
        color: '#ffa',
        fontSize: 22,
        width: 32,
        height: 32
      };

      const handleClassName = {
        color:'#ffa',
        width:50,
        height:100,
        border: 5,
        backgroundColor:'#ffa',
        backgroundImage: "url('https://ej2.syncfusion.com/demos/src/slider/images/thumb.png')",
        borderRadius:5,
      };
  
      return (
        <div >
          <div style={{ width: 200, marginLeft: 60 }}>
            <Slider
              min={0}
              max={labels.length - 1}
              value={value}
              className="custom-slider"
              handleStyle={handleStyle}
            //   graduated
              tooltip={false}
              handleTitle={labels[value]}
              handleClassName={handleClassName}
              onChange={v => {
                this.setState({ value: v });
              }}
            />
          </div>
        </div>
      );
    }
  }
