
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
// import ListItemIcon from '@material-ui/core/ListItemIcon';
// import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
// import InboxIcon from '@material-ui/icons/MoveToInbox';
// import DraftsIcon from '@material-ui/icons/Drafts';
// import SendIcon from '@material-ui/icons/Send';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Checkbox from '@material-ui/core/Checkbox';
import './IndexesMenu.css';
// import { StylesProvider } from "@material-ui/core/styles";
const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
    // borderRadius:'20px',
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    {...props}
  />
));

const StyledMenuItem = withStyles((theme) => ({
  root: {
    '&:focus': {
      backgroundColor: theme.palette.primary.main,
      '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
        color: theme.palette.common.white,
      },
    },
  },
}))(MenuItem);

export default function IndexesMenu({onItemClicked, style}) {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleItemClicked = (event) =>{
    // event.currentTarget.id;
    onItemClicked(event.currentTarget.id);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
    {/* <StylesProvider injectFirst> */}
      <Button
        className='MarketIndexesDropDown'
        // aria-controls="customized-menu"
        aria-haspopup="true"
        variant="contained"
        // color='default'
        onClick={handleClick}
        // endIcon={<ExpandMoreIcon/>}
        style={style !== undefined ? style:null}
      >
        <div className='MarketIndexesDropDown_Title'>Market Indices</div>
        <div className='MarketIndexesDropDown_Text'>S&P 500, NASDAQ 100, DOW, CRYPTO, FOREX</div>
        <div style={{position:'absolute', left:'405px', top:'14px'}}><ExpandMoreIcon/></div>
      </Button>
      <StyledMenu
        id="customized-menu"
        anchorEl={anchorEl}
        keepMounted
        disableAutoFocusItem
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <StyledMenuItem>
          <Checkbox checked disabled></Checkbox>
          <ListItemText primary="S&P 500" />
        </StyledMenuItem>

        <StyledMenuItem >
          <Checkbox checked disabled></Checkbox>
          <ListItemText primary="NASDAQ 100" />
        </StyledMenuItem>

        <StyledMenuItem >
        <Checkbox checked disabled></Checkbox>
          <ListItemText primary="DOW" />
        </StyledMenuItem>

        <StyledMenuItem >
        <Checkbox checked disabled></Checkbox>
          <ListItemText primary="CRYPTO CURRENCY" />
        </StyledMenuItem>

        <StyledMenuItem >
        <Checkbox checked disabled></Checkbox>
          <ListItemText primary="FOREX" />
        </StyledMenuItem>

        <StyledMenuItem >
        <Checkbox disabled></Checkbox>
          <ListItemText primary="RUSSELL 2000" />
        </StyledMenuItem>
        
      </StyledMenu>
      
      {/* </StylesProvider> */}
    </div>
  );
}
