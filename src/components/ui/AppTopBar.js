import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles, withStyles, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
// import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import TitleCompoenent from '../TitleComponent';
// import { Route, Redirect } from 'react-router-dom';

const StyledAppBar = withStyles({
    root:{
      background: 'radial-gradient(125.03% 319.52% at 98.46% -10.64%, #1E568E 0%, #142C4E 100%)',
      marginTop:'0px',
      // width:'1170px',
      // height:'60px',
      // width:'100px',
        // backgroundColor
    }
})((props) => <AppBar {...props} />);

const StyledTabs = withStyles({
    root:{
        height:'60px',
        width:'450px',
        marginLeft:'auto',
        // background:'#ff00ff',
        // background: 'linear-gradient(to right bottom, #430089, #82ffa1)',
    },
    indicator: {
      display: 'flex',
      justifyContent: 'center',
      backgroundColor: 'transparent',
    //   flexGrow:1,
      // backgroundColor: '#fff',
      // marginTop:'-1.5em',
    //   flexFlow:'',
      '& > span': {
        // maxWidth: 80,
        maxHeight: 2,
        width: '80px',
        marginTop:'0px',
        backgroundColor: '#ffff',
      },
    },
  })((props) => <Tabs {...props} TabIndicatorProps={{ children: <span /> }} />);
  
  const StyledTab = withStyles((theme) => ({
    root: {
        // backgroundColor:'#ffeedd',
      textTransform: 'none',
      color: '#fff',
      width:'110px',
      minWidth:'110px',
      fontWeight: theme.typography.fontWeightBold,
      fontSize: theme.typography.pxToRem(16),
      // marginRight: theme.spacing(-5),
      opacity: 0.75,
      //  left:'300px',
    //   borderRadius:'20px',
      minHeight:10,
      // float:'right',
      top:'13px',
      '&:focus': {
          opacity: 1,
          outline:'none',
          outlineOffset:'none',
          border:'none',
      },
    },
  }))((props) => <Tab disableRipple {...props} />);
  
  function TabPanel(props) {
      const { children, value, index, ...other } = props;
    
      return (
        <div
          role="tabpanel"
          hidden={value !== index}
          id={`full-width-tabpanel-${index}`}
          aria-labelledby={`full-width-tab-${index}`}
          {...other}
        >
          {value === index && (
            <Box style={{marginLeft:"0em"}}>
              <div style={{overflow:"hidden"}}>{children}</div>
            </Box>
          )}
        </div>
      );
    }
    
    TabPanel.propTypes = {
      children: PropTypes.node,
      index: PropTypes.any.isRequired,
      value: PropTypes.any.isRequired,
    };

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

export default function AppTopBar({redirectPage, defaultValue}) {
  const classes = withStyles();
  const theme = useTheme();
  const [value, setValue] = React.useState(defaultValue);


  const handleChange = (event, newValue, forceRedirect = false) => {
    if (value !== newValue || forceRedirect === true){
      switch(newValue){
        case 0:{
          redirectPage('/main');
          break;
        }
        case 1:{
          redirectPage('/about');
          break;
        }
        case 2:{
          redirectPage('/disclaimer');
          break;
        }
        case 3:{
          redirectPage('/contact');
          break;
        }
        default:{
          
        }
      }

      setValue(newValue);
    }
  };

  const redirect = () => {
    handleChange(null, 0, true);
  }


  return (
    <div className={classes.root}>
      <StyledAppBar color='transparent' position="static">
        <div className='AppBarDiv'>
          <StyledTabs centered value={value} onChange={handleChange} aria-label="simple tabs example">
            <StyledTab label="Plot It" {...a11yProps(0)} />
            <StyledTab label="About Us" {...a11yProps(1)} />
            <StyledTab label="Disclaimer" {...a11yProps(2)} />
            <StyledTab label="Contact Us" {...a11yProps(3)} />
          </StyledTabs>
          <TitleCompoenent redirectPage={redirect}/>
        </div>
      </StyledAppBar>
    </div>
  );
}
