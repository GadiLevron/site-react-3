import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';

const useStyles = makeStyles({
  root: {
    width: 200,
    marginLeft:10,
  },
});

// const marks = [
//     {
//       value: 0,
//       label: '0%',
//     },
//     {
//       value: 25,
//       label: '25%',
//     },
//     {
//       value: 50,
//       label: '50%',
//     },
//     {
//       value: 75,
//       label: '75%',
//     },
//     {
//         value: 100,
//         label: '100%',
//       },
//   ];

export default function RangeSlider({onChangeFunc, message, min, max, marks, textSuffix, defaultValue}) {
  const sliderRef = React.useRef();
  const classes = useStyles();
  const [value, setValue] = React.useState([parseInt(defaultValue !== undefined ? defaultValue : min)]);
  const handleChange = (event, newValue) => {
    setValue(newValue);
    onChangeFunc(newValue);
  };

  function valuetext(value) {
    return `${value}` + textSuffix;
  }

  onChangeFunc(value);
  return (
    <div className={classes.root}>
      <Typography id="range-slider" align='center' gutterBottom>
        {message}
      </Typography>
      <Slider
        ref={sliderRef}
        value={value < min ? min : value}
        onChange={handleChange}
        
        aria-labelledby="day-label"
        getAriaValueText={valuetext}
        valueLabelFormat={valuetext}
        step={1}
        marks={marks}
        min={parseInt(min)}
        max={parseInt(max)}
        valueLabelDisplay="auto"
      />
      
    </div>
  );
}