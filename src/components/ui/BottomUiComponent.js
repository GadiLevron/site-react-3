import React from 'react';
import './BottomUiComponent.css';
import SadFaceLottie from '../ui/lotties/SadFaceLottie';
import ScrollDownLottie from '../ui/lotties/ScrollDownLottie';
import {Searching5Icon} from '../ui/SvgIcons';

export default class BottomUiComponent extends React.Component {

    constructor(props){
        super(props);
        this.sliderValues_minMatchPercent = [80, 100];
        this.state = {
          isSearchActive : false,
          isFoundResult: false,
          isSearchEnd: false,
          progress: 0,
        };
    

        this.setSearchActive = this.setSearchActive.bind(this);
        this.foundResult = this.foundResult.bind(this);
        this.endSearch = this.endSearch.bind(this);
      }

      shouldComponentUpdate(nextProps, nextState) {
        return this.state.isSearchActive !== nextState.isSearchActive || this.state.isSearchEnd !== nextState.isSearchEnd || this.state.progress !== nextState.progress || this.state.isFoundResult !== nextState.isFoundResult;//|| (this.state.isSearchActive && nextState.isSearchEnd)
      }

      setSearchActive(isActive){
        this.setState({
          isSearchActive : isActive,
          isSearchEnd: false,
          isFoundResult: false,
          progress: 0,
        });
      }

      foundResult(){
        this.setState({
          isFoundResult: true,
        });
      }

      endSearch(){
        this.setState({
          isSearchEnd: true,
          // isSearchActive:false,
        });
      }

      showProgress(progress){
        this.setState({
          progress: progress,
        });
      }
// {/* <ScrollDownLottie width={100} height={150} position={{x:0, y:0}}/> */}
      render() {
        return (
            <div className='BottomUI_Div'>
                {
                    this.state.isSearchActive ? 
                        (
                            this.state.isFoundResult ?
                                <ScrollDownLottie width={100} height={150} position={{x:0, y:0}}/>
                                :
                                (
                                    this.state.isSearchEnd?
                                        <div className='BottomUI_NoMatchMessageDiv'>
                                            <SadFaceLottie width={90} height={85} position={{x:0, y:110}}/>
                                            <div className='BottomUI_NoMatchMessage'>Sorry, no matches found</div>
                                        </div>
                                        :
                                        <div className='BottomUI_SearchingDiv'>
                                            <div className='BottomUI_SearchingProgress'>{this.state.progress}%</div>
                                            <div className='BottomUI_SearchingAnimationDiv'>
                                                <Searching5Icon />
                                            </div>
                                        </div>
                                )
                        )
                    :
                    null
                }
            </div>
        );
      }//end render

}//end class