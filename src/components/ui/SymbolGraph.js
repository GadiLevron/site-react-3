import React from 'react';
import CanvasJSReact from '../../lib/canvasjs.react'; 
import TradingViewWidget, { Themes, BarStyles } from 'react-tradingview-widget';
import './SymbolGraph.css';

var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;


const MAX_BARS_TO_SHOW = 50;

export default class SymbolGraph extends React.Component {
    highestPrice = 0;
    lowestPrice = 0;
    // constructor(props) {
    //     super(props);
    // }

    getFormattedDate(unixTime){
        //new Date(2012, 11, 1) 01 NOV 12
        let date = new Date(unixTime * 1000);
        return date.getDate() + "/"+ parseInt(date.getMonth()+1);// +"/"+date.getFullYear();
    }

    getPricesArr(arr){
        let retArr = [];
        let counter = 0;
        const maxLength = Math.min(arr.length, MAX_BARS_TO_SHOW);
        for (let i = 0; counter < maxLength; i++){
            if (arr[i].adjclose !== undefined){
                retArr.push({label: i === 0 ? arr[i].date : "",
                            y:arr[i].adjclose,
                            lineColor: counter < this.props.data.numOfBars? "#162652" : "#162652",//ff9634
                            });//x: this.getFormattedDate(arr[i].date), 
                counter++;
            }
        }

        retArr.reverse();
        return retArr;
    }

    getCandlesPricesArr(arr){
        let retArr = [];
        let counter = 0;
        const maxLength = Math.round(Math.min(arr.length, MAX_BARS_TO_SHOW/4));
        for (let i = 0; counter < maxLength; i++){
            if (arr[i].adjclose !== undefined){
                retArr.push({label: i === 0 ? arr[i].date : "",
                            y:[arr[i].open, arr[i].high, arr[i].low, arr[i].adjclose],
                            color: arr[i].open >= arr[i].adjclose? "#f79b8e" : "#1f7807",//ff9634
                            });//x: this.getFormattedDate(arr[i].date), 
                counter++;
            }
        }

        retArr.reverse();
        return retArr;
    }

    // getShortPricesArr(arr, returnAll){
    //     let retArr = [];
    //     let counter = 0;
    //     for (let i = 0; counter < Math.min(arr.length, MAX_BARS_TO_SHOW); i++){
    //         if (arr[i].adjclose !== undefined){
    //             retArr.push({label:" ",
    //                         y:arr[i].adjclose,
    //                         lineColor: returnAll === true ? "#000000" : "#000000",//counter < this.props.data.numOfBarsBack? "#f5f5b9" : "#71db6b",
    //                         lineThickness:5,
    //                         });//x: this.getFormattedDate(arr[i].date), 
    //             counter++;
    //         }
    //     }
    //     if (returnAll === false){
    //         retArr.splice(this.props.data.numOfBarsBack);
    //     }
    //     retArr.reverse();
    //     return retArr;
    // }

    getHighestLowestPrice(arr){
        
        for (let i = 0; i < arr.length; i++){
            if (arr[i].adjclose !== undefined){
                if (arr[i].adjclose > this.highestPrice){
                    this.highestPrice = arr[i].adjclose;
                }
                else
                if (arr[i].adjclose < this.lowestPrice || this.lowestPrice === 0){
                    this.lowestPrice = arr[i].adjclose;
                }
            }
        }
        //return highest * 1.02;
    }

    showLines(){
        let retArr = [];
        
        let arrLength = Math.min(this.props.data.prices.length, MAX_BARS_TO_SHOW);
        for (let i = 0; i < arrLength; i++){
            if (this.props.data.sp.indexOf(arrLength - i - 1) !== -1){
                retArr.push({x:i, y: this.props.data.prices[arrLength - i - 1].adjclose});
            }
        }
        return retArr;
    }

    getTimeFrame(timeFrameIndex){
        switch(timeFrameIndex){
            case 0:{
                return 1;
            }
            case 1:{
                return 5;
            }
            case 2:{
                return 30;
            }
            case 3:{
                return 60;
            }
            case 4:{
                return 240;
            }
            case 5:{
                return 'D';
            }
            case 6:{
                return 'W';
            }
            case 7:{
                return 'M';
            }
            default:{
                return 'D';
            }
        }
    }


    render() {
        CanvasJS.addColorSet("customColorSet1",
            [//colorSet Array
                "#71db6b",
                
            ]);
        this.getHighestLowestPrice(this.props.data.prices);
        const options = !this.props.data.isCandles ? {
            width : 300,
            height : 130,
			animationEnabled: false,
            exportEnabled: false,
            
            colorSet:"customColorSet1",
			theme: "light1", // "light1", "dark1", "dark2"
			// title:{
			// 	text: this.props.data.symbol + " : " + this.props.data.r + "%"
            // },
            toolTip:{
                enabled: false   //enable here
              },
			axisY: {
				// title: "Adjusted Price",
				includeZero: false,
                // suffix: "",
                // maximum : this.highestPrice * 1.01,
                // minimum : this.lowestPrice * 0.99,
                lineThickness:1,
                tickLength: 0,
                valueFormatString:" ",
                gridThickness: 0,
                //margin:10,
			},
			axisX: {
				// title: "Date",
                // prefix: "",
                tickLength: 0,
                interval: 1,
                valueFormatString:" ",
                lineThickness:1,
                labelAngle : 0.1,
            },
            // axisX2: {
			// 	// title: "",
			// 	// prefix: "",
			// 	interval: 1
			// },
			data: [{
                type: "line",
                markerType:"circle",
                markerSize: 1,
                lineThickness: 3,
                markerColor: "#162652",
				toolTipContent: "{y}",
				dataPoints: this.getPricesArr(this.props.data.prices)
            }
            ,
            {
                type: "line",
                markerType:"circle",
                markerSize: 5,
                lineThickness: 2,
                markerColor: "#ff0000",
				lineColor : "#ff0000",
				dataPoints: this.showLines(this.props.data.prices)
            }
            // ,
            // {      
            //     axisXType: "secondary", 
            //     type: "line",
            //     lineThickness: 5,
            //     markerSize: 0,
            //     dataPoints: this.getPricesArr(this.props.data.prices)
            //   }
            ]
        }
        :
        //candles
        {
            width : 300,
            height : 130,
			animationEnabled: false,
            exportEnabled: false,
            // colorSet:"customColorSet1",
            theme: "light1",
            zoomEnabled: true,
            toolTip:{
                enabled: false   //enable here
              },
			axisY: {
				includeZero: false,
                lineThickness:1,
                tickLength: 0,
                valueFormatString:" ",
                gridThickness: 0,
            },
            axisX: {
                tickLength: 0,
                interval: 1,
                valueFormatString:" ",
                lineThickness:1,
                labelAngle : 0.1,
            },
            data: [{
                type: "candlestick",
                fallingColor:"red",
                risingColor:"green",
                // markerSize: 1,
                // lineThickness: 3,
                // markerColor: "#162652",
				toolTipContent: "{y}",
				dataPoints: this.getCandlesPricesArr(this.props.data.prices)
            }]
        }
        
        return (
            this.props.isEnlarge ?
            <div className="SymbolGraphModal">
                <TradingViewWidget
                    symbol={this.props.data.symbol}
                    theme={Themes.DARK}
                    style={BarStyles.CANDLES}
                    allow_symbol_change={false}
                    interval={this.getTimeFrame(this.props.data.timeFrameIndex)}
                    // autosize
                    details={false}
                    // hide_legend="false"
                    height={650}
                    width={1000}
                    // studies
                    // hide_top_toolbar="false"
                />  
            </div>
            :
            <div className="SymbolGraph">
               <CanvasJSChart options = {options} />
            </div>
        );
        
        // return (
        //     <div className="SymbolGraph">
        //        <CanvasJSChart options = {options} />
        //     </div>
        // );
    }
}