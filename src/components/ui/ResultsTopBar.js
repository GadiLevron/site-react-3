import React from 'react';
import './ResultsTopBar.css';
import StaticVars from '../../StaticVars';
import {Searching5Icon, MiniBarIcon} from './SvgIcons';
import ReactDOM from 'react-dom';

const CANVAS_SCALE = {width: StaticVars.MINI_CANVAS_SIZE.width / StaticVars.CANVAS_SIZE.width, height: StaticVars.MINI_CANVAS_SIZE.height / StaticVars.CANVAS_SIZE.height};

export default class ResultsTopBar extends React.Component {
    miniCanvasRef = null;

    constructor(props) {
        super(props);
        this.miniCanvasRef = React.createRef();
        this.state = {
            class: 'ResultsTopBarDiv',
            resultsAmount: 0,
            pattern: props.pattern,
            patternName: props.patternName,
            highestSymbol: "",
            highestR: 0,
            isEndSearch: false,
        }

        this.listenToMouseDown = this.listenToMouseDown.bind(this);
        this.listenToMouseMove = this.listenToMouseMove.bind(this);
        this.listenToMouseUp = this.listenToMouseUp.bind(this);
    }

    updateTotalResults(amount){
        this.setState({
            resultsAmount: amount,
        });
    }

    componentDidMount(){
        window.addEventListener('mousemove', this.listenToMouseMove);
        window.addEventListener('mousedown', this.listenToMouseDown);
        window.addEventListener('mouseup', this.listenToMouseUp);
        setTimeout(() => {
            this.animateItem();
        }, 50);
    }

    listenToMouseMove(event){
        if (this.state.mouseInitPos !== undefined){
            const minHeight = 60;
            const newTop = this.miniCanvasRef.current.offsetTop + event.clientY - this.state.mouseInitPos.y;
            this.setState(prevState => ({
                mousePos: this.miniCanvasRef !== undefined ? {x: this.miniCanvasRef.current.offsetLeft + event.clientX - this.state.mouseInitPos.x, y: newTop > minHeight ? newTop : minHeight} : {x:0, y:0},
                mouseInitPos: {x: event.clientX, y: event.clientY},
            }));

        }
    }

    listenToMouseDown(event){
        event.stopPropagation();
        if (event.path[0].id === "MiniCanvas"){
            this.setState({
                mouseInitPos: {x: event.clientX, y: event.clientY},
            });
        }

        
    }

    listenToMouseUp(event){
        this.setState({
            mouseInitPos: undefined,
        });
    }

    animateItem(){
        this.setState({
            class:'ResultsTopBarDiv show',
            // resultsAmount: 0,
            // isEndSearch: false,
            // highestSymbol: "",
            // highestR: 0,
        });
        const highestRes = this.props.getHighestResult();
        this.updateHighestResult(highestRes.symbol, highestRes.r);
        this.updateTotalResults(highestRes.total);
    }

    showWithModal(isShow){
        if (isShow){
            this.setState({
                class:'ResultsTopBarDiv show withModal',
            });
        }
        else{
            this.setState({
                class:'ResultsTopBarDiv show withoutModal',
            });
        }
    }

    updatePattern(pattern, patternName){
        this.setState({
            pattern: pattern,
            patternName: patternName,
        });
    }

    endSearch(){
        this.setState({
            isEndSearch: true,
        });
    }

    updateHighestResult(symbol, r){
        this.setState({
            highestSymbol: symbol,
            highestR : r,
        });
    }

    render(){
        // console.log("this.state.isEndSearch = " + this.state.isEndSearch);
        return(
            <div className={this.state.class}>
                <div className='TopBarCenterDiv'>
                    <MiniCanvas myRef={this.miniCanvasRef} pattern={this.state.pattern} isGraph={this.state.pattern[0].open === undefined} margin={this.state.mousePos} patternName={this.state.patternName}/>
                    <TotalResults amount={this.state.resultsAmount} isEndSearch={this.state.isEndSearch} />
                    {/* <HighestMatch symbol={this.state.highestSymbol} match={this.state.highestR + '%'} /> */}
                    
                    {this.state.isEndSearch ?
                        null
                        :
                        <div className='TopBarSearchingIconDiv'>
                            <Searching5Icon/>
                        </div>
                    }
                </div>
                
            </div>
        );
    }

}

function MiniCanvas(props){
    //style={{width:"100%", height:"100%"}}
    return(
        <div id="MiniCanvas"  className="MiniCanvasDiv"  ref={props.myRef}
            style={{
                        left:(props.margin !== undefined ? props.margin.x : ""),
                        top:(props.margin !== undefined ? props.margin.y : ""),
                    }}
        >
            <div id="MiniCanvas" className='MiniCanvas_PatternName'>{props.patternName}</div>
            <svg id="MiniCanvas" width='30px' height='30px' style={{position:'absolute', marginLeft:155,marginTop:5}}>
                <path d="M7.72482 0.119143L5.01695 3.25424C5.01695 3.25424 7.34669 3.254 7.45762 3.25424V12.7458C7.24833 12.7462 5.01695 12.7498 5.01695 12.7498L7.72482 15.8809C7.88368 16.0397 8.14123 16.0397 8.30009 15.8809L10.983 12.7498C10.983 12.7498 8.75167 12.7462 8.54237 12.7458V3.25424C8.75167 3.2538 10.983 3.25025 10.983 3.25025L8.30009 0.119143C8.14123 -0.0397144 7.88367 -0.0397144 7.72482 0.119143Z" fill="#3C3E45"/>
                <path d="M0.119143 8.27518L3.25424 10.9831C3.25424 10.9831 3.254 8.65331 3.25424 8.54237L12.7458 8.54237C12.7462 8.75167 12.7498 10.9831 12.7498 10.9831L15.8809 8.27518C16.0397 8.11632 16.0397 7.85876 15.8809 7.69991L12.7498 5.01695C12.7498 5.01695 12.7462 7.24833 12.7458 7.45763H3.25424C3.2538 7.24833 3.25025 5.01695 3.25025 5.01695L0.119143 7.69991C-0.0397144 7.85877 -0.0397143 8.11632 0.119143 8.27518Z" fill="#3C3E45"/>
            </svg>
            <svg xmlns="http://www.w3.org/2000/svg"
                width={StaticVars.MINI_CANVAS_SIZE.width} height={StaticVars.MINI_CANVAS_SIZE.height}
                style={{position:'absolute'}}
              id="MiniCanvas"
            >
                {props.isGraph ?
                    props.pattern.map((point, index) => (
                        index > 0 ? 
                        <g key={index}>
                            <DrawingLine  point={point} prevPoint={props.pattern[index - 1]} />
                            <PointCircle  point={point} />
                        </g>
                        :
                        <PointCircle key={index} point={point} />
                    ))
                    :
                    props.pattern.map((bar, index) => (
                        <DrawBar key={index} bar={bar} index={index} totalBars={props.pattern.length} />
                    ))
                }
            </svg>
        </div>
    );
}

/*
{pattern.map((point, index) => (
                        <DrawBar key={index} bar={pattern[index]} index={index} totalBars={pattern.length} />
                    ))}
*/

function DrawBar({bar, index, totalBars}){//{bar, index, totalBars}
    let curbar = {};
    const yBuffer = 225;
    const pos = getBarPositionByIndex(index, totalBars);
    curbar.open = yBuffer - (bar.open * 0.5);
    curbar.close = yBuffer - (bar.close * 0.5);
    curbar.high =  yBuffer - (bar.high * 0.5);
    curbar.low = yBuffer - (bar.low * 0.5);
    curbar.isGrey = bar.isGrey;
    // console.log(index + ") pos.x = " + pos.x + ", pos.y = " + pos.y); 
    return <MiniBarIcon candleInfo={curbar}
            // stroke={(curbar.open >= curbar.close ? "url(#custom_bar_green)" : "url(#custom_bar_red)")}
            // fill={bar.fill !== undefined ? bar.fill : (curbar.open >= curbar.close ? "url(#custom_bar_green)" : "url(#custom_bar_red)")} 
            strokeWidth={3}
            marginLeft={pos.x}
            marginTop={pos.y}
            scale={CANVAS_SCALE.height * 2}
            />
  
}

function getBarPositionByIndex(index, totalBars){
    const patternStartX = 175  + ((totalBars-1) * StaticVars.DEFAULT_BAR_WIDTH*CANVAS_SCALE.width);//+ (totalBars * ((StaticVars.DEFAULT_BAR_WIDTH * 1)))
    return {x: (patternStartX - (index * StaticVars.DEFAULT_BAR_WIDTH)) , y: 0};//StaticVars.MINI_CANVAS_SIZE.height - (0* CANVAS_SCALE.height)//CANVAS_SIZE.height / 2 - 50
    // const patternStartX = StaticVars.MINI_CANVAS_SIZE.width / 2 - (totalBars * ((StaticVars.DEFAULT_BAR_WIDTH * CANVAS_SCALE.width)/ 2));
    // return {x: (patternStartX + (index * StaticVars.DEFAULT_BAR_WIDTH))  * CANVAS_SCALE.width, y: 45};//StaticVars.MINI_CANVAS_SIZE.height - (0* CANVAS_SCALE.height)//CANVAS_SIZE.height / 2 - 50
}

function PointCircle({point}){
    return(
        <circle className="PointCircle" r="5" cx={point.x * CANVAS_SCALE.width} cy={StaticVars.MINI_CANVAS_SIZE.height - (point.y * CANVAS_SCALE.height)}/>
    );
}

function DrawingLine({ point, prevPoint }) {
    let line = [point, prevPoint];
    const pathData = "M " + //prevPoint.get('x') + " " + prevPoint.get('y') + point.get('x') + " " + point.get('y') + " L ";
      line
        .map(p => {
          return `${p.x * CANVAS_SCALE.width} ${StaticVars.MINI_CANVAS_SIZE.height - (p.y * CANVAS_SCALE.height)}`;
        })
        .join(" L ");
  
    return <path className="path" d={pathData} />;
}

function TotalResults({amount, isEndSearch}){
    return(
        <span className={'TotalResultsDiv' + (isEndSearch? ' EndSearch' : '')}>
            <span className='TotalResultsDivMssage'>Results Found</span>
            <span className='TotalResultsDivAmount'>{amount}</span>
        </span>
    );
}

function HighestMatch({symbol, match}){
    return(
        <span className='HighestMatchDiv'>
            <span className='HighestMatchDivMssage'>Highest Match:</span>
            {symbol !== '' ?
                    <span className='HighestMatchDivSymbol'>{symbol}</span>
                :
                null
            }
            {symbol !== '' ?
                    <span className='HighestMatchDivMatch'>{match}</span>
                :
                null
            }
        </span>
    );
}