
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
// import ListItemIcon from '@material-ui/core/ListItemIcon';
// import ListItem from '@material-ui/core/ListItem';
// import ListItemText from '@material-ui/core/ListItemText';
// import InboxIcon from '@material-ui/icons/MoveToInbox';
// import DraftsIcon from '@material-ui/icons/Drafts';
// import SendIcon from '@material-ui/icons/Send';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
// import Checkbox from '@material-ui/core/Checkbox';
import './TimeframeMenu.css';
import Divider from '@material-ui/core/Divider';
import StaticVars from '../../StaticVars';


const options = [
    '1 Minute',
    '5 Minutes',
    '30 Minutes',
    '1 Hour',
    '4 Hours',
    '1 Day',
    '1 Week',
    '1 Month'
  ];

const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
    // borderRadius:'20px',
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    {...props}
  />
));

const StyledMenuItem = withStyles((theme) => ({
  root: {
    '&:focus': {
      backgroundColor: theme.palette.primary.main,
      '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
        color: theme.palette.common.white,
      },
    },
  },
}))(MenuItem);

export default function IndexesMenu({onItemClicked, style}) {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [selectedIndex, setSelectedIndex] = React.useState(StaticVars.TIMEFRAME_MENU_DEFAULT_INDEX);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuItemClick = (event, index) => {
    onItemClicked(index);
    setSelectedIndex(index);
    setAnchorEl(null);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div style={style !== undefined ? style:null}>
    
      <Button
        className='TimeFrameDropDown'
        // aria-controls="customized-menu"
        aria-haspopup="true"
        variant="contained"
        // color='default'
        onClick={handleClick}
        // endIcon={<ExpandMoreIcon/>}
        
      >
        <div className='TimeFrameDropDown_Title'>Time Frame</div>
        <div className='TimeFrameDropDown_Text'>{options[selectedIndex]}</div>
        <div style={{position:'absolute', left:'120px', top:'14px'}}><ExpandMoreIcon/></div>
      </Button>
      <StyledMenu
        id="customized-menu"
        anchorEl={anchorEl}
        keepMounted
        disableAutoFocusItem
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {options.map((option, index) => (
            <div key={option}>
                {index === 3 || index === 5? <Divider /> : null}
                <StyledMenuItem
                    disabled={index !== StaticVars.TIMEFRAME_MENU_DEFAULT_INDEX}
                    selected={index === selectedIndex}
                    onClick={(event) => handleMenuItemClick(event, index)}
                    // disabled={index !== defaultOptionIndex}
                >
                    {option}
                </StyledMenuItem>
            </div>
        ))}
        
      </StyledMenu>
      
    </div>
  );
}
