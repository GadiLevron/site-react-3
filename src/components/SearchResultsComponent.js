import React from 'react';
import './SearchResultsComponent.css';
import CandleSymbolResultComponent from './candlesticksearch/CandleSymbolResultComponent';
import GraphSymbolResultComponent from './graphsearch/GraphSymbolResultComponent';
import { Button } from '@material-ui/core';
import ResultsTopBar from './ui/ResultsTopBar';
import StaticVars from '../StaticVars';
// import SadFaceLottie from './ui/lotties/SadFaceLottie';
// import {Searching4Icon} from './ui/SvgIcons';

export default class SearchResultsComponent extends React.Component {
    resultsArr = [];
    message = null;
    stop = false;
    resultsTimeoutId = null;
    isScrolled = false;
    showNumResults = StaticVars.DEFAULT_ALLOWED_RESULTS;
    TopBarComponentRef = React.createRef();
    _isMounted = false;
    
    constructor() {
      super();
      //console.log('constructor');
      this.state = {
        results: [],
        pattern: [],
        patternName:'',
        message : null,
        isSearchActive: false,
        canUpdate : true,
        isSearchEnd: false,
      };
      this.clearResults = this.clearResults.bind(this);
      // this.listenToScroll = this.listenToScroll.bind(this);
      // this.showMessage = this.showMessage.bind(this);
      this.addResults = this.addResults.bind(this);
      this.scrollDown = this.scrollDown.bind(this);
      this.showMoreResults = this.showMoreResults.bind(this);
      this.endSearch = this.endSearch.bind(this);
      this.showWithModal = this.showWithModal.bind(this);
      this.updatePattern = this.updatePattern.bind(this);
      this.getHighestResult = this.getHighestResult.bind(this);
      this.addToSortedList = this.addToSortedList.bind(this);
      this.isResultsChanged = this.isResultsChanged.bind(this);
    }

    // shouldComponentUpdate(){
    //   // return this.state.updateList === true;
    // }

    componentDidMount() {
      this._isMounted = true;
      //console.log('componentDidMount');
      // window.addEventListener('scroll', this.listenToScroll);
      
    }
    
    componentWillUnmount() {
      this._isMounted = false;
      //console.log('componentWillUnmount');
      // window.removeEventListener('scroll', this.listenToScroll);
    }
    
    // listenToScroll(){
    //   const winScroll =
    //     document.body.scrollTop || document.documentElement.scrollTop;
    
    //   const height =
    //     document.documentElement.scrollHeight -
    //     document.documentElement.clientHeight;
    
    //   const scrolled = winScroll / height;

    //   // if (this.state.canUpdate && scrolled >= SCROLL_TRIGGER_PERCENTAGE){
    //   //   ////console.log(scrolled);
    //   //   this.setState({
    //   //     canUpdate: false,
    //   //   }, ()=>{
    //   //     this.props.onScrollBottom();
    //   //   });
        
    //   // }
    
    //   this.setState({
    //     theposition: scrolled,
    //   });
    // }

    setCanUpdateScroll(val){
      //console.log('setCanUpdateScroll');
      // this.setState({
      //   canUpdate: val,
      // });
    }

    clearResults(callbackFunc, sliderValues){
      //console.log('clearResults');
      this.showNumResults = StaticVars.DEFAULT_ALLOWED_RESULTS;
      this.resultsArr = [];
      this.message = null;
      this.isScrolled = false;
      if (this.resultsTimeoutId !== null){
        clearTimeout(this.resultsTimeoutId);
        this.resultsTimeoutId = null;
      }
      this.setState({
        results : [],
        // pattern: [],
        isSearchActive: true,
        message : null,
        canUpdate : true,
        isSearchEnd: true,
      }, callbackFunc !== undefined ? callbackFunc(sliderValues) : null);

    }

    startResultsPolling(){
      //console.log('startResultsPolling');
      this.resultsTimeoutId = setTimeout(this.addResults, 500);
    }

    addResults(){
      //console.log('addResults');
      if (this.resultsArr.length > 0){
        const localThis = this;
        this.setState(prevState => ({
          results: this.resultsArr.slice(0, this.showNumResults),
          // updateList: (prevState.results.length < StaticVars.DEFAULT_ALLOWED_RESULTS ? true : localThis.isResultsChanged()),
        }));
      }
    }

    isResultsChanged(){
      const totalElements = Math.min(StaticVars.DEFAULT_ALLOWED_RESULTS, this.resultsArr.length);
      for (let i = 0; i < totalElements; i++){
        if (this.state.results[i].key !== this.resultsArr[i].key){
          return true;
        }
      }
      return false;
    }

    showMoreResults(){
      //console.log('showMoreResults');
      if (this.showNumResults + StaticVars.DEFAULT_ALLOWED_RESULTS <= this.resultsArr.length){
        this.showNumResults += StaticVars.DEFAULT_ALLOWED_RESULTS;
      }
      else{
        this.showNumResults = this.resultsArr.length;
      }

      this.addResults();
    }

    addToSortedList(resDataArr){
      //console.log('addToSortedList (' + resDataArr.results.length + ')');
      for (let i = 0; i < resDataArr.results.length; i++){
        // //console.log('( '+resDataArr.results[i].symbol+' )');
        resDataArr.results[i].key = resDataArr.results[i].symbol + "_" + resDataArr.results[i].r;
      }
      this.resultsArr = this.resultsArr.concat(resDataArr.results);
      this.resultsArr = this.resultsArr.sort((a,b) => a.r < b.r ? 1 : -1);
      if (this.TopBarComponentRef.current !== null){
        this.TopBarComponentRef.current.updateTotalResults(this.resultsArr.length);
        this.TopBarComponentRef.current.updateHighestResult(this.resultsArr[0].symbol, this.resultsArr[0].r);
      }
      this.addResults();
    }

    getHighestResult(){
      //console.log('getHighestResult');
      return {symbol: this.resultsArr[0].symbol, r: this.resultsArr[0].r, total: this.resultsArr.length};
    }

    showResult(resData){
      //console.log('showResult');
      this.addToSortedList(resData);
      
    }

    setActiveSearch(isSearchActive){
      //console.log('setActiveSearch');
      this.setState({
        isSearchActive: true,
      }, this.scrollDown);
    }

    scrollDown(){
      //console.log('scrollDown');
      // if (this.isScrolled === false){
      //   this.isScrolled = true;
      //   window.scrollTo({ behavior: 'smooth', top: 470 });
      // }
    }

    endSearch(){
      //console.log('endSearch');
      if (this.state.results.length === 0 && this.resultsArr.length === 0){
        this.setState({
          isSearchEnd: true,
        });
      }

      if (this.TopBarComponentRef.current !== null){
        this.TopBarComponentRef.current.endSearch();
      }
    }

    updatePattern(ipattern, patternName){
      // //console.log('updatePattern');
      // if (this.TopBarComponentRef.current !== null){
      //   this.TopBarComponentRef.current.updatePattern(pattern);
      // }
      const arr = ipattern.concat([]);
      
      this.setState({
        pattern: arr,
        patternName: patternName,
      });
    }

    showWithModal(isShow){
      // //console.log('showWithModal');
      if (this.TopBarComponentRef !== undefined){
        this.TopBarComponentRef.current.showWithModal(isShow);
      }
    }

    render() {
      if (this.state.isSearchActive === true){
          return (
                <div className='SearchResultsMainDiv'>
                  {this.state.results.length > 0 ? 
                    <div className='SearchResultDivWithResults'>
                      <ResultsTopBar ref={this.TopBarComponentRef} pattern={this.state.pattern} patternName={this.state.patternName} getHighestResult={this.getHighestResult}/>
                      <ul className='ResultsList'>
                        {
                          this.state.results.map((res, i) =>{
                            return(
                              res.prices !== undefined ?
                                (res.isCandles ? 
                                  this._isMounted ? <CandleSymbolResultComponent key={this.state.results[i].key}  res={res} size="regular" showWithModal={this.showWithModal}/> : null
                                  :
                                  this._isMounted ? <GraphSymbolResultComponent key={this.state.results[i].key}  res={res} size="regular" showWithModal={this.showWithModal}/>: null
                                )
                              :
                              null
                            )
                          })
                        }
                      </ul>
                    </div>
                    :
                    null
                  }
                  {this.state.results.length > 0 && this.resultsArr.length > this.state.results.length ? 
                    <div className='SearchResultsMoreButtonListItem'><Button onClick={this.showMoreResults} className='SearchResultsMoreButton'>CLICK FOR MORE RESULTS</Button></div>
                    :
                    null
                  }
                </div>
          )
      }
      else{
        return(
          <div className='SearchResultDivWithoutResults'>

          </div>
        )
      }
    }
}//end class