
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import Zoom from '@material-ui/core/Zoom';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import './CandleFixedPatternMenu.css';

const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
    // borderRadius:'20px',
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    {...props}
  />
));

const StyledMenuItem = withStyles((theme) => ({
  root: {
    '&:focus': {
      backgroundColor: theme.palette.primary.main,
      '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
        color: theme.palette.common.white,
        
      },
    },
  },
}))(MenuItem);

export default function CandleBearishFixedPatternMenu({onItemClicked}) {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleItemClicked = (event) =>{
    //event.currentTarget.id;
    onItemClicked(event.currentTarget.id);
    handleClose();
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const UITooltip = withStyles((theme) => ({
    tooltip: {
      backgroundColor: '#f5f5f9',
      color: 'rgba(0, 0, 0, 0.87)',
      maxWidth: 220,
      fontSize: theme.typography.pxToRem(17),
      border: '1px solid #dadde9',
    },
  }))(Tooltip);

  return (
    <div>
    
      <button
        className='CandleCanvasBearishPatternsButton'
        aria-controls="customized-menu"
        aria-haspopup="true"
        variant="contained"
        // color='default'
        onClick={handleClick}
        // endIcon={<ExpandMoreIcon/>}
      >
        <UITooltip arrow TransitionComponent={Zoom} title="Bearish Patterns">
            <div className='CandleCanvasDropdownImageDiv'>
              <img alt='bearishsvg2' className='CandleCanvasBearishDropdownImageDiv_img'></img>
            </div>
          </UITooltip>
      </button>
    
      <StyledMenu
        id="customized-menu"
        anchorEl={anchorEl}
        keepMounted
        disableAutoFocusItem
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <StyledMenuItem id='HangingMan' onClick={handleItemClicked}>
          <ListItemIcon>
            <img className="CandleFixedPatternsImage" src={require("../../assets/hangingman.svg")} alt="hnst"/>
            {/* <SendIcon fontSize="small" /> */}
          </ListItemIcon>
          <ListItemText primary="Hanging Man" />
        </StyledMenuItem>

        <StyledMenuItem id='ShootingStar' onClick={handleItemClicked}>
          <ListItemIcon>
            <img className="CandleFixedPatternsImage" src={require("../../assets/shootingstar.svg")} alt="hnsb"/>
            {/* <DraftsIcon fontSize="small" /> */}
          </ListItemIcon>
          <ListItemText primary="Shooting Star" />
        </StyledMenuItem>

        <StyledMenuItem id='GravestoneDoji' onClick={handleItemClicked}>
          <ListItemIcon>
            <img className="CandleFixedPatternsImage" src={require("../../assets/gravestonedoji.svg")} alt="hnsb"/>
            {/* <InboxIcon fontSize="small" /> */}
          </ListItemIcon>
          <ListItemText primary="Gravestone Doji" />
        </StyledMenuItem>

        <StyledMenuItem id='BearishEngulfing' onClick={handleItemClicked}>
          <ListItemIcon>
            <img className="CandleFixedPatternsImage" src={require("../../assets/bearishengulfing.svg")} alt="hnsb"/>
            {/* <InboxIcon fontSize="small" /> */}
          </ListItemIcon>
          <ListItemText primary="Bearish Engulfing" />
        </StyledMenuItem>

        <StyledMenuItem id='BearishHarami' onClick={handleItemClicked}>
          <ListItemIcon>
             <img className="CandleFixedPatternsImage" src={require("../../assets/bearishharami.svg")} alt="hnsb"/>
            {/* <InboxIcon fontSize="small" /> */}
          </ListItemIcon>
          <ListItemText primary="Bearish Harami" />
        </StyledMenuItem>

        <StyledMenuItem id='BearishKicker' onClick={handleItemClicked}>
          <ListItemIcon>
             <img className="CandleFixedPatternsImage" src={require("../../assets/bearishkicker.svg")} alt="hnsb"/>
            {/* <InboxIcon fontSize="small" /> */}
          </ListItemIcon>
          <ListItemText primary="Bearish Kicker" />
        </StyledMenuItem>

        <StyledMenuItem id='EveningStar' onClick={handleItemClicked}>
          <ListItemIcon>
             <img className="CandleFixedPatternsImage" src={require("../../assets/eveningstar.svg")} alt="hnsb"/>
            {/* <InboxIcon fontSize="small" /> */}
          </ListItemIcon>
          <ListItemText primary="Evening Star" />
        </StyledMenuItem>

        <StyledMenuItem id='BearishAbandonedBaby' onClick={handleItemClicked}>
          <ListItemIcon>
             <img className="CandleFixedPatternsImage" src={require("../../assets/bearishabandonedbaby.svg")} alt="hnsb"/>
            {/* <InboxIcon fontSize="small" /> */}
          </ListItemIcon>
          <ListItemText primary="Bearish Abandoned Baby" />
        </StyledMenuItem>

        <StyledMenuItem id='ThreeCrows' onClick={handleItemClicked}>
          <ListItemIcon>
             <img className="CandleFixedPatternsImage" src={require("../../assets/threecrows.svg")} alt="hnsb"/>
            {/* <InboxIcon fontSize="small" /> */}
          </ListItemIcon>
          <ListItemText primary="Three Crows" />
        </StyledMenuItem>

        <StyledMenuItem id='BearishThreeLineStrike' onClick={handleItemClicked}>
          <ListItemIcon>
             <img className="CandleFixedPatternsImage" src={require("../../assets/bearishthreelinestrike.svg")} alt="hnsb"/>
            {/* <InboxIcon fontSize="small" /> */}
          </ListItemIcon>
          <ListItemText primary="Bearish Three Line Strike" />
        </StyledMenuItem>

      </StyledMenu>
      {/* </StylesProvider> */}
    </div>
  );
}
