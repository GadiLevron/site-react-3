import React from 'react';
import './CandleUIComponent.css';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import Zoom from '@material-ui/core/Zoom';
import { withStyles } from '@material-ui/core/styles';
import MyCanvas from '../candlesticksearch/CandlestickCanvas';
import BullishFixedPatternMenu from './CandleBullishFixedPatternMenu';
import BearishFixedPatternMenu from './CandleBearishFixedPatternMenu';
import IndexesMenu from '../ui/IndexesMenu';
import SadFaceLottie from '../ui/lotties/SadFaceLottie';
import ScrollDownLottie from '../ui/lotties/ScrollDownLottie';
import {Searching5Icon, AddGreenBarIcon, AddRedBarIcon, AddGreyBarIcon} from '../ui/SvgIcons';
import StaticVars from '../../StaticVars';
import TimeframeMenu from '../ui/TimeframeMenu';
import BottomUIComponent from '../ui/BottomUiComponent';


export default class CandleUIComponent extends React.Component {

  sliderValues_minMatchPercent = null;
  sliderValues_minBarRange = null;
  myCanvasRef = null;
  bottomUIRef = null;
  minBarSliderRef = null;

  constructor(){
    super();
    this.sliderValues_minMatchPercent = [80, 100];
    this.state = {
      isSearchActive : false,
      isFoundResult: false,
      isSearchEnd: false,
      numOfPoints: 0,
      progress: 0,
      timeFrameIndex: StaticVars.TIMEFRAME_MENU_DEFAULT_INDEX,
      patternName: StaticVars.CUSTOM_PATTERN_TEXT,
    };

    this.myCanvasRef = React.createRef();
    this.minBarSliderRef = React.createRef();
    this.bottomUIRef = React.createRef();

    this.searchButtonClicked = this.searchButtonClicked.bind(this);
    this.searchOptionsButtonClicked = this.searchOptionsButtonClicked.bind(this);
    this.setSearchActive = this.setSearchActive.bind(this);
    this.getMinBarRange = this.getMinBarRange.bind(this);
    this.updateBarRangeValues = this.updateBarRangeValues.bind(this);
    this.updateNumOfPoints = this.updateNumOfPoints.bind(this);
    this.getPattern = this.getPattern.bind(this);
    this.getPatternName = this.getPatternName.bind(this);
    this.getDrawRightDirection = this.getDrawRightDirection.bind(this);
    this.onCanvasClicked = this.onCanvasClicked.bind(this);
    this.onFixedPatternsChange = this.onFixedPatternsChange.bind(this);
    this.foundResult = this.foundResult.bind(this);
    this.endSearch = this.endSearch.bind(this);
    this.clearSearch = this.clearSearch.bind(this);
    this.updatePatternName = this.updatePatternName.bind(this);
  }

  searchButtonClicked(){
    this.props.searchButtonCallback(this.state.timeFrameIndex);
    // if (this.props.searchButtonCallback(this.sliderValues) === true){
    //   this.setState({
    //     minimizeSearch : true,
    //   });
    // }
  }

  clearSearch(){
    // this.props.clearResults();
    this.setState({
      isSearchActive: false,
      isFoundResult: false,
      isSearchEnd: false,
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.state.patternName !== nextState.patternName ;//|| (this.state.isSearchActive && nextState.isSearchEnd)
  }

  updatePatternName(patternName){
    this.setState({
      patternName:patternName,
    });
  }

  getPattern(){
    return this.myCanvasRef.current.getPattern();
  }

  getPatternName(){
    return this.myCanvasRef.current.getPatternName();
  }

  getDrawRightDirection(){
    return this.myCanvasRef.current.getDrawRightDirection();
  }

  undoLastPoint(){
    this.props.undoLastPoint();
    return this.myCanvasRef.current.undoLastPoint();
  }

  // clearCanvas(){
  //   this.updateNumOfPoints(0);
  //   this.updateBarRangeValues([0]);
  //   this.props.clearCanvas();
  //   return this.myCanvasRef.current.clearCanvas();
  // }

  searchOptionsButtonClicked(){

  }

  drawPattern(patternId){
    this.myCanvasRef.current.drawPattern(patternId);
  }

  setSearchActive(isActive){
    this.bottomUIRef.current.setSearchActive(isActive);
    this.setState({
      isSearchActive : isActive,
      isSearchEnd: false,
      progress: 0,
    });
  }

  onCanvasClicked(){
    this.props.onCanvasClicked();
  }

  getMinBarRange(){
    return this.sliderValues_minBarRange;
  }

  updateBarRangeValues(val){
    this.sliderValues_minBarRange = val;
    // console.log(this.sliderValues_minBarRange);
  }

  updateNumOfPoints(numOfPoints){
    this.updateBarRangeValues([numOfPoints]);
    this.setState({
      numOfPoints: numOfPoints,
    });
  }

  onFixedPatternsChange(patternId){
    this.drawPattern(patternId);
  }
  
  endSearch(){
    this.bottomUIRef.current.endSearch();
    this.setState({
      isSearchEnd: true,
      // isSearchActive:false,
    });
  }

  foundResult(){
    this.bottomUIRef.current.foundResult();
    this.setState({
      isFoundResult: true,
    });
    // window.scrollTo({ behavior: 'smooth', top: 1470 });
  }

  clearCanvas(){
    this.myCanvasRef.current.clearCanvas();
  }

  loadFromLocalStorage(){
    this.myCanvasRef.current.loadFromLocalStorage();
  }

  addDefaultCandle(event){
    this.myCanvasRef.current.addDefaultCandle(event);
  }

  showProgress(progress){
    this.bottomUIRef.current.showProgress(progress);
  }

  onTimeFrameSelected(timeFrameIndex){
    this.setState({
      timeFrameIndex: timeFrameIndex,
    });
  }

  render() {
      return (
        <div className="CandleSearchUIdiv" >
          <MyCanvas ref={this.myCanvasRef} onCanvasClicked={this.onCanvasClicked} updateNumOfPoints={this.updateNumOfPoints} updatePatternName={this.updatePatternName} />
          <div className='CandleCanvas_PatternText'>{this.state.patternName}</div>
          <div className='CandleCanvas_DrawMessage'>{StaticVars.DRAW_ANYTHING_MESSAGE}</div>
          <UITooltip arrow TransitionComponent={Zoom} title="Clear Canvas">
            <button className="CandleCanvasClearButton" onClick={() => this.clearCanvas()}>
                <img alt='' data-tip="Clear Canvas"></img>
            </button>
          </UITooltip>
          <UITooltip arrow TransitionComponent={Zoom} title="Recover Last Pattern">
          <button className="CandlestickCanvasRecoverButton" onClick={() => this.loadFromLocalStorage()}>
            <img alt='' data-tip="Draw Last Pattern"></img>
          </button>
          </UITooltip>
          <UITooltip arrow TransitionComponent={Zoom} title="Add Green Candle">
            <button className="AddGreenCandleButton" id="DefaultGreenBar" data-tip="Add Candle" onClick={(event) => this.addDefaultCandle(event)} >
              <AddGreenBarIcon marginLeft={5} marginTop={0} scale={1}/>
            </button>
          </UITooltip>
          <UITooltip arrow TransitionComponent={Zoom} title="Add Red Candle">
            <button className="AddRedCandleButton" id="DefaultRedBar" data-tip="Add Candle" onClick={(event) => this.addDefaultCandle(event)} >
              <AddRedBarIcon marginLeft={5} marginTop={0} scale={1}/>
            </button>
          </UITooltip>
          <UITooltip arrow TransitionComponent={Zoom} title="Add Default Candle (can be green or red)">
            <button className="AddGreyCandleButton" id="DefaultGreyBar" data-tip="Add Candle" onClick={(event) => this.addDefaultCandle(event)} >
              <AddGreyBarIcon marginLeft={5} marginTop={0} scale={1}/>
            </button>
          </UITooltip>
          <BullishFixedPatternMenu onItemClicked={this.onFixedPatternsChange}/>
          <BearishFixedPatternMenu onItemClicked={this.onFixedPatternsChange}/>

          <IndexesMenu style={{left:'1530px', top:'477px'}} />
          <TimeframeMenu style={{position:'absolute', left:'1750px', top:'477px'}} onItemClicked={this.onTimeFrameSelected} />
          <Button className="CandleSearchButton" onClick={() => this.searchButtonClicked()}>
            Plot it!
          </Button>
          <BottomUIComponent ref={this.bottomUIRef}/>
        {/* {this.state.isSearchActive ? 
            (this.state.isFoundResult ?
              <ScrollDownLottie width={100} height={150} position={{x:1555, y:540}}/>
            :
              (this.state.isSearchEnd === false?
                <div className='CandleSearchingAnimationDiv'>
                  <Searching5Icon/>
                </div>
              :
                <div className='Candle_NoMatchMessageDiv'>
                  <SadFaceLottie width={90} height={85} position={{x:0, y:110}}/>
                  <div className='Candle_NoMatchMessage'>Sorry, no matches found</div>
                </div>
              )
            )
          :
          null
        } */}
      </div>
    )
  }
}//end class

const UITooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: '#f5f5f9',
    color: 'rgba(0, 0, 0, 0.87)',
    maxWidth: 320,
    fontSize: theme.typography.pxToRem(17),
    border: '1px solid #dadde9',
  },
}))(Tooltip);