import React from 'react';
import './CandleSymbolResultComponent.css';
import SymbolGraph from '../ui/SymbolGraph';
import MODAL from 'react-modal';
import { TradingViewEmbed, widgetType } from "react-tradingview-embed";
import { Button } from '@material-ui/core';

export default class CandleSymbolResultComponent extends React.Component {


    constructor(props) {
      super(props);

      if (props.res !== undefined){
        this.state = {
          isEnlarge: false,
          data : props.res,
          size : props.size,
          class: 'CandleResultsListItem',
        };
      }

      this.handleMouseClick = this.handleMouseClick.bind(this);
      this.handleCloseModal = this.handleCloseModal.bind(this);
    }

    componentDidMount(){
      setTimeout(() => {
        this.animateItem();
      }, 50);
    }

    handleMouseClick(){
      this.setState({
        isEnlarge: true,
      });
      this.props.showWithModal(true);
    }

    handleCloseModal(){
      this.setState({
        isEnlarge: false,
      });
      this.props.showWithModal(false);
    }

    setMessage(message){
      this.setState({
        message : message,
      });
    }

    getDate(unixTime){
      let day = new Date(unixTime * 1000);
      return day.getDate() + "/"+ parseInt(day.getMonth()+1) +"/"+day.getFullYear();
    }

    animateItem(){
      this.setState({
        class:'CandleResultsListItem show',
      });
    }

    render() {
        return (
          <div>
          <div className='CandleSymbolResultDiv' 
                // onMouseUp={this.handleMouseClick}
              >
                <li className={this.state.class} style={{display:'flex', flexDirection:'row'}} >
                  <SymbolGraph data={this.state.data} isEnlarge={false}/>
                  <div style={{display:"flex", flexDirection:"row"}}>
                    <div style={{backgroundColor:'rgba(0,0,0, 0)'}}>
                      <span className='SymbolResultTitle_Symbol'>{this.state.data.symbol}</span>
                      <span className='SymbolResultTitle_MatchDiv'>
                        <span className='SymbolResultTitle_MatchValue'>{this.state.data.r.toFixed(2)}%</span>
                        <span className='SymbolResultTitle_MatchText'>Match</span>
                      </span>
                    </div>
                    <Button onClick={this.handleMouseClick} className='SymbolResult_InfoButton'>CLICK FOR DETAILS</Button>
                  </div>
                </li>
            </div>
          {this.state.isEnlarge ?
            <MODAL 
              isOpen={this.state.isEnlarge}
              className="CanvasModal"
              overlayClassName="CanvasModalOverlay"
              shouldCloseOnOverlayClick={true}
              onRequestClose={this.handleCloseModal}
              ariaHideApp={false}
            >
              <div className='SymbolResultDivModal'>
                {/* <li className={this.state.class + " modal"}> */}
                  {/* <div style={{width:'4em', float:'left'}}> <br></br>{this.state.data.symbol} <br></br> {this.state.data.r}%</div> */}
                  <SymbolGraph data={this.state.data} isEnlarge={true}/>
                {/* </li> */}
              </div>
            </MODAL> : null}
            </div>
        );
    }
}//end class

//MODAL.setAppElement('#app-base');

/*
style={{display:'flex', flexDirection:'row'}}
<br></br>X:{this.state.data.XRatio}  

({this.state.data.numOfBars} bars back, latest bar is {this.getDate(this.state.data.prices[0].date)})

<hr></hr>
*/