import React from 'react';
import './CandleEngineComponent.css';
import CandleUIComponent from './CandleUIComponent';
import SSE from "../../lib/sse";
import SadFaceLottie from '../ui/lotties/SadFaceLottie';
import {Searching4Icon} from '../ui/SvgIcons';


// const delay = ms => new Promise(res => setTimeout(res, ms));

export default class CandleEngineComponent extends React.Component {
    sliderValues_minMatchPercent = [80];  //min percent to show results
    sliderValues_startFrom = [0]; //min match to calculate x ratio
    sliderValues_xInfluence = [50]; //x ratio weight
    sliderValues_minBarRange = null;  //min bars
    isSearchAllowed = true;
    allowSearchTimeoutId = null;
    timeFrameIndex = -1;

    constructor() {
      super();
    
      this.state = {
        //lines: new Immutable.List(),
        height: window.innerHeight / 2,
        isSearchActive : false,
      };
      this.searchButtonCallback = this.searchButtonCallback.bind(this);
      this.onShowResult = this.onShowResult.bind(this);
      this.clearResults = this.clearResults.bind(this);
      this.calcPatterns = this.calcPatterns.bind(this);
      this.clearCanvas = this.clearCanvas.bind(this);
      this.undoLastPoint = this.undoLastPoint.bind(this);
      this.onCanvasClicked = this.onCanvasClicked.bind(this);
      this.updateNumOfLines = this.updateNumOfLines.bind(this);
      this.startAllowTimer = this.startAllowTimer.bind(this);
      this.allowSearch = this.allowSearch.bind(this);
      this.endSearch = this.endSearch.bind(this);
      this.loadIdFromLocalStorage = this.loadIdFromLocalStorage.bind(this);
      this.saveIdToLocalStorage = this.saveIdToLocalStorage.bind(this);

      
    }

    componentDidMount(){
      //this.loadIdFromLocalStorage();
    }

    saveIdToLocalStorage(clientID){
      localStorage.setItem('clientID', clientID);
    }
  
    loadIdFromLocalStorage(){
      const cID = localStorage.getItem('clientID');
      if (cID){
        this.setState({
          clientID: cID,
        });
      }
    }

    async searchButtonCallback(timeFrameIndex){
      if (!this.isSearchAllowed){
        return;
      }
      
      this.timeFrameIndex = timeFrameIndex;
      const pattern = this.refs.searchUIComponent.getPattern();
      if (pattern === undefined || pattern.length < 1){
        return false;
      }

      this.isSearchAllowed = false;
      this.startAllowTimer();

      this.clearResults(this.calcPatterns);
      this.setSearchActive(true);
      // await delay(500);

      return true;
    }

    startAllowTimer(){
      this.allowSearchTimeoutId = setTimeout(this.allowSearch, 3000);
    }

    allowSearch(){
      this.isSearchAllowed = true;
    }
    
    updateSliderValues_startFrom(val){
      this.sliderValues_startFrom = val;
    }
  
    updateSliderValues_xInfluence(val){
      this.sliderValues_xInfluence = val;
    }

    calcPatterns(){
      const pattern = this.refs.searchUIComponent.getPattern();  //this works because of bind in constructor
      const patternName = this.refs.searchUIComponent.getPatternName();  //this works because of bind in constructor
      this.props.updatePattern(pattern, patternName);
    
      let data = {
        pattern : pattern,
        // clientID : this.state.clientID,
        isCandles: true,
        patternName: patternName,
        timeFrameIndex: this.timeFrameIndex,
      }

      this.setState({
        pattern : pattern,
        finishedSearch : false,
        endSearch: false,
        isSearchActive : true,
        isCandles: true,
      });

      const localThis = this;

      // console.log("sending pattern = " + pattern);
      console.log("start search");

      // this.props.onShowResultsMessage("sending pattern to server");
      //console.log("sending pattern to server");
      try{
        const source = SSE.SSE(process.env.NODE_ENV === 'production' ? "/api/candlestick-search" : "http://localhost:8080/api/candlestick-search", {headers: {'Content-Type': 'application/json'}, payload: JSON.stringify(data)});
        source.onmessage=function(event)
        {
          //console.log("response = " + event.data);
          const resData = JSON.parse(event.data);
          if (resData.finishedSearch === true){
            console.log("finished search");
          }
          
          if (resData.endSearch === true){
            console.log("end search");
            localThis.endSearch();
            localThis.setState({
              endSearch: resData.endSearch,
            });
          }
            
          if (resData.clientID !== undefined){
            // localThis.saveIdToLocalStorage(resData.clientID);
            localThis.setState({
              clientID: resData.clientID,
            });
          }
              
          if (resData.progress !== undefined){
            if (localThis.refs.searchUIComponent !== undefined){
              localThis.refs.searchUIComponent.showProgress(resData.progress);
            }
          }
                
          if (resData.results !== undefined){
            localThis.onShowResult(resData);
          }
        };

        source.stream();
      }
      catch(e){
        console.log(e);
        localThis.endSearch();
      }

    }//end calcPatterns

    setCanUpdateScroll(val){
      // this.props.setCanUpdateScroll(val);
    }

    onShowResult(resData){
      // console.log(JSON.stringify(resData));
      this.props.onShowResult(resData);
      if (this.refs.searchUIComponent !== undefined){
        this.refs.searchUIComponent.foundResult();
      }
    }

    clearResults(callbackFunc){
      this.props.clearResults(callbackFunc);
      if (this.refs.searchUIComponent !== undefined){
        this.refs.searchUIComponent.clearSearch();
      }
    }

    endSearch(){
      this.setState({
        isSearchEnd: true,
      })
      this.props.endSearch();
      if (this.refs.searchUIComponent !== undefined){
        this.refs.searchUIComponent.endSearch();
      }
    }

    undoLastPoint(){
      // this.refs.searchUIComponent.undoLastLine();
    }

    clearCanvas(){
      // this.refs.searchUIComponent.clearCanvas();
    }

    setSearchActive(isActive){
      this.setState({
        isSearchActive: isActive,
      })
      if (this.refs.searchUIComponent !== undefined){
        this.refs.searchUIComponent.setSearchActive(isActive);
      }
      this.props.setActiveSearch(isActive);
      // this.refs.myCanvas.setSearchActive(isActive);
      // this.refs.searchPropertiesTopBar.setSearchActive(isActive);
    }

    onCanvasClicked(){
      this.setSearchActive(false);
    }

    updateNumOfLines(numOfLines){
      if (this.refs.searchUIComponent !== undefined){
        this.refs.searchUIComponent.updateNumOfLines(numOfLines);
      }
    }

    render() {
      return (
        <div className='CandleSearchEngineDiv'>
              <CandleUIComponent ref="searchUIComponent" searchButtonCallback = {this.searchButtonCallback} clearResults = {this.clearResults}
                undoLastPoint = {this.undoLastPoint} clearCanvas = {this.clearCanvas} onCanvasClicked={this.onCanvasClicked}
              />
              
            
        </div>
      )
       
    }
}//end class
