
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import Zoom from '@material-ui/core/Zoom';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
    // borderRadius:'20px',
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    {...props}
  />
));

const StyledMenuItem = withStyles((theme) => ({
  root: {
    '&:focus': {
      backgroundColor: theme.palette.primary.main,
      '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
        color: theme.palette.common.white,
      },
    },
  },
}))(MenuItem);

export default function CandleBullishFixedPatternMenu({onItemClicked}) {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleItemClicked = (event) =>{
    //event.currentTarget.id;
    onItemClicked(event.currentTarget.id);
    handleClose();
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const UITooltip = withStyles((theme) => ({
    tooltip: {
      backgroundColor: '#f5f5f9',
      color: 'rgba(0, 0, 0, 0.87)',
      maxWidth: 220,
      fontSize: theme.typography.pxToRem(17),
      border: '1px solid #dadde9',
    },
  }))(Tooltip);

  return (
    <div>
    
        <button
          className='CandleCanvasBullishPatternsButton'
          aria-controls="customized-menu"
          aria-haspopup="true"
          variant="contained"
          // color='default'
          onClick={handleClick}
          // endIcon={<ExpandMoreIcon/>}
        >
          <UITooltip arrow TransitionComponent={Zoom} title="Bullish Patterns">
            <div className='CandleCanvasDropdownImageDiv'>
              <img alt='bullishsvg' className='CandleCanvasBullisDropdownImageDiv_img'></img>
            </div>
        </UITooltip>
        </button>
      
      <StyledMenu
        id="customized-menu"
        anchorEl={anchorEl}
        keepMounted
        disableAutoFocusItem
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <StyledMenuItem id='Hammer' onClick={handleItemClicked}>
          <ListItemIcon>
            <img className="CandleFixedPatternsImage" src={require("../../assets/hammer.svg")} alt="hnst"/>
            {/* <SendIcon fontSize="small" /> */}
          </ListItemIcon>
          <ListItemText primary="Hammer" />
        </StyledMenuItem>

        <StyledMenuItem id='InvertedHammer' onClick={handleItemClicked}>
          <ListItemIcon>
            <img className="CandleFixedPatternsImage" src={require("../../assets/invertedhammer.svg")} alt="hnsb"/>
            {/* <DraftsIcon fontSize="small" /> */}
          </ListItemIcon>
          <ListItemText primary="Inverted Hammer" />
        </StyledMenuItem>

        <StyledMenuItem id='DragonFlyDoji' onClick={handleItemClicked}>
          <ListItemIcon>
            <img className="CandleFixedPatternsImage" src={require("../../assets/dragonflydoji.svg")} alt="hnsb"/>
            {/* <InboxIcon fontSize="small" /> */}
          </ListItemIcon>
          <ListItemText primary="DragonFly Doji" />
        </StyledMenuItem>

        <StyledMenuItem id='BullishEngulfing' onClick={handleItemClicked}>
          <ListItemIcon>
            <img className="CandleFixedPatternsImage" src={require("../../assets/bullishengulfing.svg")} alt="hnsb"/>
            {/* <InboxIcon fontSize="small" /> */}
          </ListItemIcon>
          <ListItemText primary="Bullish Engulfing" />
        </StyledMenuItem>

        <StyledMenuItem id='BullishHarami' onClick={handleItemClicked}>
          <ListItemIcon>
             <img className="CandleFixedPatternsImage" src={require("../../assets/bullishharami.svg")} alt="hnsb"/>
            {/* <InboxIcon fontSize="small" /> */}
          </ListItemIcon>
          <ListItemText primary="Bullish Harami" />
        </StyledMenuItem>

        <StyledMenuItem id='BullishKicker' onClick={handleItemClicked}>
          <ListItemIcon>
             <img className="CandleFixedPatternsImage" src={require("../../assets/bullishkicker.svg")} alt="hnsb"/>
            {/* <InboxIcon fontSize="small" /> */}
          </ListItemIcon>
          <ListItemText primary="Bullish Kicker" />
        </StyledMenuItem>

        <StyledMenuItem id='MorningStar' onClick={handleItemClicked}>
          <ListItemIcon>
             <img className="CandleFixedPatternsImage" src={require("../../assets/morningstar.svg")} alt="hnsb"/>
            {/* <InboxIcon fontSize="small" /> */}
          </ListItemIcon>
          <ListItemText primary="Morning Star" />
        </StyledMenuItem>

        <StyledMenuItem id='BullishAbandonedBaby' onClick={handleItemClicked}>
          <ListItemIcon>
             <img className="CandleFixedPatternsImage" src={require("../../assets/bullishabandonedbaby.svg")} alt="hnsb"/>
            {/* <InboxIcon fontSize="small" /> */}
          </ListItemIcon>
          <ListItemText primary="Bullish Abandoned Baby" />
        </StyledMenuItem>

        <StyledMenuItem id='ThreeSoldiers' onClick={handleItemClicked}>
          <ListItemIcon>
             <img className="CandleFixedPatternsImage" src={require("../../assets/threesoldiers.svg")} alt="hnsb"/>
            {/* <InboxIcon fontSize="small" /> */}
          </ListItemIcon>
          <ListItemText primary="Three Soldiers" />
        </StyledMenuItem>

        <StyledMenuItem id='BullishThreeLineStrike' onClick={handleItemClicked}>
          <ListItemIcon>
             <img className="CandleFixedPatternsImage" src={require("../../assets/bullishthreelinestrike.svg")} alt="hnsb"/>
            {/* <InboxIcon fontSize="small" /> */}
          </ListItemIcon>
          <ListItemText primary="Bullish Three Line Strike" />
        </StyledMenuItem>

      </StyledMenu>
      {/* </StylesProvider> */}
    </div>
  );
}
