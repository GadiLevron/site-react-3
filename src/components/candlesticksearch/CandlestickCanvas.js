import React from 'react';
//import ReactDOM from 'react-dom';
import Immutable from 'immutable';
import './CandlestickCanvas.css';
import {EditBarBackground, CustomBarIcon, EditBarTrash} from '../ui/SvgIcons';
import StaticVars from '../../StaticVars';
// import Button from '@material-ui/core/Button';
// import AnimateHeight from 'react-animate-height';
// import { FilledInput } from '@material-ui/core';


const GRIDLINES = {horizontal:10, vertical: 40};
const BAR_WIDTH = 60;


export default class CandlestickCanvas extends React.Component {
  constructor() {
    super();

    this.state = {
      bars: new Immutable.List(),
      patternName:StaticVars.CUSTOM_PATTERN_TEXT,
      currentBarIndex: -1,
      isHover: false,
      barEditPrice: "",
      isDrawing: false,
      isCandles: true,
      
      isDrag: false,

      // points: new Immutable.List(),
      // isRightDirection : null,
      // isSearchActive:false,
      // currentPointIndex: -1,
      // savedOriginalPoint: null,
      // pointJustCreated: false,
      // isHoverOnPoint : false,
    };

    this.handleMouseDown = this.handleMouseDown.bind(this);
    this.handleMouseMove = this.handleMouseMove.bind(this);
    this.handleMouseLeave = this.handleMouseLeave.bind(this);
    this.handleMouseUp = this.handleMouseUp.bind(this);
    this.handleDragHigh = this.handleDragHigh.bind(this);
    this.handleDragLow= this.handleDragLow.bind(this);
    this.handleDragClose = this.handleDragClose.bind(this);
    this.handleDragOpen = this.handleDragOpen.bind(this);
    // this.isPointGoodDirection = this.isPointGoodDirection.bind(this);

    this.getBarIndexForMousePosition = this.getBarIndexForMousePosition.bind(this);
    this.barEditMouseDown = this.barEditMouseDown.bind(this);
    this.setNewState = this.setNewState.bind(this);
    this.loadFromLocalStorage = this.loadFromLocalStorage.bind(this);
    this.saveToLocalStorage = this.saveToLocalStorage.bind(this);
    this.isKnownPattern = this.isKnownPattern.bind(this);
    this.getPatternName = this.getPatternName.bind(this);
    this.getPattern = this.getPattern.bind(this);
    
  }

  componentDidMount() {
    document.addEventListener("mouseup", this.handleMouseUp);
    // this.loadFromLocalStorage();
  }

  componentWillUnmount() {
    document.removeEventListener("mouseup", this.handleMouseUp);
  }

  setNewState(newState){
    if (newState.bars !== undefined && newState.bars.size > 0){
      // if (newState.bars.size === 0){
      //   newState.patternName = StaticVars.CUSTOM_PATTERN_TEXT;
      // }
      // else{
      this.saveToLocalStorage(newState.bars);
      // }
    }

    if (newState.patternName !== undefined){
      this.props.updatePatternName(newState.patternName);
    }

    this.setState(prevState => ({
      bars: newState.bars !== undefined ? newState.bars : prevState.bars,
      patternName: newState.patternName !== undefined ? newState.patternName : prevState.patternName,
      currentBarIndex: newState.currentBarIndex !== undefined ? newState.currentBarIndex : prevState.currentBarIndex,
      isHover: newState.isHover !== undefined ? newState.isHover : prevState.isHover,
      barEditPrice: newState.barEditPrice !== undefined ? newState.barEditPrice : prevState.barEditPrice,
      isDrawing: newState.isDrawing !== undefined ? newState.isDrawing : prevState.isDrawing,
      isCandles: newState.isCandles !== undefined ? newState.isCandles : prevState.isCandles,
      // knownPattern: newState.knownPattern !== undefined ? newState.knownPattern : prevState.knownPattern,
      isDrag: newState.isDrag !== undefined ? newState.isDrag : prevState.isDrag,
      mousePos: newState.mousePos !== undefined ? newState.mousePos : prevState.mousePos,
      mouseInitPos: newState.mouseInitPos !== undefined ? newState.mouseInitPos : prevState.mouseInitPos,
    }));

  }

  saveToLocalStorage(newBars){
    localStorage.setItem('CandlePattern', JSON.stringify(newBars));
  }

  loadFromLocalStorage(){
    if (localStorage.getItem('CandlePattern')){
      const barsObj = JSON.parse(localStorage.getItem('CandlePattern'));
      this.setNewState(
        {
          bars: new Immutable.List(barsObj),
        }
      );
    }
  }

  drawPattern(patternId){
    this.setNewState(
      {
        bars: StaticVars.CANDLES_PATTERNS[patternId].pattern,
        patternName: StaticVars.CANDLES_PATTERNS[patternId].name,
        currentBarIndex: -1,
        isDrawing: false,
        isDrag: false,
      }
    );
  }

  handleMouseLeave(mouseEvent){
    if (!this.state.isDrawing) {
      return;
    }

    this.setNewState(
      {
        isDrawing: false,
        currentBarIndex: -1,
        isHover: false,
        barEditPrice: "",
        isDrag: false,
      }
    );

  }

  handleMouseDown(mouseEvent) {
    // if (mouseEvent.button !== 0 || this.state.knownPattern !== "") {
    //   return;
    // }

    // if (this.state.isDrawing === false && this.state.currentBarIndex > -1){
    //   this.setNewState(
    //     {
    //       isDrawing: true,
    //     }
    //   );

    // }
   
  }

  getBarIndexForMousePosition(mousePosition){
    const buffer = 10;
    for (let i = 0; i < this.state.bars.size; i++){
      const barPosition = getBarPositionByIndex(i, this.state.bars.size);
      if ( mousePosition.get("x") >= barPosition.x && mousePosition.get("x") <= barPosition.x + (StaticVars.DEFAULT_BAR_WIDTH*2)
          && mousePosition.get("y") >= StaticVars.BAR_EDIT_BACKGROUND_Y && mousePosition.get("y") <= StaticVars.BAR_EDIT_BACKGROUND_Y + StaticVars.BAR_BACKGROUND_HEIGHT
      ){
        return i;        
      }
    }
    return -1;
  }

  isKnownPattern(){
    return this.state.patternName !== "" && this.state.patternName !== StaticVars.CUSTOM_PATTERN_TEXT;
  }

  handleMouseMove(mouseEvent) {
    // if (this.isKnownPattern()){
    //   return;
    // }

    const newPoint = this.relativeCoordinatesForEvent(mouseEvent);
    // handle hover
    if (!this.state.isDrawing) {
      if (this.state.bars.size > 0){
        let index = this.getBarIndexForMousePosition(newPoint);
        // console.log("hover index = " + index);
        if (index > -1){
          if (this.state.isHover === false || this.state.currentBarIndex !== index){
            this.setNewState(
              {
                isHover: true,
                currentBarIndex: index,
              }
            );
          }
        }
        else{
          if (this.state.isHover){
            this.setNewState(
              {
                isHover: false,
                currentBarIndex: -1,
                isDrawing: false,
                barEditPrice:"",
              }
            );
          }
        }
        return;
      }
      return;
    }

    if (this.state.currentBarIndex > -1 ){
      let newBarProps = Object.assign({}, this.state.bars.get(this.state.currentBarIndex));
      if (newBarProps === undefined){
        this.setNewState(
          {
            currentBarIndex: -1,
          }
        );
        return;
      }
      //handle dragging open/high/low/close
      if (this.state.barEditPrice !== ""){
        const isGreen = (newBarProps.close <= newBarProps.open ? true : false);
        const bodyhi = Math.min(newBarProps.close, newBarProps.open);
        const bodylow = Math.max(newBarProps.close, newBarProps.open);
        let isSetState = false;
        if (this.state.barEditPrice === "high"){
          isSetState = true;
          this.handleDragHigh(newPoint, newBarProps, bodyhi);
        }
        else
        if (this.state.barEditPrice === "low"){
          isSetState = true;
          this.handleDragLow(newPoint, newBarProps, bodylow);
        }
        else
        if (this.state.barEditPrice === "close"){
          isSetState = true;
          this.handleDragClose(newPoint, newBarProps, isGreen);
        }
        else
        if (this.state.barEditPrice === "open"){
          isSetState = true;
          this.handleDragOpen(newPoint, newBarProps, isGreen);
        }
        
        if (isSetState){
          this.setNewState(
            {
              patternName: StaticVars.CUSTOM_PATTERN_TEXT,
              bars: this.state.bars.updateIn([this.state.currentBarIndex], 1, barProps => barProps = newBarProps),
              
            }
          );
        }
      }
      else{
        //handle draggin bar
        if (this.state.isDrag){
            //handle max/min high/low of bar
            if (newBarProps.low + newPoint.get("y") - this.state.mouseInitPos.y > StaticVars.BAR_EDIT_MAX_LOW){
              this.setNewState(
                {
                  patternName: StaticVars.CUSTOM_PATTERN_TEXT,
                  mousePos: {x: newPoint.get("x") - (StaticVars.DEFAULT_BAR_WIDTH), y: StaticVars.BAR_EDIT_MAX_LOW - newBarProps.low },
                  mouseInitPos: {x: this.state.mousePos.x, y: newPoint.get("y") - this.state.mousePos.y},
                }
              );
            }
            else
              if (newBarProps.high + (newPoint.get("y") - this.state.mouseInitPos.y) < StaticVars.BAR_EDIT_MIN_HIGH){
                this.setNewState(
                  {
                    patternName: StaticVars.CUSTOM_PATTERN_TEXT,
                    mousePos: {x: newPoint.get("x") - (StaticVars.DEFAULT_BAR_WIDTH), y: StaticVars.BAR_EDIT_MIN_HIGH - newBarProps.high},
                    mouseInitPos: {x: this.state.mousePos.x, y: newPoint.get("y") - this.state.mousePos.y},
                  }
                );
              }
              else{
                this.setNewState(
                  {
                    patternName: StaticVars.CUSTOM_PATTERN_TEXT,
                    mousePos: {x: newPoint.get("x") - (StaticVars.DEFAULT_BAR_WIDTH), y: newPoint.get("y") - (this.state.mouseInitPos !== undefined ? this.state.mouseInitPos.y : 0)}
                  }
                );
              }

            //handle dragging before prev bar or after next bar, change bars indexes
            if (this.state.bars.size > 1){
              if (this.state.currentBarIndex > 0){
                const prevBarPos = getBarPositionByIndex(this.state.currentBarIndex - 1, this.state.bars.size);
                if (newPoint.get("x") - StaticVars.DEFAULT_BAR_WIDTH*1.5 < prevBarPos.x){
                  let tempBars = Immutable.List(this.state.bars);
                  tempBars = tempBars.updateIn([this.state.currentBarIndex], 1, barProps => barProps = this.state.bars.get(this.state.currentBarIndex - 1));
                  tempBars = tempBars.updateIn([this.state.currentBarIndex - 1], 1, barProps => barProps = this.state.bars.get(this.state.currentBarIndex));
                  
                  this.setNewState(
                    {
                      currentBarIndex: this.state.currentBarIndex - 1,
                      bars: tempBars,
                    }
                  );
                }
              }

              if (this.state.currentBarIndex < this.state.bars.size - 1){
                const nextBarPos = getBarPositionByIndex(this.state.currentBarIndex + 1, this.state.bars.size);
                if (newPoint.get("x") - StaticVars.DEFAULT_BAR_WIDTH/2 > nextBarPos.x){
                  let tempBars = Immutable.List(this.state.bars);
                  tempBars = tempBars.updateIn([this.state.currentBarIndex], 1, barProps => barProps = this.state.bars.get(this.state.currentBarIndex + 1));
                  tempBars = tempBars.updateIn([this.state.currentBarIndex + 1], 1, barProps => barProps = this.state.bars.get(this.state.currentBarIndex));
                  
                  this.setNewState(
                    {
                      currentBarIndex: this.state.currentBarIndex + 1,
                      bars: tempBars,
                    }
                  );
                }
              }

            }
        }
        else{
          /* Start Dragging, save mouseInitPos*/
          this.setNewState(
            {
              isDrag: true,
              mouseInitPos: {x: newPoint.get("x"), y: newPoint.get("y")},
              mousePos: {
                x: newPoint.get("x") - (StaticVars.DEFAULT_BAR_WIDTH), 
                y: 0
              }
            }
          );
        }
      }
    }
    
  }

  handleMouseUp(mouseEvent) {
    const newPoint = this.relativeCoordinatesForEvent(mouseEvent);
    if (this.state.isDrawing) {
      let curBar = null;
      if (this.state.isDrag){
        curBar = Object.assign({},this.state.bars.get(this.state.currentBarIndex));
        const yBuffer = (this.state.mousePos.y);
        curBar.high += yBuffer;
        curBar.low += yBuffer;
        curBar.close += yBuffer;
        curBar.open += yBuffer;
      }
      let index = this.getBarIndexForMousePosition(newPoint);
      this.setNewState(
        {
          isHover: index === this.state.currentBarIndex ? true : false,
          currentBarIndex: index === this.state.currentBarIndex ? this.state.currentBarIndex : -1,
          isDrawing: false,
          barEditPrice:"",
          isDrag: false,
          bars: curBar !== null ? this.state.bars.updateIn([this.state.currentBarIndex], 1, barProps => barProps = curBar) : this.state.bars,
        }
      );

    }
  }  

  handleDragHigh(newPoint, newBarProps, bodyhi){
    if (newPoint.get("y") < bodyhi && newPoint.get("y") > StaticVars.BAR_EDIT_MIN_HIGH){
      newBarProps[this.state.barEditPrice] = newPoint.get("y");
    }
    else
      if (newPoint.get("y") > bodyhi){
        newBarProps[this.state.barEditPrice] = bodyhi;
      }
      else{
        newBarProps[this.state.barEditPrice] = StaticVars.BAR_EDIT_MIN_HIGH;
      }

    // this.setNewState(
    //   {
    //     bars: this.state.bars.updateIn([this.state.currentBarIndex], 1, barProps => barProps = newBarProps),
    //   }
    // );

  }

  handleDragLow(newPoint, newBarProps, bodylow){
    if (newPoint.get("y") > bodylow && newPoint.get("y") < StaticVars.BAR_EDIT_MAX_LOW){
      newBarProps[this.state.barEditPrice] = newPoint.get("y");
    }
    else
      if (newPoint.get("y") < bodylow){
        newBarProps[this.state.barEditPrice] = bodylow;
      }
      else{
        newBarProps[this.state.barEditPrice] = StaticVars.BAR_EDIT_MAX_LOW;
      }

    // this.setNewState(
    //   {
    //     bars: this.state.bars.updateIn([this.state.currentBarIndex], 1, barProps => barProps = newBarProps),
    //   }
    // );

  }

  handleDragClose(newPoint, newBarProps, isGreen){
    if (isGreen){
      if (newPoint.get("y") > newBarProps.high){
        if (newPoint.get("y") < newBarProps.low){
          newBarProps[this.state.barEditPrice] = (Math.abs(newPoint.get("y") - newBarProps.open) > StaticVars.BAR_EDIT_OPEN_CLOSE_BUFFER*4 ? newPoint.get("y") : newBarProps.open - StaticVars.BAR_EDIT_OPEN_CLOSE_BUFFER);
        }
        else{
          newBarProps[this.state.barEditPrice] = Math.min(newBarProps.low-StaticVars.BAR_EDIT_OPEN_CLOSE_BUFFER, newBarProps.open-StaticVars.BAR_EDIT_OPEN_CLOSE_BUFFER);
        }
      }
      else{
        newBarProps[this.state.barEditPrice] = Math.min(newBarProps.high-StaticVars.BAR_EDIT_OPEN_CLOSE_BUFFER, newBarProps.open-StaticVars.BAR_EDIT_OPEN_CLOSE_BUFFER);
      }
    }
    else{
      if (newPoint.get("y") < newBarProps.low){
        if (newPoint.get("y") > newBarProps.high){
          newBarProps[this.state.barEditPrice] = (Math.abs(newPoint.get("y") - newBarProps.open) > StaticVars.BAR_EDIT_OPEN_CLOSE_BUFFER*4 ? newPoint.get("y") : newBarProps.open + StaticVars.BAR_EDIT_OPEN_CLOSE_BUFFER);
        }
        else{
          newBarProps[this.state.barEditPrice] = Math.max(newBarProps.high+StaticVars.BAR_EDIT_OPEN_CLOSE_BUFFER, newBarProps.open+StaticVars.BAR_EDIT_OPEN_CLOSE_BUFFER);
        }
      }
      else{
        newBarProps[this.state.barEditPrice] = Math.max(newBarProps.low+StaticVars.BAR_EDIT_OPEN_CLOSE_BUFFER, newBarProps.open+StaticVars.BAR_EDIT_OPEN_CLOSE_BUFFER);
      }
    }

    // this.setNewState(
    //   {
    //     bars: this.state.bars.updateIn([this.state.currentBarIndex], 1, barProps => barProps = newBarProps),
    //   }
    // );
  }

  handleDragOpen(newPoint, newBarProps, isGreen){
    if (isGreen){
      if (newPoint.get("y") < newBarProps.low){
        if (newPoint.get("y") > newBarProps.high){
          newBarProps[this.state.barEditPrice] = (Math.abs(newPoint.get("y") - newBarProps.close) > StaticVars.BAR_EDIT_OPEN_CLOSE_BUFFER*4 ? newPoint.get("y") : newBarProps.close + StaticVars.BAR_EDIT_OPEN_CLOSE_BUFFER);
        }
        else{
          newBarProps[this.state.barEditPrice] = Math.max(newBarProps.high+StaticVars.BAR_EDIT_OPEN_CLOSE_BUFFER, newBarProps.close+StaticVars.BAR_EDIT_OPEN_CLOSE_BUFFER);
        }
      }
      else{
        newBarProps[this.state.barEditPrice] = Math.max(newBarProps.low+StaticVars.BAR_EDIT_OPEN_CLOSE_BUFFER, newBarProps.close+StaticVars.BAR_EDIT_OPEN_CLOSE_BUFFER);
      }
    }
    else{
      if (newPoint.get("y") > newBarProps.high){
        if (newPoint.get("y") < newBarProps.low){
          newBarProps[this.state.barEditPrice] = (Math.abs(newPoint.get("y") - newBarProps.close) > StaticVars.BAR_EDIT_OPEN_CLOSE_BUFFER*4 ? newPoint.get("y") : newBarProps.close - StaticVars.BAR_EDIT_OPEN_CLOSE_BUFFER);
        }
        else{
          newBarProps[this.state.barEditPrice] = Math.min(newBarProps.low-StaticVars.BAR_EDIT_OPEN_CLOSE_BUFFER, newBarProps.close-StaticVars.BAR_EDIT_OPEN_CLOSE_BUFFER);
        }
      }
      else{
        newBarProps[this.state.barEditPrice] = Math.min(newBarProps.high-StaticVars.BAR_EDIT_OPEN_CLOSE_BUFFER, newBarProps.close-StaticVars.BAR_EDIT_OPEN_CLOSE_BUFFER);
      }
    }

    // this.setNewState(
    //   {
    //     bars: this.state.bars.updateIn([this.state.currentBarIndex], 1, barProps => barProps = newBarProps),
    //   }
    // );
  }

  barEditMouseDown(event){
    switch(event.currentTarget.id){
      case "trash":{
        this.setNewState(
          {
            bars: this.state.bars.remove(this.state.currentBarIndex),
          }
        );
        break;
      }
      case "barbody":{
        this.setNewState(
          {
            isDrawing: true,
          }
        );
        break;
      }
      default:{
        this.setNewState(
          {
            barEditPrice: event.currentTarget.id,
            isDrawing: true,
          }
        );
      }
    }
  }

  addDefaultCandle(event){
    let addBar = null;

    if (this.state.bars.size === StaticVars.MAX_ALLOWED_CANDLES || this.isKnownPattern()){
      return;
    }
    switch (event.currentTarget.id){
      case "DefaultRedBar":{
        addBar = {high: (StaticVars.CANVAS_SIZE.height/2) - 50, low: (StaticVars.CANVAS_SIZE.height/2) + 50, open: (StaticVars.CANVAS_SIZE.height/2) - 25, close: (StaticVars.CANVAS_SIZE.height/2) + 25};
        break;
      }
      case "DefaultGreenBar":{
        addBar = {high: (StaticVars.CANVAS_SIZE.height/2) - 50, low: (StaticVars.CANVAS_SIZE.height/2) + 50, open: (StaticVars.CANVAS_SIZE.height/2) + 25, close: (StaticVars.CANVAS_SIZE.height/2) - 25};
        break;
      }
      case "DefaultGreyBar":{
        addBar = {high: (StaticVars.CANVAS_SIZE.height/2) - 50, low: (StaticVars.CANVAS_SIZE.height/2) + 50, open: (StaticVars.CANVAS_SIZE.height/2) + 25, close: (StaticVars.CANVAS_SIZE.height/2) - 25, isGrey:true};
        break;
      }
      default:{

      }
    }

    this.setNewState(
      {
        bars: this.state.bars.push(addBar),
        patternName: StaticVars.CUSTOM_PATTERN_TEXT,
      }
    );
  }

  render() {
    const trashYoffset = StaticVars.BAR_EDIT_BACKGROUND_Y + StaticVars.BAR_BACKGROUND_HEIGHT - 40;
    const trashXoffset = StaticVars.DEFAULT_BAR_WIDTH- 9;
    return (
      <div  className="CandlestickCanvas_myCanvas" >
          
        <div
            className={"candlestickdrawArea" + (this.state.isDrawing ? "" : "")}//selectItem {StaticVars.DRAW_ANYTHING_MESSAGE}
            ref="drawArea"
            onMouseDown={this.handleMouseDown}
            onMouseMove={this.handleMouseMove}
            onMouseLeave={this.handleMouseLeave}
        >
            
            {this.state.bars.map((bar, index) => (
              this.state.currentBarIndex === index ?
              //Edit Bar
              <React.Fragment key={index} >
                <EditBarBackground 
                  barEditEvents={{ onMouseDown: this.barEditMouseDown}}
                  isEdit={true}
                  position={getBarPositionByIndex(index, this.state.bars.size)}
                />
                <EditBarTrash 
                  scale={0.7} 
                  position={{x:getBarPositionByIndex(index, this.state.bars.size).x + trashXoffset, y: getBarPositionByIndex(index, this.state.bars.size).y + trashYoffset}}
                  onMouseDown={this.barEditMouseDown}
                />
                <DrawBar
                    // barEvents={{onMouseOver:this.barMouseOver , onMouseOut: this.barMouseOut, onMouseDown: this.barMouseDown}}//, onMouseLeave:this.barMouseOut
                    barEditEvents={{ onMouseDown: this.barEditMouseDown}}//onMouseDown: this.barEditMouseDown,
                    isEdit={!this.state.isDrag}
                    // key={index} 
                    bar={bar} 
                    index={index} 
                    totalBars={this.state.bars.size}
                    pos={this.state.isDrag ? {x:this.state.mousePos.x , y:this.state.mousePos.y} : getBarPositionByIndex(index, this.state.bars.size)}
                />
              </React.Fragment>
                :
                //regular bar
              <React.Fragment key={index} >
                <EditBarBackground 
                  barEditEvents={{ onMouseDown: this.barEditMouseDown}}
                  isEdit={false}
                  position={getBarPositionByIndex(index, this.state.bars.size)}
                />
                <DrawBar
                    // barEvents={{onMouseOver:this.barMouseOver}}//, onMouseLeave:this.barMouseOut
                    isEdit={false}
                    // key={index} 
                    bar={bar} 
                    index={index} 
                    totalBars={this.state.bars.size} 
                    pos={getBarPositionByIndex(index, this.state.bars.size)}
                />
              </React.Fragment>
            ))}
            
          <Drawing currentBarIndex={this.state.currentBarIndex}/>
        </div>
        
      </div>
      
    );
  }//end render

  relativeCoordinatesForEvent(mouseEvent) {
    const boundingRect = this.refs.drawArea.getBoundingClientRect();
    return new Immutable.Map({
      x: mouseEvent.clientX - boundingRect.left,
      y: mouseEvent.clientY - boundingRect.top,
    });
  }

  getPatternName(){
    return this.isKnownPattern() ? this.state.patternName : StaticVars.CUSTOM_PATTERN_TEXT;
    // return "";
  }

  getPattern() {
    let retArr = [];
    for (let i = 0; i < this.state.bars.size; i++){
      retArr.push({
                    open: StaticVars.CANVAS_SIZE.height - this.state.bars.get(i).open, 
                    close: StaticVars.CANVAS_SIZE.height - this.state.bars.get(i).close,
                    high: StaticVars.CANVAS_SIZE.height - this.state.bars.get(i).high,
                    low: StaticVars.CANVAS_SIZE.height - this.state.bars.get(i).low,
                    isGrey: this.state.bars.get(i).isGrey,
                  });
    }
    return retArr.reverse();
  }

  clearCanvas() {
    this.setNewState({
        bars: new Immutable.List(),
        patternName: StaticVars.CUSTOM_PATTERN_TEXT,
        isDrawing : false,
        isDrag: false,
        currentBarIndex: -1,
        
        barEditPrice: "",
      });
  }

}//end class

function getBarPositionByIndex(index, totalBars){
  const patternStartX = StaticVars.CANVAS_SIZE.width / 2 - (totalBars * (BAR_WIDTH / 2));
  return {x: patternStartX + (index * BAR_WIDTH * 1.1), y: 0};//CANVAS_SIZE.height / 2 - 50
}

function DrawBar({bar, index, totalBars, isEdit, barEvents, barEditEvents, pos}){
  return <CustomBarIcon 
            isEdit={isEdit} 
            id={index} 
            barEvents={barEvents} 
            barEditEvents={barEditEvents} 
            candleInfo={bar} 
            stroke={bar.stroke} 
            fill={bar.fill} 
            strokeWidth={3} marginLeft={pos.x} marginTop={pos.y} scale={1}  
          />
}


function Drawing({ currentBarIndex }) {
  return (
    <svg className="CandlestickCanvas_drawing">
      <filter id="dropshadow" x="-40%" y="-40%" width="180%" height="180%" filterUnits="userSpaceOnUse">
        <feGaussianBlur in="SourceAlpha" stdDeviation="1"/>
        <feOffset dx="2" dy="2" result="offsetblur"/> 
        <feOffset dx="-2" dy="-2" result="offsetblur"/>
        <feMerge> 
          <feMergeNode/>
          <feMergeNode in="SourceGraphic"/>
          <feMergeNode in="SourceGraphic"/>
        </feMerge>
    </filter>

    {/* {[...Array(GRIDLINES.vertical)].map((x, i) =>
      <GridLineVertical key={i} lineNumber={i}/>
    )} */}
    {[...Array(GRIDLINES.horizontal)].map((x, i) =>
      <GridLineHorizontal key={i} lineNumber={i}/>
    )}
    </svg>
  );
}

function GridLineHorizontal({lineNumber}){
  return(
    <line className="CandlestickCanvas_gridLine" x1={0} y1={(lineNumber + 1) * StaticVars.CANVAS_SIZE.height / GRIDLINES.horizontal} x2={StaticVars.CANVAS_SIZE.width} y2={(lineNumber + 1) * StaticVars.CANVAS_SIZE.height / GRIDLINES.horizontal}/>
  );
}

function GridLineVertical({lineNumber}){
  return(
    <line className="CandlestickCanvas_gridLine" x1={(lineNumber + 1) * StaticVars.CANVAS_SIZE.width / GRIDLINES.vertical} y1={0} x2={(lineNumber + 1) * StaticVars.CANVAS_SIZE.width / GRIDLINES.vertical} y2={StaticVars.CANVAS_SIZE.height}/>
  );
}


/*

{ <use href="#redCircle"/> }
*/