import React from 'react';
import './GraphEngineComponent.css';
import GraphUIComponent from './GraphUIComponent';
import SSE from "../../lib/sse";
// import TutorialAnimation from '../TutorialAnimation';


const delay = ms => new Promise(res => setTimeout(res, ms));

export default class GraphEngineComponent extends React.Component {
    sliderValues_minMatchPercent = [80];  //min percent to show results
    sliderValues_startFrom = [0]; //min match to calculate x ratio
    sliderValues_xInfluence = [50]; //x ratio weight
    sliderValues_minBarRange = null;  //min bars
    isSearchAllowed = true;
    allowSearchTimeoutId = null;
    timeFrameIndex = -1;

    constructor() {
      super();
      this.state = {
        isSearchEnd: false,
        height: window.innerHeight / 2,
        isSearchActive : false,
      };
      this.searchButtonCallback = this.searchButtonCallback.bind(this);
      this.onShowResult = this.onShowResult.bind(this);
      this.clearResults = this.clearResults.bind(this);
      this.calcPatterns = this.calcPatterns.bind(this);
      this.clearCanvas = this.clearCanvas.bind(this);
      this.undoLastPoint = this.undoLastPoint.bind(this);
      this.onCanvasClicked = this.onCanvasClicked.bind(this);
      this.updateNumOfLines = this.updateNumOfLines.bind(this);
      this.startAllowTimer = this.startAllowTimer.bind(this);
      this.allowSearch = this.allowSearch.bind(this);
      this.endSearch = this.endSearch.bind(this);
      this.loadIdFromLocalStorage = this.loadIdFromLocalStorage.bind(this);
      this.saveIdToLocalStorage = this.saveIdToLocalStorage.bind(this);

      
    }

    componentDidMount(){
      this.loadIdFromLocalStorage();
    }

    saveIdToLocalStorage(clientID){
      localStorage.setItem('clientID', clientID);
    }
  
    loadIdFromLocalStorage(){
      const cID = Number(localStorage.getItem('clientID'));
      if (cID){
        this.setState({
          clientID: cID,
        });
      }
    }

    async searchButtonCallback(timeFrameIndex){
      if (!this.isSearchAllowed){
        return;
      }

      this.timeFrameIndex = timeFrameIndex;

      const searchLines = this.refs.searchUIComponent.getPattern();
      if (searchLines.length < 2){
        return false;
      }

      this.isSearchAllowed = false;
      this.startAllowTimer();

      this.clearResults(this.calcPatterns);
      this.setSearchActive(true);
      await delay(100);
      // window.scrollTo({ behavior: 'smooth', top: 120});

      return true;
    }
    
    // updateSliderValues_startFrom(val){
    //   this.sliderValues_startFrom = val;
    // }
  
    // updateSliderValues_xInfluence(val){
    //   this.sliderValues_xInfluence = val;
    // }

    startAllowTimer(){
      this.allowSearchTimeoutId = setTimeout(this.allowSearch, 3000);
    }

    allowSearch(){
      this.isSearchAllowed = true;
    }

    calcPatterns(){
      const pattern = this.refs.searchUIComponent.getPattern(); 
      const patternName = this.refs.searchUIComponent.getPatternName(); 
      this.props.updatePattern(pattern, patternName);
      const isRightDirection = this.refs.searchUIComponent.getDrawRightDirection();
      let data = {
        pattern : pattern,
        patternName: patternName,
        isRightDirection : isRightDirection, 
        timeFrameIndex: this.timeFrameIndex,
        clientID : this.state.clientID,
      }

      this.setState({
        pattern : pattern,
        isRightDirection : isRightDirection,
        finishedSearch : false,
        endSearch: false,
        isSearchActive : true,
      });

      const localThis = this;
      console.log("start search");
      try{
        const source = SSE.SSE(process.env.NODE_ENV === 'production' ? "/api/graph-search" : "http://localhost:8080/api/graph-search", {headers: {'Content-Type': 'application/json'}, payload: JSON.stringify(data)});
        source.onmessage=function(event)
        {
          //console.log("response = " + event.data);
          const resData = JSON.parse(event.data);
          if (resData.finishedSearch === true){
            console.log("finished search");
            // localThis.props.onShowResultsMessage("finished search");
            localThis.setCanUpdateScroll(true);
          }
          
          if (resData.endSearch === true){
            console.log("end search");
            // localThis.props.onShowResultsMessage("search has ended.");
            localThis.setCanUpdateScroll(false);
            localThis.endSearch();
            localThis.setState({
              endSearch: resData.endSearch,
            });
          }
            
          if (resData.clientID !== undefined){
            localThis.saveIdToLocalStorage(resData.clientID);
            localThis.setState({
              clientID: resData.clientID,
            });
          }
              
          if (resData.progress !== undefined){
            if (localThis.refs.searchUIComponent !== undefined){
              localThis.refs.searchUIComponent.showProgress(resData.progress);
            }
          }
                
          if (resData.results !== undefined){
            //localThis.clientID = resData.clientID;
            localThis.onShowResult(resData);
          }
        };

        source.stream();
      }
      catch(e){
        console.log(e);
        localThis.endSearch();
      }

    }//end calcPatterns

    setCanUpdateScroll(val){
      
    }

    onShowResult(resData){
      //console.log(JSON.stringify(resData));
      // window.scrollTo({ behavior: 'smooth', top: 470 });
      // window.scrollTo(0, 400);
      if (this.refs.searchUIComponent !== undefined){
        this.refs.searchUIComponent.foundResult();
      }
      this.props.onShowResult(resData);
    }

    clearResults(callbackFunc){
      this.props.clearResults(callbackFunc);
      if (this.refs.searchUIComponent !== undefined){
        this.refs.searchUIComponent.clearSearch();
      }
    }

    undoLastPoint(){
      
    }

    clearCanvas(){
      
    }

    setSearchActive(isActive){
      this.setState({
        isSearchActive: isActive,
      })
      if (this.refs.searchUIComponent !== undefined){
        this.refs.searchUIComponent.setSearchActive(isActive);
      }
      this.props.setActiveSearch(isActive);
      
    }

    onCanvasClicked(){
      
    }

    updateNumOfLines(numOfLines){
      if (this.refs.searchUIComponent !== undefined){
        this.refs.searchUIComponent.updateNumOfLines(numOfLines);
      }
    }

    endSearch(){
      this.setState({
        isSearchEnd: true,
      })
      this.props.endSearch();
      if (this.refs.searchUIComponent !== undefined){
        this.refs.searchUIComponent.endSearch();
      }
    }

    render() {
      return (
        <div className={'SearchEngineDiv' }>
              <GraphUIComponent ref="searchUIComponent" searchButtonCallback = {this.searchButtonCallback} clearResults = {this.clearResults}
                undoLastPoint = {this.undoLastPoint} clearCanvas = {this.clearCanvas} 
              />
              {/* <TutorialAnimation/> */}
        </div>
      )
       
    }
}//end class
