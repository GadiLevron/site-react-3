
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import Zoom from '@material-ui/core/Zoom';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import './GraphFixedPatternMenu.css';

const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
    // borderRadius:'20px',
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    {...props}
  />
));

const StyledMenuItem = withStyles((theme) => ({
  root: {
    '&:focus': {
      backgroundColor: theme.palette.primary.main,
      '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
        color: theme.palette.common.white,
      },
    },
  },
}))(MenuItem);

export default function GraphBearishPatternMenu({onItemClicked}) {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleItemClicked = (event) =>{
    //event.currentTarget.id;
    onItemClicked(event.currentTarget.id);
    handleClose();
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const UITooltip = withStyles((theme) => ({
    tooltip: {
      backgroundColor: '#f5f5f9',
      color: 'rgba(0, 0, 0, 0.87)',
      maxWidth: 220,
      fontSize: theme.typography.pxToRem(17),
      border: '1px solid #dadde9',
    },
  }))(Tooltip);

  return (
    <div>
      
        <button
          className='GraphCanvasBearishPatternsButton'
          aria-controls="customized-menu"
          aria-haspopup="true"
          variant="contained"
          onClick={handleClick}
        >
          <UITooltip arrow TransitionComponent={Zoom} title="Bearish Patterns">
            <div className='GraphCanvasDropdownImageDiv'>
              <img alt='bearishsvg' className='GraphCanvasBearishDropdownImageDiv_img'></img>
            </div>
          </UITooltip>
        </button>
      
      <StyledMenu
        id="customized-menu"
        anchorEl={anchorEl}
        keepMounted
        disableAutoFocusItem
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <StyledMenuItem id='HeadShouldersTop' onClick={handleItemClicked}>
          <ListItemIcon>
            <img className="GraphFixedPatternsImage" src={require("../../assets/headshoulders.svg")} alt="hnst"/>
            {/* <SendIcon fontSize="small" /> */}
          </ListItemIcon>
          <ListItemText primary="Head & Shoulders Top" />
        </StyledMenuItem>
        <StyledMenuItem id='DoubleTop' onClick={handleItemClicked}>
          <ListItemIcon>
            <img className="GraphFixedPatternsImage" src={require("../../assets/doubletop.svg")} alt="hnsb"/>
            {/* <InboxIcon fontSize="small" /> */}
          </ListItemIcon>
          <ListItemText primary="Double Top" />
        </StyledMenuItem>
        <StyledMenuItem id='WedgeUp' onClick={handleItemClicked}>
          <ListItemIcon>
             <img className="GraphFixedPatternsImage" src={require("../../assets/wedgeup.svg")} alt="hnsb"/>
            {/* <InboxIcon fontSize="small" /> */}
          </ListItemIcon>
          <ListItemText primary="Wedge Up" />
        </StyledMenuItem>
        <StyledMenuItem id='CupHandleBottom' onClick={handleItemClicked}>
          <ListItemIcon>
             <img className="GraphFixedPatternsImage" src={require("../../assets/cuphandlebottom.svg")} alt="hnsb"/>
            {/* <InboxIcon fontSize="small" /> */}
          </ListItemIcon>
          <ListItemText primary="Cup And Handle Bottom" />
        </StyledMenuItem>
      </StyledMenu>
      
    </div>
  );
}
