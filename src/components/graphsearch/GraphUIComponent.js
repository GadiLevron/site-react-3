import React from 'react';
import './GraphUIComponent.css';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import Zoom from '@material-ui/core/Zoom';
import { withStyles } from '@material-ui/core/styles';
// import {Spring} from 'react-spring/renderprops'
import GraphCanvas from './GraphCanvas';
// import MinBarSlider from '../ui/MinBarSlider';
import TimeframeMenu from '../ui/TimeframeMenu';
import BullishPatternMenu from './GraphBullishPatternMenu';
import BearishPatternMenu from './GraphBearishPatternMenu';
import IndexesMenu from '../ui/IndexesMenu';
import BottomUIComponent from '../ui/BottomUiComponent';
import StaticVars from '../../StaticVars';


export default class GraphUIComponent extends React.Component {
  sliderValues_minMatchPercent = null;
  sliderValues_minBarRange = null;
  myCanvasRef = null;
  bottomUIRef = null;
  minBarSliderRef = null;

  constructor(){
    super();
    this.sliderValues_minMatchPercent = [80, 100];
    this.state = {
      patternName: StaticVars.CUSTOM_PATTERN_TEXT,
      timeFrameIndex: StaticVars.TIMEFRAME_MENU_DEFAULT_INDEX,
      isSearchActive: false,
      numOfPoints: 0,
      isFoundResult: false,
      isSearchEnd: false,
    };

    this.myCanvasRef = React.createRef();
    this.bottomUIRef = React.createRef();
    this.minBarSliderRef = React.createRef();

    this.searchButtonClicked = this.searchButtonClicked.bind(this);
    this.searchOptionsButtonClicked = this.searchOptionsButtonClicked.bind(this);
    this.setSearchActive = this.setSearchActive.bind(this);
    // this.getMinBarRange = this.getMinBarRange.bind(this);
    // this.updateBarRangeValues = this.updateBarRangeValues.bind(this);
    this.updateNumOfPoints = this.updateNumOfPoints.bind(this);
    this.getPattern = this.getPattern.bind(this);
    this.getPatternName = this.getPatternName.bind(this);
    this.getDrawRightDirection = this.getDrawRightDirection.bind(this);
    this.onCanvasClicked = this.onCanvasClicked.bind(this);
    this.onFixedPatternsChange = this.onFixedPatternsChange.bind(this);
    this.foundResult = this.foundResult.bind(this);
    this.clearCanvas = this.clearCanvas.bind(this);
    this.loadFromLocalStorage = this.loadFromLocalStorage.bind(this);
    this.updatePatternName = this.updatePatternName.bind(this);
    this.onTimeFrameSelected = this.onTimeFrameSelected.bind(this);
    
  }

  searchButtonClicked(){
    this.props.searchButtonCallback(this.state.timeFrameIndex); 
    // if (this.props.searchButtonCallback(this.sliderValues) === true){
    //   this.setState({
    //     minimizeSearch : true,
    //   });
    // }
  }

  clearSearch(){ 
    // this.props.clearResults();
    this.setState({
      isSearchActive: false,
      isFoundResult: false,
      isSearchEnd: false,
    });
  }

  getPattern(){
    return this.myCanvasRef.current.getPattern();
  }

  getPatternName(){
    return this.myCanvasRef.current.getPatternName();
  }

  getDrawRightDirection(){
    return this.myCanvasRef.current.getDrawRightDirection();
  }

  undoLastPoint(){
    this.props.undoLastPoint();
    return this.myCanvasRef.current.undoLastPoint();
  }

  clearCanvas(){
    this.myCanvasRef.current.clearCanvas();
  }

  loadFromLocalStorage(){
    this.myCanvasRef.current.loadFromLocalStorage();
  }

  searchOptionsButtonClicked(){

  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.state.patternName !== nextState.patternName || this.state.numOfPoints !== nextState.numOfPoints ;    
  }

  updatePatternName(patternName){
    this.setState({
      patternName:patternName,
    });
  }

  drawPattern(patternId){
    this.myCanvasRef.current.drawPattern(patternId);
  }

  setSearchActive(isActive){
    this.myCanvasRef.current.setSearchActive(isActive);
    this.bottomUIRef.current.setSearchActive(isActive);
    this.setState(prevstate =>({
      isSearchActive : isActive,
      isSearchEnd: false,
    }));
  }

  onCanvasClicked(){
  }


  updateNumOfPoints(numOfPoints){
    this.setState({
      numOfPoints: numOfPoints,
    });
  }

  onFixedPatternsChange(patternId){
    this.drawPattern(patternId);
  }

  endSearch(){
    this.bottomUIRef.current.endSearch();
    this.setState({
      isSearchEnd: true,
    });
  }

  foundResult(){
    this.bottomUIRef.current.foundResult();
    this.setState({
      isFoundResult: true,
    });
    // window.scrollTo({ behavior: 'smooth', top: 1470 });
  }

  onTimeFrameSelected(timeFrameIndex){
    this.setState({
      timeFrameIndex: timeFrameIndex,
    });
  }

  showProgress(progress){
    this.bottomUIRef.current.showProgress(progress);
  }

  render() {
    // console.log("progress = " + this.state.progress);
      return (
        <div className="GraphSearchUIdiv" >
          <GraphCanvas ref={this.myCanvasRef} updateNumOfPoints={this.updateNumOfPoints} updatePatternName={this.updatePatternName} />
          <UITooltip arrow TransitionComponent={Zoom} title="Clear Canvas">
            <button className="GraphCanvasClearButton" onClick={() => this.clearCanvas()}>              
                <img alt='' data-tip="Clear Canvas"></img>
            </button>
          </UITooltip>
          <UITooltip arrow TransitionComponent={Zoom} title="Recover Last Pattern">
            <button className="GraphCanvasRecoverButton" onClick={() => this.loadFromLocalStorage()}>
              <img alt='' data-tip="Draw Last Pattern"></img>
            </button>
          </UITooltip>
          <div className='GraphCanvas_PatternText'>{this.state.patternName}</div>
          <div className='GraphCanvas_DrawMessage'>{StaticVars.DRAW_ANYTHING_MESSAGE}</div>
          
          <BullishPatternMenu onItemClicked={this.onFixedPatternsChange}/>
          <BearishPatternMenu onItemClicked={this.onFixedPatternsChange}/>
          
          <IndexesMenu style={{left:'460px', top:'477px'}}/>
          <TimeframeMenu style={{position:'absolute', left:'680px', top:'478px'}} onItemClicked={this.onTimeFrameSelected}/>
          {/* <div className='MinBarSliderDiv'>
            <MinBarSlider 
              onChangeFunc={value => this.updateBarRangeValues(value)}
              min={this.state.numOfPoints}
              max='40'
              message={StaticVars.MIN_BARS_SLIDER_MESSAGE}
              textSuffix=""
            />
          </div> */}
          <Button className="GraphSearchButton" onClick={() => this.searchButtonClicked()}>
            {StaticVars.SEARCH_BUTTON_TEXT}
          </Button>
          <BottomUIComponent ref={this.bottomUIRef}/>
        </div>
      )
  }
}//end class


const UITooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: '#f5f5f9',
    color: 'rgba(0, 0, 0, 0.87)',
    maxWidth: 220,
    fontSize: theme.typography.pxToRem(17),
    border: '1px solid #dadde9',
  },
}))(Tooltip);