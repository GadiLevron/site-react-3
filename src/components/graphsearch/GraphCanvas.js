import React from 'react';
//import ReactDOM from 'react-dom';
import Immutable from 'immutable';
import './GraphCanvas.css';
import StaticVars from '../../StaticVars';
import {GraphSimplePoint, GraphSelectedPoint} from '../ui/SvgIcons';
// import AnimateHeight from 'react-animate-height';
// import { FilledInput } from '@material-ui/core';


const GRIDLINES = {horizontal:18, vertical: 36};
const MAX_ALLOWED_POINTS = 12;

export default class GraphCanvas extends React.Component {
  constructor() {
    super();

    this.state = {
      points: new Immutable.List(),
      patternName:StaticVars.CUSTOM_PATTERN_TEXT,
      isDrawing: false,
      isRightDirection : null,
      isSearchActive:false,
      currentPointIndex: -1,
      savedOriginalPoint: null,
      pointJustCreated: false,
      isHoverOnPoint : false,
      isHoverNearPoint: false,
    };

    this.handleMouseDown = this.handleMouseDown.bind(this);
    this.handleMouseMove = this.handleMouseMove.bind(this);
    this.handleMouseLeave = this.handleMouseLeave.bind(this);
    this.handleMouseUp = this.handleMouseUp.bind(this);
    this.isPointGoodDirection = this.isPointGoodDirection.bind(this);
    this.onDeletePoint = this.onDeletePoint.bind(this);
    this.setNewState = this.setNewState.bind(this);
    this.loadFromLocalStorage = this.loadFromLocalStorage.bind(this);
  }

  componentDidMount() {
    document.addEventListener("mouseup", this.handleMouseUp);
    // this.loadFromLocalStorage();
  }

  componentWillUnmount() {
    document.removeEventListener("mouseup", this.handleMouseUp);
  }

  setNewState(newState, callback){
    this.setState(prevState => ({
      points: newState.points !== undefined ? newState.points : prevState.points,
      patternName: newState.patternName !== undefined ? newState.patternName : prevState.patternName,
      isDrawing: newState.isDrawing !== undefined ? newState.isDrawing : prevState.isDrawing,
      isRightDirection: newState.isRightDirection !== undefined ? newState.isRightDirection : prevState.isRightDirection,
      isSearchActive: newState.isSearchActive !== undefined ? newState.isSearchActive : prevState.isSearchActive,
      currentPointIndex: newState.currentPointIndex !== undefined ? newState.currentPointIndex : prevState.currentPointIndex,
      savedOriginalPoint: newState.savedOriginalPoint !== undefined ? newState.savedOriginalPoint : prevState.savedOriginalPoint,
      pointJustCreated: newState.pointJustCreated !== undefined ? newState.pointJustCreated : prevState.pointJustCreated,
      isHoverOnPoint: newState.isHoverOnPoint !== undefined ? newState.isHoverOnPoint : prevState.isHoverOnPoint,
      isHoverNearPoint: newState.isHoverNearPoint !== undefined ? newState.isHoverNearPoint : prevState.isHoverNearPoint,
    }), callback !== undefined ? callback : null);

    if (newState.patternName !== undefined){
      this.props.updatePatternName(newState.patternName);
    }

    if (newState.points !== undefined ){
      if (newState.points.size > 0){
        this.saveToLocalStorage(newState.points);
      }
    }
  }

  saveToLocalStorage(newPoints){
    localStorage.setItem('GraphPattern', JSON.stringify(newPoints));
  }

  loadFromLocalStorage(){
    if (localStorage.getItem('GraphPattern')){
      const pointsObj = JSON.parse(localStorage.getItem('GraphPattern'));
      for (let i = 0; i  < pointsObj.length; i++){
        pointsObj[i] = new Immutable.Map(pointsObj[i]);
      }
      this.setNewState(
        {
          points: new Immutable.List(pointsObj),
          isRightDirection: true,
        }
      );
    }
  }

  drawPattern(patternId){
    this.setNewState(
      {
        points: StaticVars.GRAPHS_PATTERNS[patternId].pattern,
        patternName:StaticVars.GRAPHS_PATTERNS[patternId].name,
        currentPointIndex: -1,
        pointJustCreated: false,
        savedOriginalPoint: null,
        isRightDirection: true,
        isDrawing: false,
      }, this.props.updateNumOfPoints(StaticVars.GRAPHS_PATTERNS[patternId].pattern.size)
    );
  }

  isPointVeryClose(newPoint, lastPoint){
    // for convenience, the user can be within 10 px from the point
    return (newPoint !== null && lastPoint !== null && Math.abs((lastPoint.get('x') - newPoint.get('x'))) <= 15 && Math.abs((lastPoint.get('y') - newPoint.get('y'))) <= 15);
  }

  isPointInBound(newPoint){
    const boundingRect = this.refs.drawArea.getBoundingClientRect();
    return (newPoint.get('x') >= 5 && newPoint.get('x') <= boundingRect.width - 10 && newPoint.get('y') >= 5 && newPoint.get('y') <= boundingRect.height - 10)
  }

  isPointGoodDirection(point, prevPoint){
    if (point !== null && prevPoint !== null && this.state.isRightDirection !== null //check if line is illegal
      && (
      (this.state.isRightDirection && point.get('x') < prevPoint.get('x')) 
      || (!this.state.isRightDirection && point.get('x') > prevPoint.get('x'))
      ))
    {
      return false;
    }

    return true;
  }

  handleMouseLeave(mouseEvent){
    if (!this.state.isDrawing) {
      return;
    }

    if (this.state.pointJustCreated === true){
      this.setNewState(
        {
          points: this.state.points.pop(),
          isDrawing: false,
          currentPointIndex: -1,
          pointJustCreated: false,
        }, this.props.updateNumOfPoints(this.state.points.size - 1)
      );
    }
    else{
      this.setNewState(
        {
          points: this.isPointGoodDirection(this.state.currentPointIndex > 0 ? this.state.savedOriginalPoint : this.state.points.get(this.state.currentPointIndex + 1), this.state.currentPointIndex > 0 ? this.state.points.get(this.state.currentPointIndex - 1) : this.state.savedOriginalPoint) ?
          this.state.points.updateIn([this.state.currentPointIndex], 1, point => point = this.state.savedOriginalPoint)
                  :
                  this.state.points.splice(this.state.currentPointIndex, 1)
                  ,
          isDrawing: false,
          currentPointIndex: -1,
          pointJustCreated: false,
        }, this.props.updateNumOfPoints(this.state.points.size)
      );
    }
  }

  hoverOnPoint(newPoint){
    const buffer = 11;
    for (let i = 0; i < this.state.points.size; i++){
      const curPoint = this.state.points.get(i);
      if ( Math.abs(curPoint.get('x') - newPoint.get('x')) <= buffer && Math.abs(curPoint.get('y') - newPoint.get('y')) <= buffer ){
        return i;        
      }
    }
    return -1;
    // return this.hoverNearPoint(newPoint);
  }

  hoverNearPoint(newPoint){
    const hoverArea = {width: 62, height: 102};

    // const buffer = this.state.isHoverNearPoint? 30 : 15;
    for (let i = 0; i < this.state.points.size; i++){
      const curPoint = this.state.points.get(i);
      if ( Math.abs(curPoint.get('x') - newPoint.get('x')) <= hoverArea.width/2 
            && ((newPoint.get('y') - curPoint.get('y') <= hoverArea.height/3 && newPoint.get('y') - curPoint.get('y') >= 0)
                || (curPoint.get('y') - newPoint.get('y') <= 2*hoverArea.height/3 && curPoint.get('y') - newPoint.get('y') >= 0))
      ){
        return i;        
      }
    }
    return -1;
  }

  handleMouseDown(mouseEvent) {
    if (mouseEvent.button !== 0) {
      return;
    }
    const newPoint = this.relativeCoordinatesForEvent(mouseEvent);

    let index = this.hoverOnPoint(newPoint);
    if (this.state.isDrawing === true){
      this.handleMouseLeave(null);
    }
    else{
      if (index > -1){
        this.setNewState(
          {
            isDrawing: true,
            currentPointIndex: index,
            savedOriginalPoint: newPoint,
          }
        );
      }
      else{
        //adds new point
        let indexNear= this.hoverNearPoint(newPoint);
        if (indexNear === -1){
          this.setNewState(
            {
              points: this.state.points.push(newPoint),
              savedOriginalPoint: newPoint,
              isDrawing: true,
              pointJustCreated: true,
              currentPointIndex: this.state.points.size,
            }, this.props.updateNumOfPoints(this.state.points.size + 1)
          );
        }
      }
    }
  }

  handleMouseMove(mouseEvent) {
    const newPoint = this.relativeCoordinatesForEvent(mouseEvent);
    
    if (!this.state.isDrawing) {
      // let index = this.hoverOnPoint(newPoint);
      let indexNear = this.hoverNearPoint(newPoint);
      if (indexNear > -1){
        if (this.state.isHoverNearPoint === false){
          this.setNewState(
            {
              isHoverNearPoint: true,
              currentPointIndex: indexNear,
            }
          );
          return;
        }
      }
      else
        if (this.state.isHoverNearPoint === true){
          this.setNewState(
            {
              isHoverNearPoint: false,
              currentPointIndex: -1,
            }
          );
          return;
        }
      
      let indexOn = this.hoverOnPoint(newPoint);
      if (indexOn > -1 ){
        if (this.state.isHoverOnPoint === false){
          this.setNewState(
            {
              isHoverOnPoint: true,
              currentPointIndex: indexOn,
            }
          );
          return;
        }
      }
      else
        if (this.state.isHoverOnPoint === true){
          this.setNewState(
            {
              isHoverOnPoint: false,
              currentPointIndex: this.state.isHoverNearPoint ? this.state.currentPointIndex : -1,
            }
          );
          return;
        }

      return;
    }

    if (!this.isPointInBound(newPoint)){
      this.handleMouseLeave(null);
      return;
    }

    if (this.state.points.size === 1){ //should be === 1 is first point cant be moved
      this.setNewState(
        {
          points: this.state.points.push(newPoint),
          currentPointIndex: this.state.points.size,
        }
      );
    }
    else{
      if (this.state.points.size > 0 && (newPoint.get("x") !== this.state.points.get(this.state.currentPointIndex).get("x") //should be points.size > 1 
      || newPoint.get("y") !== this.state.points.get(this.state.currentPointIndex).get("y"))){
        this.setNewState(
          {
            points: this.state.points.updateIn([this.state.currentPointIndex], 1, point => point = newPoint),
            // patternName: StaticVars.CUSTOM_PATTERN_TEXT,
          }
        );

      }
    }

    this.props.updateNumOfPoints(this.state.points.size);
  }

  handleMouseUp(mouseEvent) {
    if (!this.state.isDrawing) {
      return;
    }

    // const lastPoint = this.state.points.get(this.state.points.size - 2);
    const prevPoint = this.state.currentPointIndex > 0 ? this.state.points.get(this.state.currentPointIndex - 1) : null;
    const nextPoint = this.state.currentPointIndex > -1 && this.state.currentPointIndex < this.state.points.size - 1 ? this.state.points.get(this.state.currentPointIndex + 1) : null;
    const newPoint = this.relativeCoordinatesForEvent(mouseEvent);
    

    if (this.state.points.size > 1){ 
      if (this.isPointGoodDirection(newPoint, prevPoint) === false || this.isPointGoodDirection(nextPoint, newPoint) === false){
        this.handleMouseLeave(null);
        return;
      }

      if (this.isPointVeryClose(newPoint, prevPoint) || this.isPointVeryClose(nextPoint, newPoint)){
        this.handleMouseLeave(null);
        return;
      }
    }

    if (this.state.points.size > MAX_ALLOWED_POINTS){
      this.handleMouseLeave(null);
      return;
    }

    if (this.isPointInBound(newPoint)){
      if (this.state.pointJustCreated === true){
        this.setNewState(
          {
            // currentPointIndex: -1,
            patternName: StaticVars.CUSTOM_PATTERN_TEXT,
            isHoverOnPoint: true,
            isHoverNearPoint: true,
            isDrawing: false,
            pointJustCreated: false,
            isRightDirection : (this.state.isRightDirection === null && this.state.points.size > 1 ? (prevPoint.get('x') < newPoint.get('x') ? true : false) : this.state.isRightDirection),
          }
        );
      }
      else{
        this.setNewState(
          {
            patternName: StaticVars.CUSTOM_PATTERN_TEXT,
            isDrawing: false,
            isRightDirection : (this.state.isRightDirection === null && this.state.points.size > 1 ? (prevPoint.get('x') < newPoint.get('x') ? true : false) : this.state.isRightDirection),
          }
        );
      }
      
    }

    this.props.updateNumOfPoints(this.state.points.size);
  }

  relativeCoordinatesForEvent(mouseEvent) {
    const boundingRect = this.refs.drawArea.getBoundingClientRect();
    return new Immutable.Map({
      x: mouseEvent.clientX - boundingRect.left,
      y: mouseEvent.clientY - boundingRect.top,
    });
  }

  getPattern() {
    let retArr = [];
    for (let i = 0; i < this.state.points.size; i++){
      retArr.push({x: this.state.points.get(i).get('x'), y: StaticVars.CANVAS_SIZE.height - this.state.points.get(i).get('y')});
    }
    return retArr;
    // return this.state.points;  old version - server calcs canvasheight - y for each point
  }

  isKnownPattern(){
    return this.state.patternName !== "" && this.state.patternName !== StaticVars.CUSTOM_PATTERN_TEXT;
  }

  getPatternName(){
    return this.isKnownPattern() ? this.state.patternName : StaticVars.CUSTOM_PATTERN_TEXT;
    // return "";
  }

  getDrawRightDirection() {
    return this.state.isRightDirection;
  }

  clearCanvas() {
    this.props.updateNumOfPoints(0);
    this.setNewState(
      {
        points: new Immutable.List(),
        patternName:StaticVars.CUSTOM_PATTERN_TEXT,
        isDrawing : false,
        isRightDirection : null,
        isHoverOnPoint: false,
        isHoverNearPoint: false,
      }
    );

  }

  setSearchActive(isActive){
    this.setNewState(
      {
        isSearchActive:isActive,
      }
    );

  }

  onDeletePoint(event){
    event.stopPropagation();
    this.props.updateNumOfPoints(this.state.points.size > 0 ?  this.state.points.size - 1 : 0);
    this.setNewState(
      {
        points: this.state.points.splice(this.state.currentPointIndex, 1),
        patternName: StaticVars.CUSTOM_PATTERN_TEXT,
        isDrawing: false,
        isHoverOnPoint: false,
        isHoverNearPoint: false,
        isRightDirection : (this.state.points.size - 1 <= 1 ? null : this.state.isRightDirection),
      }
    );
  }

  render() {
    //onClick={() => this.loadFromLocalStorage()}
    return (
      <div  className="myCanvas" >
        <div
            className={"GraphDrawArea" + (this.state.isHoverOnPoint ? (this.state.isDrawing ? " grabItem" : " selectItem") : " regularCursor")}
            ref="drawArea"
            onMouseDown={this.handleMouseDown}
            onMouseMove={this.handleMouseMove}
            onMouseLeave={this.handleMouseLeave}
        >
        <Drawing points={this.state.points} currentPointIndex={this.state.currentPointIndex} isHoverOnPoint={this.state.isHoverNearPoint} isHideMenu={this.state.isHoverOnPoint && this.state.isDrawing}/>
        {this.state.isHoverNearPoint && !this.state.isDrawing? <EditPointTrash point={this.state.points.get(this.state.currentPointIndex)} onMouseDown={this.onDeletePoint}/> : null}

        </div>

      </div>
      
      
    );
  }//end render
}

function EditPointTrash({point, onMouseDown}){
  const trans = "scale(0.7 0.7)";
  return (
    <svg xmlns="http://www.w3.org/2000/svg" style={{left: point.get('x') + StaticVars.GRAPH_SELECTED_BACKGROUND_SIZE.width*1.25 - 3, top: point.get('y') - StaticVars.GRAPH_SELECTED_BACKGROUND_SIZE.height/2 + 5, width: '30', height:'30', position:"absolute",'zIndex':1 }}>
      <g  
        id="trash"
        transform={trans}
        cursor="pointer"
        onMouseDown={onMouseDown}
      >
        <path d="M26.2887 4.12399H22.2507C22.0858 1.2938 21.0145 0 17.4709 0H11.9494C8.65303 0 7.5817 1.05121 7.49929 4.12399H3.79085C1.73061 4.12399 0 5.74124 0 7.84367V8.08625C0 9.94609 1.40097 11.4825 3.21398 11.8059V25.6334C3.21398 29.434 4.36772 30 7.66411 30H21.509C25.135 30 26.2887 29.1105 26.2887 25.6334V11.8059C28.349 11.7251 29.9972 10.1078 29.9972 8.08625V7.84367C30.0796 5.74124 28.349 4.12399 26.2887 4.12399ZM11.867 1.77898H17.3885C19.6135 1.77898 20.1904 2.02156 20.2728 4.04313H9.2299C9.31231 2.10243 9.55954 1.77898 11.867 1.77898ZM24.5581 25.6334C24.5581 28.1402 24.2285 28.221 21.6738 28.221H7.74652C5.027 28.221 5.10941 28.0593 5.10941 25.6334V11.8059H24.5581V25.6334ZM28.2666 8.08625C28.2666 9.13747 27.3601 10.027 26.2887 10.027H3.79085C2.71952 10.027 1.81302 9.13747 1.81302 8.08625V7.84367C1.81302 6.79245 2.71952 5.90297 3.79085 5.90297H26.2063C27.2776 5.90297 28.1842 6.79245 28.1842 7.84367V8.08625H28.2666ZM20.3552 25.3908C20.6025 25.3908 20.8497 25.1482 20.8497 24.9057V14.0701C20.8497 13.8275 20.6025 13.5849 20.3552 13.5849C20.108 13.5849 19.8608 13.8275 19.8608 14.0701V24.9865C19.8608 25.2291 20.108 25.3908 20.3552 25.3908ZM14.8338 25.3908C15.081 25.3908 15.3282 25.1482 15.3282 24.9057V14.0701C15.3282 13.8275 15.081 13.5849 14.8338 13.5849C14.5865 13.5849 14.3393 13.8275 14.3393 14.0701V24.9865C14.3393 25.2291 14.5865 25.3908 14.8338 25.3908ZM9.2299 25.3908C9.47713 25.3908 9.72436 25.1482 9.72436 24.9057V14.0701C9.72436 13.8275 9.47713 13.5849 9.2299 13.5849C8.98267 13.5849 8.73544 13.8275 8.73544 14.0701V24.9865C8.81785 25.2291 8.98267 25.3908 9.2299 25.3908Z" fill="#000000"/>
        <rect width="30" height="30" style={{fill:"rgba(0,0,0,0)"}}/>
      </g>
    </svg>
  );
}

function Drawing({ points, currentPointIndex, isHoverOnPoint, isHideMenu }) {
  return (
    <svg className="drawing">
      <linearGradient id="grid-gradient" gradientUnits="userSpaceOnUse" x1="0%" y1="0%" x2="100%" y2="100%" gradientTransform="matrix(1 0 0 -1 -761.14 398.97)">
        <stop offset="0%" stopColor="var(--color-stop-1)" />
        <stop offset="100%" stopColor="var(--color-stop-2)" />
      </linearGradient>
      <filter id="dropshadow" x="-40%" y="-40%" width="180%" height="180%" filterUnits="userSpaceOnUse">
        <feGaussianBlur in="SourceAlpha" stdDeviation="1"/>
        <feOffset dx="2" dy="2" result="offsetblur"/> 
        <feOffset dx="-2" dy="-2" result="offsetblur"/>
        <feMerge> 
          <feMergeNode/>
          <feMergeNode in="SourceGraphic"/>
          <feMergeNode in="SourceGraphic"/>
        </feMerge>
    </filter>

      {[...Array(GRIDLINES.vertical)].map((x, i) =>
        <GridLineVertical key={i} lineNumber={i}/>
      )}
      {[...Array(GRIDLINES.horizontal)].map((x, i) =>
        <GridLineHorizontal key={i} lineNumber={i}/>
      )}
      {points.map((point, index) => (
        index > 0 ? 
          <DrawingLine key={index} point={point} prevPoint={points.get(index - 1)} />
          :
          null
      ))}
      {points.map((point, index) => (
          currentPointIndex === index && isHoverOnPoint?
            <EditCircle key={index} point={point} isHideMenu={isHideMenu} />
            :
            <SimpleCircle  style={{filter:"url(#dropshadow)"}} key={index} point={point} />
      ))}
    </svg>
  );
}

function SimpleCircle({point}){
  return(
    <GraphSimplePoint point={point}/>
  );
}

function EditCircle({point, isHideMenu}){
  return(
    <GraphSelectedPoint point={point} isHideMenu={isHideMenu}/>
  );
}

function GridLineHorizontal({lineNumber}){
  return(
    <line className="GraphCanvas_gridLine" x1={0} y1={(lineNumber + 1) * StaticVars.CANVAS_SIZE.height / GRIDLINES.horizontal} x2={StaticVars.CANVAS_SIZE.width} y2={(lineNumber + 1) * StaticVars.CANVAS_SIZE.height / GRIDLINES.horizontal}/>
  );
}

function GridLineVertical({lineNumber}){
  return(
    <line className="GraphCanvas_gridLine" x1={(lineNumber + 1) * StaticVars.CANVAS_SIZE.width / GRIDLINES.vertical} y1={0} x2={(lineNumber + 1) * StaticVars.CANVAS_SIZE.width / GRIDLINES.vertical} y2={StaticVars.CANVAS_SIZE.height}/>
  );
}

function DrawingLine({ point, prevPoint }) {
  let line = [point, prevPoint];
  const pathData = "M " +
    line
      .map(p => {
        return `${p.get('x')} ${p.get('y')}`;
      })
      .join(" L ");

  return <path className="path" d={pathData} />;
}
